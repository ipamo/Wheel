﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Wheel
{
    /// <summary>
    /// Specify a log level for an enum value.
    /// </summary>
    [AttributeUsage(AttributeTargets.Field)]
    public class LevelAttribute : Attribute
    {
        public LevelAttribute(LogLevel level)
        {
            Level = level;
        }

        public LogLevel Level { get; }
    }
}
