﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Diagnostics;
using Wheel;
using Wheel.Data;
using Wheel.Data.Tests.DbContextIntegration.Data;
using Wheel.Data.Tests.DbContextIntegration.Init;

namespace Wheel.Data.Tests.DbContextIntegration
{
    static class Integration
    {
        static void Run()
        {
            var host = CreateHostBuilder()
                .Build();

            using (var scope = host.Services.CreateScope())
            {
                var services = scope.ServiceProvider;
                var initializer = services.GetRequiredService<Initializer>();
                initializer.Init();
            }
        }

        private static IHostBuilder CreateHostBuilder(string[] args = null)
            => Host.CreateDefaultBuilder(args)
                .ConfigureServices(ConfigureServices);

        private static void ConfigureServices(HostBuilderContext context, IServiceCollection services)
        {
            var connectionString = context.Configuration.GetConnectionString("Default");

            // Root services
            services.AddSingleton<IDbms, Dbms>();

            // Scoped services
            services.AddDbContext<ApplicationDbContext>(o =>
            {
                if (context.HostingEnvironment.IsDevelopment())
                    o.EnableSensitiveDataLogging();
                o.UseNpgsql(connectionString);
            });
            services.AddScoped<DbContextHelper>();
            services.AddScoped<ISavedByResolver, EditionResolver>();
            services.AddScoped<Initializer>();
        }
    }
}
