﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Text;
using Wheel;
using Wheel.Commands;
using Wheel.Commands.Internal;
using Wheel.Testing;
using Xunit;
using Xunit.Abstractions;

namespace Wheel.Commands.Tests
{
    public class OptionsTests : IDisposable
    {
        [Command("def")]
        class DefaultCommand
        {
            [CommandOption("-o|--option1", Description = "hey")]
            public int MyIntOption { get; set; } = 4;

            [CommandOption]
            public LogLevel MyEnumOption { get; set; } = LogLevel.Information;

            [CommandOption]
            public LogLevel? NullableOption { get; set; } = LogLevel.Information;

            public void Run([Required] string arg1, string arg2 = "hey", int arg3 = 10)
            {
                Console.WriteLine($"arg1: {arg1}, arg2: {arg2}, arg3: {arg3}, int: {MyIntOption}, enum: {MyEnumOption}, nullable: {NullableOption}");
            }
        }

        [Command("nodef")]
        class NoDefaultCommand
        {
            [CommandOption("-o|--option1", Description = "hey")]
            public int MyIntOption { get; set; }

            [CommandOption]
            public LogLevel MyEnumOption { get; set; }

            [CommandOption]
            public LogLevel? NullableOption { get; set; }

            public void Run([Required] string arg1, string arg2, int arg3)
            {
                Console.WriteLine($"arg1: {arg1}, arg2: {arg2}, arg3: {arg3}, int: {MyIntOption}, enum: {MyEnumOption}, nullable: {NullableOption}");
            }
        }

        private readonly ITestOutputHelper _output;
        private readonly TestLogger _logger;

        public OptionsTests(ITestOutputHelper output)
        {
            _output = output;
            _logger = new TestLogger(output);
        }

        public void Dispose()
        {
            _logger.Clear();
        }

        [Fact]
        public void OptionFromProperty()
        {
            var prop = typeof(DefaultCommand).GetProperty("MyEnumOption");
            Assert.Equal("--my-enum-option", CommandTypeWrapper.GetOptionTemplate(prop));
            Assert.Equal("Trace | Debug | Information | Warning | Error | Critical | None", CommandTypeWrapper.GetOptionDescription(prop));
                        
            prop = typeof(DefaultCommand).GetProperty("NullableOption");
            Assert.Equal("--nullable-option", CommandTypeWrapper.GetOptionTemplate(prop));
            Assert.Equal("Trace | Debug | Information | Warning | Error | Critical | None", CommandTypeWrapper.GetOptionDescription(prop));
        }

        [Fact]
        public void DefaultValues()
        {
            string stdout;
            using (var capture = new ConsoleCapture(_output))
            {
                var app = BuildCommandApplication();

                app.Run("def", "arg1");
                stdout = capture.GetOut();
            }

            Assert.Contains($"arg1: arg1, arg2: hey, arg3: 10, int: 4, enum: Information, nullable: Information", stdout);
        }

        [Fact]
        public void NoDefaultValues()
        {
            string stdout;
            using (var capture = new ConsoleCapture(_output))
            {
                var app = BuildCommandApplication();

                app.Run("nodef", "arg1");
                stdout = capture.GetOut();
            }

            Assert.Contains($"arg1: arg1, arg2: , arg3: 0, int: 0, enum: Trace, nullable: ", stdout);
        }

        private ICommandApplication BuildCommandApplication()
        {
            return CommandApplication.CreateDefaultBuilder()
                .ConfigureRootCommand(cmd =>
                {
                    cmd.Command<DefaultCommand>();
                    cmd.Command<NoDefaultCommand>();
                })
                .ConfigureLogging(builder =>
                {
                    builder.SetMinimumLevel(LogLevel.Trace);
                    builder.AddProvider(new SingleLoggerProvider(_logger));
                })
                .Build();
        }
    }
}
 