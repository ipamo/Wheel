﻿using System;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.IO;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace Wheel.Data
{
    public class SaveChangesTracker
    {
        protected readonly IDbms _dbms;

        // IServiceProvider will be used to provide IUserResolver at first call to UpdateAutoProperties.
        // IUserResolver cannot be included as a constructor dependency as it would introduce a
        // dependency loop with DbContext.
        protected readonly IServiceProvider _services;

        public SaveChangesTracker(IDbms dbms, IServiceProvider services)
        {
            _dbms = dbms;
            _services = services;
        }

        private bool _firstCall = true;
        private ISavedAtResolver _savedAtResolver;
        private ISavedByResolver _savedByResolver;
        private ISavedThroughResolver _savedThroughResolver;

        /// <summary>
        /// Default helper for SaveChanges methods.
        /// </summary>
        /// <remarks>
        /// Should be added in overrides of the two methods:
        /// <see cref="DbContext.SaveChanges(bool)"/> and <see cref="DbContext.SaveChangesAsync(bool, CancellationToken)"/>.
        /// </remarks>
        public virtual void SaveChanges(ChangeTracker tracker)
        {
            // First call: retrieve resolvers + resolve and cache "byId" value.
            if (_firstCall)
            {
                _savedAtResolver = _services.GetService<ISavedAtResolver>();
                _savedByResolver = _services.GetService<ISavedByResolver>();
                _savedThroughResolver = _services.GetService<ISavedThroughResolver>();
                _firstCall = false;
            }

            // Resolve "byId" value
            object byId;
            if (_savedByResolver != null)
            {
                byId = _savedByResolver.GetId();
            }
            else
            {
                byId = null;
            }

            // Resolve "throughId" value
            object throughId;
            if (_savedThroughResolver != null)
            {
                throughId = _savedThroughResolver.GetId();
            }
            else
            {
                throughId = null;
            }

            // Resolve "at" value
            DateTime at;
            if (_savedAtResolver != null)
            {
                at = _savedAtResolver.GetAt();
            }
            else
            {
                at = DateTime.Now;
            }

            foreach (var entity in tracker.Entries().Where(x => x.State == EntityState.Added || x.State == EntityState.Modified))
            {
                foreach (var property in entity.Properties)
                {
                    HandleAt(entity, property, at);

                    if (byId != null)
                        HandleById(entity, property, byId);

                    if (throughId != null)
                        HandleThroughId(entity, property, throughId);

                    PreventInsertedModification(entity, property);
                }
            }
        }

        private void HandleAt(EntityEntry entity, PropertyEntry property, DateTime at)
        {
            if (!MustAssignAt(property.Metadata.Name, entity.State))
                return;

            if (property.Metadata.ClrType != typeof(DateTime) && property.Metadata.ClrType != typeof(DateTime?))
                throw new InvalidOperationException($"Invalid type for property {property.Metadata.Name}: {property.Metadata.ClrType} (expected DateTime)");

            property.CurrentValue = at;
        }

        private void HandleById(EntityEntry entity, PropertyEntry property, object byId)
        {
            if (!MustAssignById(property.Metadata.Name, entity.State))
                return;

            var expectedType = byId.GetType();
            if (property.Metadata.ClrType != expectedType && property.Metadata.ClrType != expectedType.GetNullableType())
                throw new InvalidOperationException($"Invalid type for property {entity.Metadata.Name}.{property.Metadata.Name}: {property.Metadata.ClrType} (expected {expectedType})");

            property.CurrentValue = byId;
        }

        private void HandleThroughId(EntityEntry entity, PropertyEntry property, object throughId)
        {
            if (!MustAssignThroughId(property.Metadata.Name, entity.State))
                return;

            var expectedType = throughId.GetType();
            if (property.Metadata.ClrType != expectedType && property.Metadata.ClrType != expectedType.GetNullableType())
                throw new InvalidOperationException($"Invalid type for property {entity.Metadata.Name}.{property.Metadata.Name}: {property.Metadata.ClrType} (expected {expectedType})");

            property.CurrentValue = throughId;
        }

        private void PreventInsertedModification(EntityEntry entity, PropertyEntry property)
        {
            if (entity.State == EntityState.Modified)
            {
                // Ensure InsertedAt/By/Through are never modified
                var propertyName = property.Metadata.Name;
                if (_dbms.IsInsertedAtProperty(propertyName) || _dbms.InsertedByIdProperty == propertyName || _dbms.InsertedThroughIdProperty == propertyName)
                {
                    property.IsModified = false;
                }
            }
        }

        protected virtual bool MustAssignAt(string property, EntityState state)
        {
            if (state == EntityState.Added)
            {
                return _dbms.IsInsertedAtProperty(property)
                    || (_dbms.AssignUpdatedPropertiesAtInsertion && _dbms.IsUpdatedAtProperty(property));
            }
            else if (state == EntityState.Modified)
            {
                return _dbms.IsUpdatedAtProperty(property);
            }
            else
            {
                return false;
            }
        }

        protected virtual bool MustAssignById(string property, EntityState state)
        {
            if (state == EntityState.Added)
            {
                return property == _dbms.InsertedByIdProperty
                    || (_dbms.AssignUpdatedPropertiesAtInsertion && property == _dbms.UpdatedByIdProperty);
            }
            else if (state == EntityState.Modified)
            {
                return property == _dbms.UpdatedByIdProperty;
            }
            else
            {
                return false;
            }
        }

        protected virtual bool MustAssignThroughId(string property, EntityState state)
        {
            if (state == EntityState.Added)
            {
                return property == _dbms.InsertedThroughIdProperty
                    || (_dbms.AssignUpdatedPropertiesAtInsertion && property == _dbms.UpdatedThroughIdProperty);
            }
            else if (state == EntityState.Modified)
            {
                return property == _dbms.UpdatedThroughIdProperty;
            }
            else
            {
                return false;
            }
        }
    }
}
