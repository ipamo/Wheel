﻿using Wheel.Data.Tests.DbContextIntegration.Data;
using Wheel.Data.Tests.DbContextIntegration.Models;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;

namespace Wheel.Data.Tests.DbContextIntegration.Init
{
    public class Initializer
    {
        private readonly ApplicationDbContext _context;
        private readonly ILogger _logger;

        public Initializer(ApplicationDbContext context,
            ILogger<Initializer> logger)
        {
            _context = context;
            _logger = logger;
        }

        public void Init()
        {
            _logger.LogInformation("EnsureDeleted ...");
            _context.Database.EnsureDeleted();

            _logger.LogInformation("EnsureCreated ...");
            _context.Database.EnsureCreated();

            _logger.LogInformation("Seed ...");
            var post = new Post
            {
                Title = "Test post"
            };
            _context.Add(post);

            _logger.LogInformation("SaveChanges ...");
            _context.SaveChanges();
        }
    }
}
