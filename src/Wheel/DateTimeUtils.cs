﻿using System;
using System.Globalization;
using System.Text.RegularExpressions;

namespace Wheel
{
    public static class DateTimeUtils
    {
#if !NET20
        /// <summary>
        /// Return the min and max datetime from the given values, if any.
        /// If none, return default value.
        /// </summary>
        public static (DateTime min, DateTime max) GetMinMax(DateTime? dt1, DateTime? dt2,
            DateTime? byDefault = null)
        {
            var any = false;
            var min = DateTime.MaxValue;
            var max = DateTime.MinValue;
            var dts = new DateTime?[] { dt1, dt2 };
            foreach (var dt in dts)
            {
                if (dt.HasValue)
                {
                    any = true;
                    if (dt.Value < min)
                        min = dt.Value;
                    if (dt.Value > max)
                        max = dt.Value;
                }
            }
            if (! any)
            {
                if (byDefault.HasValue)
                {
                    min = byDefault.Value;
                    max = byDefault.Value;
                }
                else
                {
                    min = default(DateTime);
                    max = default(DateTime);
                }
            }
            return (min, max);
        }
#endif

        /// <summary>
        /// Usage example:
        /// <code>CultureInfo.CurrentUICulture.DateTimeFormat.GetShortDateWithoutYearPattern()</code>
        /// </summary>
        public static string GetShortDateWithoutYearPattern(this DateTimeFormatInfo dateTimeFormat)
        {
            var shortDatePattern = dateTimeFormat.ShortDatePattern;

            // Strip yyyy prefixed or followed by any non-alphanumeric pattern
            var regex = new Regex(@"^y+[^0-9A-Za-z]+|[^0-9A-Za-z]+y+$");
            return regex.Replace(shortDatePattern, "");
        }
    }
}
