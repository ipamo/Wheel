:: Prerequisite: nuget SetApiKey Your-API-Key
@echo off
setlocal enabledelayedexpansion

call :push_project Wheel.Backports || goto :eof
call :push_project Wheel || goto :eof
call :push_project Wheel.Plus || goto :eof
call :push_project Wheel.Plus.Backports || goto :eof
call :push_project Wheel.Commands || goto :eof
call :push_project Wheel.Data || goto :eof
call :push_project Wheel.Data.PostgreSql || goto :eof
call :push_project Wheel.Data.SqlServer || goto :eof
call :push_project Wheel.Data.Oracle || goto :eof
call :push_project Wheel.Testing || goto :eof
call :push_project Wheel.Ftp || goto :eof

goto :eof

:push_project
	set project=%~1
	set version=3.1.0
	if "!project!" == "Wheel.Backports" set version=3.1.1

	set /P AREYOUSURE=Confirm publication of !project! !version! (y/[n])?
	if /I "%AREYOUSURE%" neq "y" goto :eof

	nuget push build\!project!.!version!.nupkg -Source https://api.nuget.org/v3/index.json
	nuget push build\!project!.!version!.Symbols.nupkg -Source https://nuget.smbsrc.net/

	goto :eof
