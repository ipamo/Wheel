﻿using System;
using Microsoft.Extensions.DependencyInjection;
using Wheel;
using Wheel.Commands;

namespace ConsoleApp_Net461
{
    class Program
    {
        static int Main(string[] args)
            => CommandApplication.CreateDefaultBuilder()
                .UseStartup<Program>()
                .Build()
                .Run(args);

        public void ConfigureCommands(ICommandBuilder root, IServiceCollection services)
        {
            root.Command("essai", cmd =>
            {
                cmd.Description = "Essai";
                cmd.Action = () => Console.WriteLine("Hello from essai");
            });
        }
    }
}
