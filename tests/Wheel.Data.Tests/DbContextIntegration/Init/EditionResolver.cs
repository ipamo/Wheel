﻿using Wheel.Data.Tests.DbContextIntegration.Data;
using Wheel.Data.Tests.DbContextIntegration.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Wheel.Data;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace Wheel.Data.Tests.DbContextIntegration
{
    public class EditionResolver : ISavedByResolver
    {
        private readonly ApplicationDbContext _context;

        public EditionResolver(ApplicationDbContext context)
        {
            _context = context;
        }

        public DateTime GetNow() => DateTime.UtcNow;

        public PropertyEntry GetByIdEntry()
        {
            var name = $"{Environment.UserDomainName}\\{Environment.UserName}";

            var user = _context.Users
                .Where(x => x.Name == name)
                .FirstOrDefault();

            if (user == null)
            {
                user = new User
                {
                    Name = name
                };
                _context.Add(user);
            }

            return _context.Entry(user).Property(x => x.Id);
        }

        public PropertyEntry GetThroughIdEntry() => null;
    }
}
