﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace Wheel
{
    /// <summary>
    /// Provide general information about an application.
    /// </summary>
    public interface IApplicationInformation
    {
        Assembly Assembly { get; }

        string Name { get; }

        string Version { get; }

        string Description { get; }
    }
}
