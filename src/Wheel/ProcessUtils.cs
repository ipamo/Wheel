﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Diagnostics;
using System.IO;
using Microsoft.Extensions.Logging;

namespace Wheel
{
    /// <summary>
    /// Execute system processus.
    /// </summary>
    public static class ProcessUtils
    {
        public static bool IsRunning(int processId)
        {
            try
            {
                Process.GetProcessById(processId);
            }
            catch (InvalidOperationException)
            {
                return false;
            }
            catch (ArgumentException)
            {
                return false;
            }
            return true;
        }

        public static readonly Encoding WINDOWS_CMD_ENCODING = Encoding.GetEncoding(850);

        /// <summary>
        /// Execute a system processus.
        /// </summary>
        /// <param name="filename">Name of the file or command to execute.</param>
        /// <param name="arguments">Command-line arguments.</param>
        /// <param name="workingDirectory">Working directory.</param>
        /// <param name="environmentVariables">Environment variables to pass to the process.</param>
        /// <param name="stdin">Standard input reader. Can be <see cref="Stream"/> or <see cref="string"/>.</param>
        /// <param name="stdout">Standard output writer.</param>
        /// <param name="stderr">Standard error writer.</param>
        /// <param name="encoding">Encoding for input and output streams.
        /// By default, uses system's default (or Encoding.UTF8 for .NET Core).
        /// Use <see cref="WINDOWS_CMD_ENCODING"/> for batch scripts.</param>
        /// <param name="timeout">Timeout in milliseconds.</param>
        /// <param name="killTimeout">Timeout in millisecond before hard killing if a <paramref name="timeout"/> was reached.
        /// If 0, <c>1000 ms</c> will be used.
        /// If <c>-1</c>, there will be no hard killing.</param>
        /// <param name="logger">A logger for stdout, stderr and timeout problems.</param>
        /// <param name="stdoutLevel">Logging level for the standard output (use LogLevel.None to disable logging).</param>
        /// <param name="stderrLevel">Logging level for the standard error (use LogLevel.None to disable logging).</param>
        /// <returns>Exit code</returns>
        public static int ExecuteProcess(
            string filename,
            string arguments = null,
            string workingDirectory = null,
            Dictionary<string, string> environmentVariables = null,
            object stdin = null,
            TextWriter stdout = null,
            TextWriter stderr = null,
            int timeout = 60000,
            int killTimeout = 0,
            Encoding encoding = null,
            ILogger logger = null,
            LogLevel stdoutLevel = LogLevel.Debug,
            LogLevel stderrLevel = LogLevel.Error
            )
        {
            if (stdin != null && !(stdin is Stream) && !(stdin is string))
                throw new ArgumentException($"stdin is of type {stdin.GetType()} (expected: Stream or string)", "stdin");
            
            if (encoding == null)
            {
#if !NETSTANDARD2_0
                encoding = Encoding.Default;
#else
                encoding = new UTF8Encoding(encoderShouldEmitUTF8Identifier: false);
#endif
            }

            using (var process = new Process())
            {
                process.StartInfo.FileName = filename;
                if (arguments != null)
                    process.StartInfo.Arguments = arguments;
                if (workingDirectory != null)
                    process.StartInfo.WorkingDirectory = workingDirectory;

                if (environmentVariables != null)
                {
                    foreach (KeyValuePair<string, string> entry in environmentVariables)
                    {
#if NETSTANDARD2_0
                        process.StartInfo.Environment[entry.Key] = entry.Value;
#else
                        process.StartInfo.EnvironmentVariables[entry.Key] = entry.Value;
#endif
                    }
                }

                process.StartInfo.UseShellExecute = false;
                process.StartInfo.CreateNoWindow = true;

                process.StartInfo.RedirectStandardOutput = true;
                process.StartInfo.RedirectStandardError = true;
                if (stdin != null)
                    process.StartInfo.RedirectStandardInput = true;

                process.StartInfo.StandardOutputEncoding = encoding;
                process.StartInfo.StandardErrorEncoding = encoding;

                using (var outputWaitHandle = new AutoResetEvent(false))
                using (var errorWaitHandle = new AutoResetEvent(false))
                {
                    process.OutputDataReceived += (sender, e) =>
                    {
                        if (e.Data == null)
                        {
                            outputWaitHandle.Set();
                        }
                        else
                        {
                            if (stdout != null)
                                stdout.WriteLine(e.Data);
                            if (logger != null && logger.IsEnabled(stdoutLevel))
                                LoggingUtils.Log(logger, stdoutLevel, "[PID {0}] {1}", process.Id, e.Data);
                        }
                    };
                    process.ErrorDataReceived += (sender, e) =>
                    {
                        if (e.Data == null)
                        {
                            errorWaitHandle.Set();
                        }
                        else
                        {
                            if (stderr != null)
                                stderr.WriteLine(e.Data);
                            if (logger != null && logger.IsEnabled(stderrLevel))
                                LoggingUtils.Log(logger, stderrLevel, "[PID {0}] {1}", process.Id, e.Data);
                        }
                    };

                    process.Start();

                    process.BeginOutputReadLine();
                    process.BeginErrorReadLine();

                    if (stdin != null)
                    {
                        if (stdin is Stream)
                        {
                            process.StandardInput.AutoFlush = true;
                            process.StandardInput.Write((Stream)stdin);
                            process.StandardInput.Dispose();
                        }
                        else if (stdin is string)
                        {
                            Encoding stdinEncoding;
                            if (encoding is UTF8Encoding)
                                stdinEncoding = new UTF8Encoding(encoderShouldEmitUTF8Identifier: false);
                            else
                                stdinEncoding = encoding;

                            var writer = new StreamWriter(process.StandardInput.BaseStream, stdinEncoding);
                            writer.AutoFlush = true;
                            writer.Write((string)stdin);
                            writer.Dispose();
                        }
                    }

#if NET20
                    if (process.WaitForExit(timeout) && outputWaitHandle.WaitOne(timeout, false) && errorWaitHandle.WaitOne(timeout, false))
#else
                    if (process.WaitForExit(timeout) && outputWaitHandle.WaitOne(timeout) && errorWaitHandle.WaitOne(timeout))
#endif
                    {
                        return process.ExitCode;
                    }
                    else
                    {
                        if (killTimeout >= 0)
                        {
                            try
                            {
                                if (logger != null)
                                    logger.LogDebug($"Timeout: kill PID {process.Id} ...");
                                process.Kill();

                                // Laisse un peu de temps au processus pour s'arrêter.
                                if (killTimeout == 0)
                                {
                                    killTimeout = timeout / 10;
                                    if (killTimeout > 1000)
                                        killTimeout = 1000;
                                    else if (killTimeout < 50)
                                        killTimeout = 50;
                                }

                                if (!process.WaitForExit(killTimeout))
                                {
                                    string message;
                                    if (!process.HasExited)
                                        message = $"Killing PID {process.Id}: not killed after {killTimeout} ms";
                                    else
                                        message = $"Killing PID {process.Id}: unknown status after {killTimeout} ms (WaitForExit returned false but HasExited returned true)";
                                    LoggingUtils.SafeLog(logger, LogLevel.Warning, message);
                                }
                                else
                                {
                                    if (logger != null)
                                        logger.LogDebug($"Timeout: PID {process.Id} killed");
                                }
                            }
                            catch (Exception e)
                            {
                                string message = $"Killing PID {process.Id}: " + e.Message;
                                LoggingUtils.SafeLog(logger, LogLevel.Warning, message);
                            }
                        }
                        throw new TimeoutException($"Timeout of PID {process.Id} ({filename})");
                    }
                }
            }
        }
    }
}
