﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Threading;

namespace Wheel.Commands.Internal
{
    internal class CommandRunner : IDisposable
    {
        private readonly DateTime _initTime;
        private readonly WheelInfra _infra;
        private readonly CommandApplicationOptions _options;
        private readonly IReporter _reporter;
        private readonly ILogger _logger;
        private readonly CommandBuilder _command;

        public CommandRunner(WheelInfra infra, CommandApplicationOptions options, CommandBuilder command)
        {
            _initTime = DateTime.Now;
            _infra = infra ?? throw new InvalidOperationException(nameof(infra));
            _options = options ?? throw new InvalidOperationException(nameof(options));
            _logger = _infra.LoggerFactory.CreateLogger<CommandRunner>();
            _command = command ?? throw new InvalidOperationException(nameof(command));
            _reporter = BuildLogReporter();
            _infra.RegisterReporter(_reporter);
        }

        public void Dispose()
        {
            _infra.UnregisterReporter();
        }

        private IReporter BuildLogReporter()
        {
            if (_options.ForceHtmlReporter || _options.ReportEmailEnabled == true || _command.ParsedReport || _command.ParsedReportTo != null)
            {
                return new HtmlReporter();
            }
            else
            {
                return new LogCounter();
            }
        }

        public int Run()
        {
            try
            {
                _logger.LogTrace("Command {0} starts in thread {1}", _command.Name, Thread.CurrentThread.ManagedThreadId);
                return Terminate(_command.Func());
            }
            catch (Exception e)
            {
                return HandleException(e);
            }
        }

        private int HandleException(Exception e)
        {
            if (e is MessageException)
            {
                var level = ((MessageException)e).Level;

                if (e.InnerException != null)
                    LoggingUtils.SafeLog(_logger, level, e.InnerException, e.Message);
                else
                    LoggingUtils.SafeLog(_logger, level, e.Message);

                if (level <= LogLevel.Information)
                    return Terminate(ReturnCode.OK);
                else
                    return Terminate((int)level);
            }
            else
            {
                LoggingUtils.SafeLog(_logger, LogLevel.Critical, e, e.Message);
                return Terminate(ReturnCode.ERROR);
            }
        }

        private int Terminate(int returnCode)
        {
            try
            {
#if !NET20
                try
                {
                    if (_command.ParsedReport || _command.ParsedReportTo != null || _options.ReportEmailEnabled == true)
                    {
                        SendReportEmail(_reporter, _command.ParsedReportTo);
                    }
                }
                catch (Exception e)
                {
                    _logger.LogError("Cannot send report for command {0}: {1}", _command.Name, e.Message);
                    returnCode = ReturnCode.SEND_REPORT_ERROR;
                }
#endif

                string message;

                if (_reporter.CountErrors > 0 || _reporter.CountCriticals > 0)
                {
                    message = $"Command {_command.Name} emitted {_reporter.CountErrors + _reporter.CountCriticals} error{(_reporter.CountErrors + _reporter.CountCriticals > 1 ? "s" : "")}" + (_reporter.CountWarnings > 0 ? $" and {_reporter.CountWarnings} warning{(_reporter.CountWarnings > 1 ? "s" : "")}" : "");
                    _logger.LogError(message);
                    if (returnCode < ReturnCode.ERROR)
                        returnCode = ReturnCode.ERROR;
                }
                else if (_reporter.CountWarnings > 0)
                {
                    message = $"Command {_command.Name} emitted {_reporter.CountWarnings} warning{(_reporter.CountWarnings > 1 ? "s" : "")}";
                    _logger.LogWarning(message);
                    if (returnCode < ReturnCode.WARNING)
                        returnCode = ReturnCode.WARNING;
                }

                _logger.LogTrace("Command {0} terminates with return code {1}", _command.Name, returnCode);
                return returnCode;
            }
            catch (Exception e) when (!Debugger.IsAttached)
            {
                _logger.LogError("Exception raised during command {0} termination: {1}", _command.Name, e);
                return ReturnCode.COMMAND_TERMINATE_ERROR;
            }
        }

#if !NET20
        private void SendReportEmail(IReporter reporter, string to = null)
        {
            if (!(reporter is HtmlReporter))
                throw new InvalidOperationException("Only HtmlReporter objects can be sent by email");

            var htmlReporter = (HtmlReporter)reporter;

            if (string.IsNullOrEmpty(to))
                to = _options.ReportEmailTo?.Trim();
            else
                to = to.Trim();
            
            if (string.IsNullOrEmpty(to))
                throw new InvalidOperationException("No recipient configured for report email");

            var emailSender = _infra.ApplicationServices.GetService<IEmailSender>();
            if (emailSender == null)
                throw new InvalidOperationException("No IEmailSender service available");

            _logger.LogDebug($"Send report to {to}");
            htmlReporter.SetApplicationDetails(
                environmentName: _infra.Environment.EnvironmentName,
                applicationName: _infra.ApplicationInformation.Name,
                applicationVersion: _infra.ApplicationInformation.Version,
                commandName: _command.Name,
                arguments: _command.ParsedArguments,
                initTime: _initTime);

            emailSender.SendHtmlEmailAsync(to, htmlReporter.GetSubject(), htmlReporter.GetBodyContent(), highImportance: htmlReporter.HighImportance).Wait();

            if (_options.SleepAfterReportSent > 0)
                Thread.Sleep(_options.SleepAfterReportSent);
        }
#endif
    }
}
