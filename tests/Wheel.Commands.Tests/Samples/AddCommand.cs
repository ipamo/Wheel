﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.ComponentModel.DataAnnotations;
using Wheel;
using Wheel.Commands;

namespace Wheel.Commands.Tests
{
    /// <summary>
    /// Example of command implicitely defined using attributes.
    /// </summary>
    [Command(Description = "Add or multiply two integers")]
    class AddCommand
    {
        private ILogger _logger;

        public AddCommand(ILogger<AddCommand> logger)
        {
            _logger = logger;
        }

        [CommandOption("-m|--multiply", Description = "If set, multiply instead of add")]
        public bool Multiply { get; set; }

        public int Run([Required] int num1, [Required] int num2, int[] nums)
        {
            int result;
            if (Multiply)
                result = num1 * num2;
            else
                result = num1 + num2;

            foreach (var num in nums)
            {
                if (Multiply)
                    result = result * num;
                else
                    result = result + num;
            }

            _logger.LogInformation("Result is: {result}", result);
            return result;
        }
    }
}
