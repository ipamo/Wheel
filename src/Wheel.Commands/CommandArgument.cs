// Adapted from .NET Foundation's CommandLineUtils.
// Copyright (c) .NET Foundation. All rights reserved.
// Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.

using System;
using System.Collections.Generic;
using System.Linq;

namespace Wheel.Commands
{
    /// <summary>
    /// Describe an argument (or all remaining arguments if <see cref="Multiple"/> is true) for the command.
    /// Should be defined using one of the <c>Argument</c> methods of <see cref="ICommandBuilder"/>.
    /// </summary>
    public class CommandArgument
    {
        private readonly List<string> _rawValues = new List<string>();

        public CommandArgument(string name)
        {
            Name = name;
        }

        /// <summary>
        /// Name of the argument displayed in help text. For example: <c>[arg]</c>.
        /// </summary>
        public string Name { get; }

        /// <summary>
        /// Short description of the argument displayed in help text. For example: <c>An argument.</c>.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Indicate whether the option can have multiple values.
        /// </summary>
        public bool Multiple { get; set; }

        /// <summary>
        /// Indicate whether the argument is required. If it is required and not given, a <see cref="CommandParserException"/> will be raised
        /// when using <see cref="CommandArgument.Value"/> property.
        /// </summary>
        public bool Required { get; set; }

        /// <summary>
        /// When disabled, the argument does not appear in the list of options and arguments of the command's help text. Enabled by default.
        /// </summary>
        public bool ShowInHelp { get; set; } = true;

        internal void Clear()
        {
            _rawValues.Clear();
        }

        internal void AddValue(string value)
        {
            _rawValues.Add(value);
        }

        /// <summary>
        /// Get all values of the argument (intended for multiple-value arguments).
        /// </summary>
        public List<string> Values
        {
            get
            {
                if (Required)
                {
                    var firstValue = _rawValues.FirstOrDefault();
                    if (string.IsNullOrEmpty(firstValue))
                        throw new CommandParserException($"Argument '{Name}' is required");
                }
                return _rawValues;
            }
        }

        /// <summary>
        /// Indicate whether a value has been assigned for the argument.
        /// </summary>
        public bool HasValue => _rawValues.Any();

        /// <summary>
        /// Get the value of the argument (intended for non-multiple-value arguments).
        /// </summary>
        public string Value => Values.FirstOrDefault();

        /// <summary>
        /// Get the value of the argument as an integer. Throws an exception if the provided value is not an integer.
        /// </summary>
        public int IntValue
        {
            get
            {
                var value = _rawValues.FirstOrDefault();
                if (string.IsNullOrEmpty(value))
                {
                    if (Required)
                        throw new CommandParserException($"Argument '{Name}' is required");
                    else
                        return default(int);
                }
                if (!int.TryParse(value, out int intValue))
                    throw new CommandParserException($"Argument '{Name}' must be an integer");
                return intValue;
            }
        }
    }
}
