﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;
using Wheel.Testing;
using Xunit;
using Xunit.Abstractions;

namespace Wheel.Tests
{
    public class LoggingTests
    {
        private static TestLogger _logger;

        public LoggingTests(ITestOutputHelper output)
        {
            _logger = new TestLogger(output);
        }

        [Fact]
        public void InformationOnly()
        {
            _logger.LogInformation("Info");
            Assert.Equal(0, _logger.CountProblems);
        }

        [Fact]
        public void OneWarning()
        {
            _logger.LogWarning("Warning0");
            Assert.Equal(1, _logger.CountWarnings);
            Assert.Equal(1, _logger.CountProblems);
        }

        [Fact]
        public void OneWarningAndOneError()
        {
            _logger.LogWarning("Warning");
            _logger.LogError("Error");
            Assert.Equal(1, _logger.CountWarnings);
            Assert.Equal(1, _logger.CountErrors);
            Assert.Equal(2, _logger.CountProblems);
        }

        [Fact]
        public void TwoWarnings()
        {
            _logger.LogWarning("WarningA");
            _logger.LogWarning("WarningB");
            Assert.Equal(2, _logger.CountWarnings);
            Assert.Equal(2, _logger.CountProblems);
        }

        [Fact]
        public void TwoWarningsAndTwoErrors()
        {
            _logger.LogWarning("Warning1");
            _logger.LogError("Error1");
            _logger.LogWarning("Warning2");
            _logger.LogError("Error2");
            Assert.Equal(2, _logger.CountWarnings);
            Assert.Equal(2, _logger.CountErrors);
            Assert.Equal(4, _logger.CountProblems);
        }
    }
}
