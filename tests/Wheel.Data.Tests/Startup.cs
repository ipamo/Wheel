﻿using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using Wheel.Data;
using Wheel.Testing;
using Xunit;
using Microsoft.Extensions.Configuration;
using Wheel.Data.Tests.Models;

namespace Wheel.Data.Tests
{
    public class Startup
    {
        public void ConfigureServices(IServiceCollection services, IConfiguration config)
        {
            if (config["Dbms:Type"].ToLower().Contains("sqlserver"))
            {
                services.AddSingleton<IDbms, SqlServerDbms>();
                services.AddDbContext<TestsDbContext>(builder => builder.UseSqlServer(config.GetConnectionString("Default")));
            }
            else
            {
                services.AddSingleton<IDbms, PostgreSqlDbms>();
                services.AddDbContext<TestsDbContext>(builder => builder.UseNpgsql(config.GetConnectionString("Default")));
            }

            services.AddIdentityCore<User>(options =>
            {
                // User settings
                options.User.AllowedUserNameCharacters = @"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-._@+\";
                options.User.RequireUniqueEmail = true;

                // Password settings
                options.Password.RequireDigit = false;
                options.Password.RequiredLength = 6;
                options.Password.RequireNonAlphanumeric = false;
                options.Password.RequireLowercase = false;
                options.Password.RequireUppercase = false;

                // Lockout settings
                options.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes(30);
                options.Lockout.MaxFailedAccessAttempts = 5;
                options.Lockout.AllowedForNewUsers = true;
            })
                .AddRoles<Role>()
                .AddEntityFrameworkStores<TestsDbContext>();
        }
    }
}
