﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace Wheel.Internal
{
    internal class ApplicationInformation : IApplicationInformation
    {
        public Assembly Assembly { get; set; }

        public string Name { get; set; }

        public string Version { get; set; }

        public string Description { get; set; }
    }
}
