﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Wheel
{
    /// <summary>
    /// Hash files, strings or byte arrays.
    /// </summary>
    public static class HashUtils
    {
        // ********************************************************************
        #region "Md5"

        /// <summary>
        /// Compute the Md5 hash of a byte array.
        /// </summary>
        public static byte[] Md5(byte[] input, int offset, int count)
        {
            using (var md5 = System.Security.Cryptography.MD5.Create())
            {
                return md5.ComputeHash(input, offset, count);
            }
        }

        /// <summary>
        /// Compute the Md5 hash of a byte array.
        /// </summary>
        public static byte[] Md5(byte[] input, int offset)
        {
            return Md5(input, offset, input.Length - offset);
        }

        /// <summary>
        /// Compute the Md5 hash of a byte array.
        /// </summary>
        public static byte[] Md5(byte[] input)
        {
            return Md5(input, 0, input.Length);
        }

        /// <summary>
        /// Compute the Md5 hash of a stream.
        /// </summary>
        /// <param name="stream">Input stream</param>
        /// <returns>Md5 hash. Use <see cref="HexUtils.ToString"/> to convert this hash into a string.</returns>
        public static byte[] Md5(Stream stream)
        {
            using (var md5 = System.Security.Cryptography.MD5.Create())
            {
                return md5.ComputeHash(stream);
            }
        }

        /// <summary>
        /// Compute the Md5 hash of a string.
        /// </summary>
        /// <param name="input">Input string</param>
        /// <returns>Md5 hash. Use <see cref="HexUtils.ToString"/> to convert this hash into a string.</returns>
        public static byte[] Md5String(string input)
        {
            byte[] bytes = System.Text.Encoding.UTF8.GetBytes(input);
            return Md5(bytes);
        }

        /// <summary>
        /// Compute the Md5 hash of a file.
        /// </summary>
        /// <param name="path">Path of the file</param>
        /// <param name="bufferSize">Size of the computation buffer (in bytes)</param>
        /// <returns>Md5 hash. Use <see cref="HexUtils.ToString"/> to convert this hash into a string.</returns>
        public static byte[] Md5File(string path, int bufferSize = 1000000)
        {
            // Buffer is compulsory in order to accelerate computation: cf. http://stackoverflow.com/questions/1177607/what-is-the-fastest-way-to-create-a-checksum-for-large-files-in-c-sharp
            using (var stream = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.Read, bufferSize))
            {
                return Md5(stream);
            }
        }

        #endregion


        // ********************************************************************
        #region "Sha1"

        /// <summary>
        /// Compute the Sha1 hash of a byte array.
        /// </summary>
        public static byte[] Sha1(byte[] input, int offset, int count)
        {
            using (var sha1 = System.Security.Cryptography.SHA1.Create())
            {
                return sha1.ComputeHash(input, offset, count);
            }
        }

        /// <summary>
        /// Compute the Sha1 hash of a byte array.
        /// </summary>
        public static byte[] Sha1(byte[] input, int offset)
        {
            return Sha1(input, offset, input.Length - offset);
        }

        /// <summary>
        /// Compute the Sha1 hash of a byte array.
        /// </summary>
        public static byte[] Sha1(byte[] input)
        {
            return Sha1(input, 0, input.Length);
        }

        /// <summary>
        /// Compute the Sha1 hash of a stream.
        /// </summary>
        /// <param name="stream">Input stream</param>
        /// <returns>Sha1 hash. Use <see cref="HexUtils.ToString"/> to convert this hash into a string.</returns>
        public static byte[] Sha1(Stream stream)
        {
            using (var Sha1 = System.Security.Cryptography.SHA1.Create())
            {
                return Sha1.ComputeHash(stream);
            }
        }

        /// <summary>
        /// Compute the Sha1 hash of a string.
        /// </summary>
        /// <param name="input">Input string</param>
        /// <returns>Sha1 hash. Use <see cref="HexUtils.ToString"/> to convert this hash into a string.</returns>
        public static byte[] Sha1String(string input)
        {
            byte[] bytes = System.Text.Encoding.UTF8.GetBytes(input);
            return Sha1(bytes);
        }

        /// <summary>
        /// Compute the Sha1 hash of a file.
        /// </summary>
        /// <param name="path">Path of the file</param>
        /// <param name="bufferSize">Size of the computation buffer (in bytes)</param>
        /// <returns>Sha1 hash. Use <see cref="HexUtils.ToString"/> to convert this hash into a string.</returns>
        public static byte[] Sha1File(string path, int bufferSize = 1000000)
        {
            // Buffer is compulsory in order to accelerate computation: cf. http://stackoverflow.com/questions/1177607/what-is-the-fastest-way-to-create-a-checksum-for-large-files-in-c-sharp
            using (var stream = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.Read, bufferSize))
            {
                return Sha1(stream);
            }
        }

        #endregion
    }
}
