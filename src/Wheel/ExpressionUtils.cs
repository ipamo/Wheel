﻿#if !NET20
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace Wheel
{
    public static class ExpressionUtils
    {
        /// <summary>
        /// Return the PropertyInfo refered by the given expression.
        /// </summary>
        /// <remarks>
        /// https://stackoverflow.com/questions/3567857/why-are-some-object-properties-unaryexpression-and-others-memberexpression
        /// </remarks>
        public static PropertyInfo GetExpressionProperty(LambdaExpression expression)
        {
            // Get the member expression
            MemberExpression memberExpression;
            if (expression.Body is MemberExpression)
                memberExpression = (MemberExpression) expression.Body;
            else if (expression.Body is UnaryExpression)
            {
                var expr = ((UnaryExpression) expression.Body).Operand;
                if (expr is MemberExpression)
                    memberExpression = (MemberExpression)expr;
                else
                    throw new InvalidOperationException(string.Format(
                        "Expression body operand for \"{0}\" is not a MemberExpression",
                        GetExpressionText(expression)
                    ));
            }
            else
                throw new InvalidOperationException(string.Format(
                    "Expression body for \"{0}\" is neither MemberExpression or UnaryExpression",
                    GetExpressionText(expression)
                ));

            // Get the property info
            if (memberExpression.Member is PropertyInfo)
                return (PropertyInfo)memberExpression.Member;
            else
                throw new InvalidOperationException(string.Format(
                    "Expression \"{0}\" does not refer to a property",
                    GetExpressionText(expression)
                ));
        }

        /// <summary>
        /// Return expression as a text.
        /// <para>Exemples:
        /// - For argument "x => x.Place", return "Place"
        /// - For argument "x => x.Place.CategoryId", return "Place.CategoryId"
        /// - For argument "x => x.Places[0].CategoryId", return "Places[0].CategoryId"
        /// </para>
        /// </summary>
        public static string GetExpressionText(LambdaExpression expression)
        {
            return string.Join(".", GetExpressionParts(expression.Body));
        }


        /// <summary>
        /// Return expression as a text.
        /// <para>Exemples:
        /// - For argument "x => x.Place", return "Place"
        /// - For argument "x => x.Place.CategoryId", return "Place.CategoryId"
        /// - For argument "x => x.Places[0].CategoryId", return "Places[0].CategoryId"
        /// </para>
        /// </summary>
        public static string GetExpressionText<T>(Expression<Func<T, object>> expression)
            => GetExpressionText((LambdaExpression)expression);

        private static string GetExpressionName(Expression e, out Expression parent)
        {
            if (e is MemberExpression m)
            {
                // Property or field           
                parent = m.Expression;
                return m.Member.Name;
            }
            else if (e is MethodCallExpression mc)
            {
                if (mc.Method.IsSpecialName)
                {
                    // For indexers
                    var argList = new List<string>();
                    foreach (var ma in mc.Arguments)
                    {
                        if (ma.NodeType == ExpressionType.MemberAccess)
                        {
                            var value = GetMemberExpressionValue((MemberExpression)ma);
                            argList.Add(value.ToString());
                        }
                        else
                        {
                            var arg = string.Join(",", GetExpressionParts(ma));
                            argList.Add(arg);
                        }
                    }

                    var args = string.Join(",", argList);
                    return $"{GetExpressionName(mc.Object, out parent)}[{args}]";
                }
                else
                {
                    // Other method calls
                    var args = string.Join(",", mc.Arguments.SelectMany(GetExpressionParts));
                    parent = mc.Object;
                    return $"{mc.Method.Name}({args})";
                }
            }
            else if (e is ConstantExpression c)
            {
                // Constant value
                parent = null;
                return c.Value?.ToString() ?? "null";
            }
            else if (e is UnaryExpression u)
            {
                // Convert
                parent = u.Operand;
                return null;
            }
            else
            {
                parent = null;
                return e.ToString();
            }
        }

        private static IEnumerable<string> GetExpressionParts(Expression e)
        {
            var list = new List<string>();
            while (e != null && !(e is ParameterExpression))
            {
                var name = GetExpressionName(e, out e);
                if (name != null)
                    list.Add(name);
            }
            list.Reverse();
            return list;
        }

        private static object GetMemberExpressionValue(MemberExpression member)
        {
            var objectMember = Expression.Convert(member, typeof(object));
            var getterLambda = Expression.Lambda<Func<object>>(objectMember);
            var getter = getterLambda.Compile();
            return getter();
        }
    }
}
#endif
