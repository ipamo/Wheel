﻿using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wheel;
using Wheel.Data.Tests;

namespace Wheel.Data.Tests
{
    public class DbContextFactory : IDesignTimeDbContextFactory<TestsDbContext>
    {
        public TestsDbContext CreateDbContext(string[] args)
        {
            var services = new Fixture().Services;            
            return services.GetRequiredService<TestsDbContext>();
        }
    }
}
