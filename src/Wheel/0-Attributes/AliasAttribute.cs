﻿using System;
using System.Collections.Generic;

namespace Wheel
{
    /// <summary>
    /// Specify one or more string alias on an Enum field value.
    /// </summary>
    [AttributeUsage(AttributeTargets.Field)]
    public class AliasAttribute : Attribute
    {
        public AliasAttribute(params string[] aliases)
        {
            if (aliases != null)
            {
                var list = new List<string>(aliases.Length);
                foreach (var alias in aliases)
                {
                    if (! list.Contains(alias))
                    {
                        list.Add(alias);
                    }
                }
                Aliases = list.ToArray();
            }
            else
            {
                Aliases = new string[] { };
            }
        }

        public string[] Aliases { get; }
    }
}
