﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;

namespace Wheel
{
    /// <summary>
    /// Standard return codes for success, warnings and errors.
    /// </summary>
    public static class ReturnCode
    {
        public const int OK = 0;
        public const int WARNING = (int)LogLevel.Warning;
        public const int ERROR = (int)LogLevel.Error;
        public const int CRITICAL = (int)LogLevel.Critical;

        // When these codes occur, the report email might not have arrived
        public const int GENERAL_ERROR = 10001;
        public const int COMMAND_TERMINATE_ERROR = 10002;
        public const int SEND_REPORT_ERROR = 10003;
    }
}
