﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.ComponentModel.DataAnnotations;
using Wheel;
using Wheel.Commands;

namespace Wheel.Commands.Tests
{
    /// <summary>
    /// Example of command implicitely defined using attributes, with Run(string[]) method.
    /// </summary>
    [Command(Description = "Log the list of arguments")]
    class StringArrayCommand
    {
        private ILogger _logger;

        public StringArrayCommand(ILogger<AddCommand> logger)
        {
            _logger = logger;
        }
        
        public void Run(string[] args)
        {
            _logger.LogInformation($"{args.Length} arguments: {string.Join(", ", args)}");
        }
    }
}
