﻿using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using Wheel.Internal;

namespace Wheel
{
    /// <summary>
    /// Build infrastructure services (environment, configuration, logging).
    /// </summary>
    public class WheelInfraBuilder
    {
        private readonly List<Action<HostBuilderContext, IConfigurationBuilder>> _configureAppConfigurationDelegates;
#if NET20
        private readonly List<Action<HostBuilderContext, ILoggerFactory>> _configureLoggingDelegates;
#else
        private readonly List<Action<HostBuilderContext, ILoggingBuilder>> _configureLoggingDelegates;
#endif
        private Func<IServiceCollection, IServiceProvider> _dependencyInjectionBuilder;

        private WheelInfra _infra;

        private WheelInfraBuilder()
        {
            _configureAppConfigurationDelegates = new List<Action<HostBuilderContext, IConfigurationBuilder>>();
#if NET20
            _configureLoggingDelegates = new List<Action<HostBuilderContext, ILoggerFactory>>();
#else
            _configureLoggingDelegates = new List<Action<HostBuilderContext, ILoggingBuilder>>();
#endif
        }

        public WheelInfra Build()
        {
            var sw = Stopwatch.StartNew();
            var subsw = Stopwatch.StartNew();

            _infra = new WheelInfra();
            BuildEnvironment();
            var environmentDuration = subsw.ElapsedMilliseconds;
            subsw.Reset();
            subsw.Start();

            BuildConfiguration();
            var configurationDuration = subsw.ElapsedMilliseconds;
            subsw.Reset();
            subsw.Start();

            BuildLogging();
            var loggingDuration = subsw.ElapsedMilliseconds;
            subsw.Reset();
            subsw.Start();

            if (_dependencyInjectionBuilder != null)
                _infra.DependencyInjectionBuilder = _dependencyInjectionBuilder;
            _infra.EndConfigure();
            var servicesDuration = subsw.ElapsedMilliseconds;
            subsw.Reset();
            subsw.Start();
            
            var logger = _infra.LoggerFactory.CreateLogger<WheelInfraBuilder>();
            logger.LogTrace("Built infra in {0} ms (environment: {1} ms, configuration: {2} ms, logging: {3} ms, services: {4} ms)", sw.ElapsedMilliseconds, environmentDuration, configurationDuration, loggingDuration, servicesDuration);
            return _infra;
        }

        private void BuildEnvironment()
        {
            var info = new ApplicationInformation();
            info.Assembly = ReflectionUtils.GetApplicationAssembly();
            info.Version = ReflectionUtils.GetAssemblyVersion(info.Assembly);
            info.Description = ReflectionUtils.GetAssemblyDescription(info.Assembly);
            _infra.ApplicationInformation = info;

            var env = new WheelEnvironment(info.Assembly);
            info.Name = env.ApplicationName;
            _infra.Environment = env;
        }

        private void BuildConfiguration()
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(_infra.Environment.ContentRootPath);

            var properties = new Dictionary<object, object>();
            var context = new HostBuilderContext(properties)
            {
                Configuration = null, // not available yet
                HostingEnvironment = _infra.Environment
            };

            foreach (var configure in _configureAppConfigurationDelegates)
            {
                configure(context, builder);
            }

            _infra.Configuration = builder.Build();
            _infra.BuilderContext = new HostBuilderContext(properties)
            {
                Configuration = _infra.Configuration, // now available
                HostingEnvironment = _infra.Environment
            };
        }

#if NET20
        private void BuildLogging()
        {
            _infra.ConfigureLogging((context, factory) =>
            {
                foreach (var configure in _configureLoggingDelegates)
                {
                    configure(context, factory);
                }
            });
        }
#else
        private void BuildLogging()
        {
            _infra.ConfigureLogging((context, builder) =>
            {
                foreach (var configure in _configureLoggingDelegates)
                {
                    configure(context, builder);
                }
            });
        }
#endif

        public WheelInfraBuilder ConfigureAppConfiguration(Action<HostBuilderContext, IConfigurationBuilder> configureAppConfiguration)
        {
            if (configureAppConfiguration == null)
                throw new ArgumentNullException(nameof(configureAppConfiguration));

            _configureAppConfigurationDelegates.Add(configureAppConfiguration);
            return this;
        }

        public WheelInfraBuilder ConfigureAppConfiguration(Action<IConfigurationBuilder> configureAppConfiguration)
        {
            if (configureAppConfiguration == null)
                throw new ArgumentNullException(nameof(configureAppConfiguration));

            return ConfigureAppConfiguration((_, builder) => configureAppConfiguration(builder));
        }

#if NET20
        public WheelInfraBuilder ConfigureLogging(Action<HostBuilderContext, ILoggerFactory> configureLogging)
        {
            if (configureLogging == null)
                throw new ArgumentNullException(nameof(configureLogging));

            _configureLoggingDelegates.Add(configureLogging);
            return this;
        }
        
        public WheelInfraBuilder ConfigureLogging(Action<ILoggerFactory> configureLogging)
        {
            if (configureLogging == null)
                throw new ArgumentNullException(nameof(configureLogging));

            return ConfigureLogging((_, factory) => configureLogging(factory));
        }

#else
        public WheelInfraBuilder ConfigureLogging(Action<HostBuilderContext, ILoggingBuilder> configureLogging)
        {
            if (configureLogging == null)
                throw new ArgumentNullException(nameof(configureLogging));

            _configureLoggingDelegates.Add(configureLogging);
            return this;
        }
        
        public WheelInfraBuilder ConfigureLogging(Action<ILoggingBuilder> configureLogging)
        {
            if (configureLogging == null)
                throw new ArgumentNullException(nameof(configureLogging));

            return ConfigureLogging((_, builder) => configureLogging(builder));
        }

#endif

        public WheelInfraBuilder UseDependencyInjectionBuilder(Func<IServiceCollection, IServiceProvider> dependencyInjectionBuilder)
        {
            if (dependencyInjectionBuilder == null)
                throw new ArgumentNullException(nameof(dependencyInjectionBuilder));

            _dependencyInjectionBuilder = dependencyInjectionBuilder;
            return this;
        }
        
        public static WheelInfraBuilder CreateDefaultBuilder()
        {
            return new WheelInfraBuilder()
                .ConfigureAppConfiguration(ConfigurationUtils.WheelConfigure)
                .ConfigureLogging(LoggingPlusUtils.WheelConfigure);
        }
    }
}
