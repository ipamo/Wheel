﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Wheel.Commands
{
    /// <summary>
    /// Configure a class to become a command.
    /// </summary>
    /// <remarks>
    /// The command must contain an instance method named <c>Run</c> that will be called with the command arguments.
    /// Options can be defined by applying the <see cref="CommandOptionAttribute"/> to the properties of the class.
    /// </remarks>
    [AttributeUsage(AttributeTargets.Class)]
    public class CommandAttribute : Attribute
    {
        /// <summary>
        /// Indicate that a type is a command.
        /// </summary>
        /// <param name="name">Name for the command. If not set, the name will be automatically set based on the command type name.</param>
        public CommandAttribute(string name = null)
        {
            Name = name;
        }

        /// <summary>
        /// Name for the command. If not set, the name will be automatically set based on the command type name.
        /// </summary>
        public string Name { get; }

        /// <summary>
        /// Short description of the command. Displayed in the list of commands in the parent's help text.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Detailed description of the behaviour of the command. Displayed under <see cref="Description"/> in the command's help text.
        /// <para>
        /// No need to add a leading or trailing new line.
        /// </para>
        /// </summary>
        public string HelpText { get; set; }
    }
}
