﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using Wheel;
using Wheel.Commands;

namespace HelloWorld
{
    class Program
    {
        static int Main(string[] args)
            => CommandApplication.CreateDefaultBuilder()
                .UseStartup<Startup>()
                .Build()
                .Run(args);
    }
}
