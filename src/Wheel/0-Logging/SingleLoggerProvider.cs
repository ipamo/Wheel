﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Wheel
{
    /// <summary>
    /// An implementation of Microsoft.Extensions.Logging's <see cref="ILoggerProvider"/> that always return
    /// the same <see cref="ILogger"/> object.
    /// </summary>
    public class SingleLoggerProvider : ILoggerProvider
    {
        private ILogger _logger;

        public SingleLoggerProvider(ILogger logger)
        {
            _logger = logger;
        }

        public ILogger CreateLogger(string categoryName)
        {
            return _logger;
        }

        public void Dispose()
        {

        }
    }
}
