﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using Wheel.Testing;

namespace Wheel.Tests
{
    public class StringUtilsTests
    {
        [Fact]
        public void Snake()
        {
            Assert.Equal("date_time", "DateTime".ToLowerSnake());
            Assert.Equal("date_time", "dateTime".ToLowerSnake());
            Assert.Equal("date_time", "date_time".ToLowerSnake());
            Assert.Equal("date_time", "DATE_TIME".ToLowerSnake());

            Assert.Equal("DATE_TIME", "DateTime".ToUpperSnake());
            Assert.Equal("DATE_TIME", "dateTime".ToUpperSnake());
            Assert.Equal("DATE_TIME", "date_time".ToUpperSnake());
            Assert.Equal("DATE_TIME", "DATE_TIME".ToUpperSnake());
        }
    }
}
