﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System.Threading;
using Microsoft.Extensions.Hosting;

namespace Wheel.Commands.Internal
{
    internal sealed class CommandApplicationBuilder : ICommandApplicationBuilder
    {
        private readonly List<Action<HostBuilderContext, IConfigurationBuilder>> _configureAppConfigurationDelegates;
#if NET20
        private readonly List<Action<HostBuilderContext, ILoggerFactory>> _configureLoggingDelegates;
#else
        private readonly List<Action<HostBuilderContext, ILoggingBuilder>> _configureLoggingDelegates;
#endif
        private readonly List<Action<CommandApplicationOptions>> _configureOptionsDelegates;
        private readonly List<Action<ICommandBuilder>> _configureRootCommandDelegates;
        private readonly List<Action<HostBuilderContext, IServiceCollection>> _configureServicesDelegates;
        private WheelInfra _infra;
        private Func<IServiceCollection, IServiceProvider> _diBuilder;
        private ILogger _logger;

        public CommandApplicationBuilder()
        {
            _configureAppConfigurationDelegates = new List<Action<HostBuilderContext, IConfigurationBuilder>>();
#if NET20
            _configureLoggingDelegates = new List<Action<HostBuilderContext, ILoggerFactory>>();
#else
            _configureLoggingDelegates = new List<Action<HostBuilderContext, ILoggingBuilder>>();
#endif
            _configureOptionsDelegates = new List<Action<CommandApplicationOptions>>();
            _configureRootCommandDelegates = new List<Action<ICommandBuilder>>();
            _configureServicesDelegates = new List<Action<HostBuilderContext, IServiceCollection>>();
        }

        public ICommandApplicationBuilder UseInfra(WheelInfra infra)
        {
            if (infra == null)
                throw new ArgumentNullException(nameof(infra));

            _infra = infra;
            return this;
        }

        public ICommandApplicationBuilder ConfigureAppConfiguration(Action<HostBuilderContext, IConfigurationBuilder> configureAppConfiguration)
        {
            if (configureAppConfiguration == null)
                throw new ArgumentNullException(nameof(configureAppConfiguration));

            _configureAppConfigurationDelegates.Add(configureAppConfiguration);
            return this;
        }

        public ICommandApplicationBuilder ConfigureAppConfiguration(Action<IConfigurationBuilder> configureAppConfiguration)
        {
            if (configureAppConfiguration == null)
                throw new ArgumentNullException(nameof(configureAppConfiguration));

            return ConfigureAppConfiguration((_, builder) => configureAppConfiguration(builder));
        }

#if NET20
        public ICommandApplicationBuilder ConfigureLogging(Action<HostBuilderContext, ILoggerFactory> configureLogging)
        {
            if (configureLogging == null)
                throw new ArgumentNullException(nameof(configureLogging));

            _configureLoggingDelegates.Add(configureLogging);
            return this;
        }
        
        public ICommandApplicationBuilder ConfigureLogging(Action<ILoggerFactory> configureLogging)
        {
            if (configureLogging == null)
                throw new ArgumentNullException(nameof(configureLogging));

            return ConfigureLogging((_, factory) => configureLogging(factory));
        }

#else
        public ICommandApplicationBuilder ConfigureLogging(Action<HostBuilderContext, ILoggingBuilder> configureLogging)
        {
            if (configureLogging == null)
                throw new ArgumentNullException(nameof(configureLogging));

            _configureLoggingDelegates.Add(configureLogging);
            return this;
        }

        public ICommandApplicationBuilder ConfigureLogging(Action<ILoggingBuilder> configureLogging)
        {
            if (configureLogging == null)
                throw new ArgumentNullException(nameof(configureLogging));

            return ConfigureLogging((_, builder) => configureLogging(builder));
        }

#endif

        public ICommandApplicationBuilder UseDependencyInjectionBuilder(Func<IServiceCollection, IServiceProvider> diBuilder)
        {
            if (diBuilder == null)
                throw new ArgumentNullException(nameof(diBuilder));

            _diBuilder = diBuilder;
            return this;
        }

        public ICommandApplicationBuilder ConfigureServices(Action<IServiceCollection> configureServices)
        {
            if (configureServices == null)
                throw new ArgumentNullException(nameof(configureServices));

            return ConfigureServices((_, services) => configureServices(services));
        }
        
        public ICommandApplicationBuilder ConfigureServices(Action<HostBuilderContext, IServiceCollection> configureServices)
        {
            if (configureServices == null)
                throw new ArgumentNullException(nameof(configureServices));

            _configureServicesDelegates.Add(configureServices);
            return this;
        }

        public ICommandApplicationBuilder ConfigureOptions(Action<CommandApplicationOptions> configure)
        {
            if (configure == null)
                throw new ArgumentNullException(nameof(configure));
            _configureOptionsDelegates.Add(configure);
            return this;
        }

        public ICommandApplicationBuilder ConfigureRootCommand(Action<ICommandBuilder> configureRootCommand)
        {
            _configureRootCommandDelegates.Add(configureRootCommand);
            return this;
        }
        
        public ICommandApplication Build()
        {
            try
            {
                var sw = Stopwatch.StartNew();
                var subsw = Stopwatch.StartNew();

                // ------------------------------------------------------------
                // Build infrastructure

                WheelInfra infra;
                if (_infra != null)
                {
                    if (_configureAppConfigurationDelegates.Any())
                        throw new InvalidOperationException("ConfigureAppConfiguration method ignored when UseInfra is used.");
                    if (_configureLoggingDelegates.Any())
                        throw new InvalidOperationException("ConfigureLogging method ignored when UseInfra is used.");
                    if (_diBuilder != null)
                        throw new InvalidOperationException("UseDependencyInjectionBuilder method ignored when UseInfra is used.");
                    infra = _infra;
                }
                else
                {
                    var infraBuilder = WheelInfraBuilder.CreateDefaultBuilder()
                                        .ConfigureAppConfiguration((context, builder) =>
                                        {
                                            foreach (var configure in _configureAppConfigurationDelegates)
                                                configure(context, builder);
                                        })
                                        .ConfigureLogging((context, builder) =>
                                        {
                                            foreach (var configure in _configureLoggingDelegates)
                                                configure(context, builder);
                                        });
                    if (_diBuilder != null)
                        infraBuilder.UseDependencyInjectionBuilder(_diBuilder);
                    infra = infraBuilder.Build();
                }
                
                _logger = infra.LoggerFactory.CreateLogger<ILogger<CommandApplicationBuilder>>();

                var infraDuration = subsw.ElapsedMilliseconds;
                subsw.Reset();
                subsw.Start();

                // ------------------------------------------------------------
                // Configure application (services, options, root command)

                foreach (var configure in _configureServicesDelegates)
                    configure(infra.BuilderContext, infra.ServiceCollection);

                var options = infra.Configuration.GetSection("CommandApplication")
                    .Get<CommandApplicationOptions>();
                if (options == null)
                    options = new CommandApplicationOptions();
                foreach (var configure in _configureOptionsDelegates)
                    configure(options);
                
                var root = new CommandBuilder(infra, options);
                foreach (var configure in _configureRootCommandDelegates)
                    configure(root);
                
                var configureDuration = subsw.ElapsedMilliseconds;
                subsw.Reset();
                subsw.Start();

                // ------------------------------------------------------------
                // Build application services

                long buildServicesDuration;
                if (infra.IsDependencyInjectionEnabled)
                {
                    infra.BuildApplicationServices();
                    buildServicesDuration = subsw.ElapsedMilliseconds;
                }
                else
                {
                    buildServicesDuration = 0;
                }

                var app = new CommandApplication(root, options, infra);
                _logger.LogTrace("Built command application {0} ({1}) in {2} ms (infra: {3} ms, configure: {4} ms, build services: {5} ms)", infra.ApplicationInformation.Name, infra.Environment.EnvironmentName, sw.ElapsedMilliseconds, infraDuration, configureDuration, buildServicesDuration);
                return app;
            }
            catch (Exception e)
            {
                HandleException(e);
                NLog.LogManager.Flush();
                throw;
            }
        }

        /// <summary>
        /// Log exception raised during application initialization.
        /// </summary>
        /// <remarks>
        /// These exceptions are critical because they prevent execution of the application.
        /// </remarks>
        private void HandleException(Exception e)
        {
            var level = LogLevel.Critical;
            var message = string.Format("{0} raised during application initialization: {1}", e.GetType().Name, e.Message);

            if (e is MessageException)
            {
                if (e.InnerException != null)
                    LoggingUtils.SafeLog(_logger, level, e.InnerException, message);
                else
                    LoggingUtils.SafeLog(_logger, level, message);
            }
            else
            {
                LoggingUtils.SafeLog(_logger, level, e, message);
            }
        }
    }
}
