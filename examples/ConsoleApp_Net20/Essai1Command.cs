﻿using System;
using System.Collections.Generic;
using System.Text;
using Wheel.Commands;

namespace ConsoleApp_Net20
{
    class Essai1Command
    {
        public static void ConfigureCommand(ICommandBuilder cmd)
        {
            cmd.Description = "A command configured using reflection";
            cmd.Action = Run;
        }

        public static void Run()
        {
            Console.WriteLine("Hello world from essai1");
        }
    }
}
