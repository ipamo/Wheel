﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Net;
using System.Threading;

namespace Wheel
{
    /// <summary>
    /// An object that can subscribe to the logging system and report the number of log messages
    /// encountered for each log level.
    /// </summary>
    public interface IReporter : ILogger
    {
        /// <summary>
        /// Get the number of reported logs for each enabled level.
        /// </summary>
        Dictionary<LogLevel, int> Count { get; }

        /// <summary>
        /// Shortcut for Count[LogLevel.Critical].
        /// </summary>
        int CountCriticals { get; }

        /// <summary>
        /// Shortcut for Count[LogLevel.Error].
        /// </summary>
        int CountErrors { get; }

        /// <summary>
        /// Shortcut for Count[LogLevel.Warning].
        /// </summary>
        int CountWarnings { get; }

        /// <summary>
        /// Shortcut for Count[>=LogLevel.Warning].
        /// </summary>
        int CountProblems { get; }
    }
}
