﻿#if !NET20
using System;
using System.Collections.Generic;
using System.Text;

namespace Wheel
{
    /// <summary>
    /// Register an implementation of this service to inline CSS of HTML emails sent with <see cref="EmailSender"/>.
    /// </summary>
    public interface ICssInliner
    {
        string Inline(string html);
    }
}
#endif
