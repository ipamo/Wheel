﻿using Wheel.Data;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;
using Xunit.Abstractions;
using Wheel.Testing;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Wheel.Data.Tests.Models;

namespace Wheel.Data.Tests
{
    [Collection(nameof(FixtureCollection))]
    public class DbTests
    {
        protected readonly IDbms _dbms;
        protected readonly UserManager<User> _userManager;
        protected readonly TestsDbContext _ctx;
        protected readonly ITestOutputHelper _output;

        public DbTests(Fixture fx, ITestOutputHelper output)
        {
            _userManager = fx.Services.GetRequiredService<UserManager<User>>();
            _ctx = fx.Services.GetRequiredService<TestsDbContext>();
            _dbms = fx.Services.GetRequiredService<IDbms>();
            _output = output;
        }

        [DbFact]
        public virtual void GetComponent()
        {
            var datetime = DateTime.Now.ToLongTimeString();

            _dbms.GetComponentId($"test");
            _dbms.GetComponentId($"test-{datetime}");
            Assert.True(true); // No exception raised
        }

        [DbFact]
        public virtual void PositionalParameters()
        {
            var table = _dbms.GetTableName(typeof(Dict));
            var idColumn = _dbms.GetColumnName(typeof(Dict), nameof(Dict.Id));
            var valueColumn = _dbms.GetColumnName(typeof(Dict), nameof(Dict.Value));
            var insertedThroughIdColumn =_dbms.GetColumnName(typeof(Dict), _dbms.InsertedThroughIdProperty);

            var datetime = DateTime.Now.ToLongTimeString();
            var id = $"PositionalParameters-{datetime}";
            var value = datetime;
            var componentId = _dbms.GetComponentId<DbTests>();

            _dbms.NonQuery($"INSERT INTO {table} ({idColumn}, {valueColumn}, {insertedThroughIdColumn}) VALUES (@1, @2, @3)", id, value, componentId);

            var actual = (string)_dbms.QueryScalar($"SELECT {valueColumn} FROM {table} WHERE {idColumn} = @1", id);

            Assert.Equal(datetime, actual);
        }

        [DbFact]
        public virtual void NamedParameters()
        {
            var table = _dbms.GetTableName(typeof(Dict));
            var idColumn = _dbms.GetColumnName(typeof(Dict), nameof(Dict.Id));
            var valueColumn = _dbms.GetColumnName(typeof(Dict), nameof(Dict.Value));
            var insertedThroughIdColumn = _dbms.GetColumnName(typeof(Dict), _dbms.InsertedThroughIdProperty);

            var datetime = DateTime.Now.ToLongTimeString();
            var id = $"NamedParameters-{datetime}";
            var value = datetime;
            var componentId = _dbms.GetComponentId<DbTests>();

            _dbms.NonQuery($"INSERT INTO {table} ({idColumn}, {valueColumn}, {insertedThroughIdColumn}) VALUES (@id, @value, @componentId)", new Dictionary<string, object> {
                { "@id", id },
                { "@value", value },
                { "@componentId", componentId },
            });

            var actual = (string)_dbms.QueryScalar($"SELECT {valueColumn} FROM {table} WHERE {idColumn} = @id", new Dictionary<string, object> {
                { "@id", id }
            });

            Assert.Equal(datetime, actual);
        }

        [DbFact]
        public virtual void SetDict()
        {
            var datetime = DateTime.Now.ToLongTimeString();

            _dbms.SetDictValue($"test-{datetime}", "success");
            Assert.Equal("success", _dbms.GetDictValue($"test-{datetime}"));

            _dbms.SetDictValue($"test", datetime);
            Assert.Equal(datetime, _dbms.GetDictValue($"test"));
        }

        [DbFact]
        public virtual void ListUsersFromUserManager()
        {
            try
            {
                foreach (var user in _userManager.Users)
                {
                    _output.WriteLine($"{user.UserName} ({user.Email}), password: {(user.PasswordHash != null ? "yes" : "no")}");
                }
                Assert.True(true);
            }
            catch (Exception e)
            {
                _output.WriteLine(e.ToString());
                Assert.True(false, e.Message);
            }
        }

        [DbFact]
        public virtual void AddOrModifyUser()
        {
            var userName = "test";
            User user;

            if ((user = _userManager.FindByNameAsync(userName).Result) != null)
            {
                Assert.True(true, "User found");

                // Modify
                user.PhoneNumber = DateTime.Now.ToLongTimeString();
                _userManager.UpdateAsync(user).Wait();
                Assert.True(true, "User updated");
            }
            else
            {
                // Create
                var result = _userManager.CreateAsync(new User { UserName = userName, Email = "test@example.com" }).Result;
                Assert.Equal(IdentityResult.Success, result);

                // Find
                user = _userManager.FindByNameAsync(userName).Result;
                Assert.NotNull(user);
            }
        }

        [DbFact]
        public virtual void ListUsersFromContext()
        {
            try
            {
                foreach (var user in _ctx.Users)
                {
                    _output.WriteLine($"{user.UserName} ({user.Email}), password: {(user.PasswordHash != null ? "yes" : "no")}");
                }
                Assert.True(true);
            }
            catch (Exception e)
            {
                _output.WriteLine(e.ToString());
                Assert.True(false, e.Message);
            }
        }
    }
}
