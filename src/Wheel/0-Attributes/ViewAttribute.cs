﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Wheel
{
    /// <summary>
    /// Map an entity to a view.
    /// <para>
    /// See: https://docs.microsoft.com/en-us/ef/core/modeling/keyless-entity-types.
    /// </para>
    /// </summary>
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = false)]
    public class ViewAttribute : Attribute
    {
        /// <summary>
        /// Map an entity to a view.
        /// </summary>
        /// <param name="name">The name of the view.</param>
        public ViewAttribute(string name = null, string schema = null)
        {
            Name = name;
            Schema = schema;
        }

        /// <summary>
        /// The name of the view.
        /// </summary>
        public string Name { get; }

        /// <summary>
        /// The schema of the view.
        /// </summary>
        public string Schema { get; }
    }
}
