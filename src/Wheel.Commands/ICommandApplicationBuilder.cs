﻿using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;

namespace Wheel.Commands
{
    /// <summary>
    /// Command-line application builder.
    /// </summary>
    public interface ICommandApplicationBuilder
    {
        /// <summary>
        /// Build a <see cref="ICommandApplication"/>.
        /// </summary>
        ICommandApplication Build();

        /// <summary>
        /// Register the application infrastructure to use.
        /// </summary>
        ICommandApplicationBuilder UseInfra(WheelInfra infra);

        /// <summary>
        /// Adds a delegate for configuring the configuration of the application infrastructure.
        /// <para>
        /// Ignored if <see cref="UseInfra"/> is used.
        /// </para>
        /// </summary>
        ICommandApplicationBuilder ConfigureAppConfiguration(Action<HostBuilderContext, IConfigurationBuilder> configureAppConfiguration);

        /// <summary>
        /// Adds a delegate for configuring the configuration of the application infrastructure.
        /// <para>
        /// Ignored if <see cref="UseInfra"/> is used.
        /// </para>
        /// </summary>
        ICommandApplicationBuilder ConfigureAppConfiguration(Action<IConfigurationBuilder> configureAppConfiguration);

#if NET20
        /// <summary>
        /// Adds a delegate for configuring the logging of the application infrastructure.
        /// <para>
        /// Ignored if <see cref="UseInfra"/> is used.
        /// </para>
        /// </summary>
        ICommandApplicationBuilder ConfigureLogging(Action<HostBuilderContext, ILoggerFactory> configureLogging);
        
        /// <summary>
        /// Adds a delegate for configuring the logging of the application infrastructure.
        /// <para>
        /// Ignored if <see cref="UseInfra"/> is used.
        /// </para>
        /// </summary>
        ICommandApplicationBuilder ConfigureLogging(Action<ILoggerFactory> configureLogging);

#else
        /// <summary>
        /// Adds a delegate for configuring the logging of the application infrastructure.
        /// <para>
        /// Ignored if <see cref="UseInfra"/> is used.
        /// </para>
        /// </summary>
        ICommandApplicationBuilder ConfigureLogging(Action<HostBuilderContext, ILoggingBuilder> configureLogging);

        /// <summary>
        /// Adds a delegate for configuring the logging of the application infrastructure.
        /// <para>
        /// Ignored if <see cref="UseInfra"/> is used.
        /// </para>
        /// </summary>
        ICommandApplicationBuilder ConfigureLogging(Action<ILoggingBuilder> configureLogging);

#endif

        /// <summary>
        /// Register the dependency injection builder to use with the application infrastructure.
        /// <para>
        /// Ignored if <see cref="UseInfra"/> is used.
        /// </para>
        /// </summary>
        ICommandApplicationBuilder UseDependencyInjectionBuilder(Func<IServiceCollection, IServiceProvider> diBuilder);

        /// <summary>
        /// Adds a delegate for configuring additional services for the command application.
        /// </summary>
        ICommandApplicationBuilder ConfigureServices(Action<IServiceCollection> configureServices);

        /// <summary>
        /// Adds a delegate for configuring additional services for the command application.
        /// </summary>
        ICommandApplicationBuilder ConfigureServices(Action<HostBuilderContext, IServiceCollection> configureServices);

        /// <summary>
        /// Adds a delegate for configuring the application options.
        /// </summary>
        ICommandApplicationBuilder ConfigureOptions(Action<CommandApplicationOptions> configure);

        /// <summary>
        /// Adds a delegate for configuring the root command.
        /// </summary>
        ICommandApplicationBuilder ConfigureRootCommand(Action<ICommandBuilder> configureRootCommand);
    }
}
