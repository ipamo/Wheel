﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Wheel
{
    public static class TypeUtils
    {
        /// <summary>
        /// Return the nullable type of the given type.
        /// </summary>
        public static Type GetNullableType(this Type type)
        {
            // Use Nullable.GetUnderlyingType() to remove the Nullable<T> wrapper if type is already nullable.
            type = Nullable.GetUnderlyingType(type) ?? type; // avoid type becoming null
            if (type.IsValueType)
                return typeof(Nullable<>).MakeGenericType(type);
            else
                return type;
        }


        /// <summary>
        /// Retrieves the default value for a given Type
        /// </summary>
        /// <param name="type">The Type for which to get the default value</param>
        /// <returns>The default value for <paramref name="type"/></returns>
        /// <remarks>
        /// If a null Type, a reference Type, or a System.Void Type is supplied, this method always returns null.  If a value type 
        /// is supplied which is not publicly visible or which contains generic parameters, this method will fail with an 
        /// exception.
        /// </remarks>
        public static object GetDefaultValue(this Type type)
        {
            // If no Type was supplied, if the Type was a reference type, or if the Type was a System.Void, return null
            if (type == null || !type.IsValueType || type == typeof(void))
                return null;

            // If the supplied Type has generic parameters, its default value cannot be determined
            if (type.ContainsGenericParameters)
                throw new ArgumentException("Default value cannot be retrieved for type " + type +
                    " because it contains generic parameters");

            // If the Type is a primitive type, or if it is another publicly-visible value type (i.e. struct/enum), return a 
            // default instance of the value type
            if (type.IsPrimitive || !type.IsNotPublic)
            {
                try
                {
                    return Activator.CreateInstance(type);
                }
                catch (Exception e)
                {
                    throw new ArgumentException("Default value cannot be retrieved for type " + type, e);
                }
            }

            // Fail with exception
            throw new ArgumentException("Default value cannot be retrieved for type " + type + " because it is not a publicly-visible type");
        }

        /// <summary>
        /// Reports whether a value of type T (or a null reference of type T) contains the default value for that Type
        /// </summary>
        /// <remarks>
        /// Reports whether the object is empty or unitialized for a reference type or nullable value type (i.e. is null) or 
        /// whether the object contains a default value for a non-nullable value type (i.e. int = 0, bool = false, etc.)
        /// <para></para>
        /// NOTE: For non-nullable value types, this method introduces a boxing/unboxing performance penalty.
        /// </remarks>
        /// <param name="type">Type of the object to test</param>
        /// <param name="value">The object value to test, or null for a reference Type or nullable value Type</param>
        public static bool IsObjectSetToDefault(this Type type, object value)
        {
            // If no type was supplied, attempt to determine from value
            if (type == null)
            {
                // If no value was supplied, abort
                if (value == null)
                {
                    throw new ArgumentNullException(nameof(type));
                }

                // Determine type from value
                type = value.GetType();
            }

            // Get the default value of type ObjectType
            object defaultForType = type.GetDefaultValue();

            // If a non-null value was supplied, compare Value with its default value and return the result
            if (value != null)
                return value.Equals(defaultForType);
            else
                // Since a null value was supplied, report whether its default value is null
                return defaultForType == null;
        }
    }
}
