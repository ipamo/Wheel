﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Wheel.Commands
{
    /// <summary>
    /// Configure a property to become a command option. The class of the property must have the <see cref="CommandAttribute"/>.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class CommandOptionAttribute : Attribute
    {
        public CommandOptionAttribute(string template = null)
        {
            Template = template;
        }

        /// <summary>
        /// Name(s) of the option, for example: <c>-o|--option</c>.
        /// </summary>
        public string Template { get; }

        /// <summary>
        /// Short description of the option displayed in help text. For example: <c>An option.</c>.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Indicate whether the option is required. If it is required and not given, a <see cref="CommandParserException"/> will be raised
        /// when using <see cref="CommandOption.Value"/> property.
        /// </summary>
        public bool Required { get; set; }

        /// <summary>
        /// When disabled, the option does not appear in the list of options and argument of the command's help text. Enabled by default.
        /// </summary>
        public bool ShowInHelp { get; set; } = true;
    }
}
