﻿using System;
using System.Collections.Generic;

namespace Wheel
{
    public static class CollectionUtils
    {
        public static TValue GetOrDefault<TKey, TValue>(this IDictionary<TKey, TValue> dict,
            TKey key, TValue defaultValue = default(TValue))
        {
            if (dict == null)
                return defaultValue;
            if (dict.TryGetValue(key, out TValue value))
                return value;
            return defaultValue;
        }

        public static TValue GetOrDefault<TKey, TValue>(this IDictionary<TKey, TValue> dict,
             TKey key, Func<TValue> defaultValueProvider)
        {
            if (dict == null)
                return defaultValueProvider();
            if (dict.TryGetValue(key, out TValue value))
                return value;
            return defaultValueProvider();
        }
    }
}
