﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Wheel
{
    /// <summary>
    /// Specify that a column holds decimal values.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class DecimalAttribute : Attribute
    {
        public DecimalAttribute(int precision, int scale)
        {
            Precision = precision;
            Scale = scale;
        }

        public int Precision { get; }
        public int Scale { get; }
    }
}
