﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Microsoft.Extensions.Logging
{
    internal static class Resource
    {
        public static string FormatUnexpectedNumberOfNamedParameters(params object[] args)
        {
            return string.Format("The format string '{0}' does not have the expected number of named parameters. Expected {1} parameter(s) but found {2} parameter(s).", args);
        }
    }
}
