﻿using System;
using Microsoft.Extensions.DependencyInjection;
using Wheel;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Configuration;
using System.Threading;
using Wheel.Commands;

namespace ConsoleApp
{
    class Program
    {
        static int Main(string[] args)
            => CommandApplication.CreateDefaultBuilder()
                .UseStartup<Program>()
                .Build()
                .Run(args);

        public static void ConfigureCommands(ICommandBuilder root)
        {
            root.DefaultCommand<Essai1Command>();

            root.Command("essai2", cmd =>
            {
                cmd.Description = "A command configured using actions";
                cmd.Action = () =>
                {
                    Console.WriteLine("Hello world from essai2");
                };
            });

            root.Command<Essai3Command>();
        }
    }
}
