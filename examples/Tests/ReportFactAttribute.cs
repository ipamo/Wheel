﻿using Xunit;

namespace Wheel.Testing
{
    /// <summary>
    /// A xunit fact skipped if settings directive "Tests:SkipReport" is enabled.
    /// </summary>
    public class ReportFactAttribute : FactAttribute
    {
        public ReportFactAttribute()
            : base()
        {
            Skip = TestingInit.SkipReport;
        }
    }
}
