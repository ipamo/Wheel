﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Wheel.Data
{
    public static class DbSetUtils
    {

        /// <summary>
        /// Search for an object, first in DbSet.Local, and if not found in DbSet.
        /// </summary>
        /// <typeparam name="T">Entity type</typeparam>
        /// <param name="set">EntityFramework DbSet</param>
        /// <param name="predicate">Search condition</param>
        /// <returns></returns>
        public static async Task<T> FirstOrDefaultIncludingLocalAsync<T>(this DbSet<T> set, Expression<Func<T, bool>> predicate)
            where T : class
        {
            var entity = set.Local.FirstOrDefault(predicate.Compile());
            if (entity != default(T))
                return entity;

            return await set.FirstOrDefaultAsync(predicate);
        }
    }
}
