﻿// Adapted from .NET Foundation's CommandLineUtils.
// Copyright (c) .NET Foundation. All rights reserved.
// Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.

using System;

namespace Wheel.Commands
{
    /// <summary>
    /// Exception raised in case of problem related to the definition of commands.
    /// </summary>
    public class CommandDefinitionException : MessageException
    {
        public CommandDefinitionException(string message)
            : base(message)
        {

        }
    }
}
