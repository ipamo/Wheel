﻿using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.FileProviders;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;

namespace Wheel.Internal
{
    internal class WheelEnvironment : IHostEnvironment
    {
        public string EnvironmentName { get; set; }
        public string ApplicationName { get; set; }
        public string WebRootPath { get; set; }
        public IFileProvider WebRootFileProvider { get; set; }
        public string ContentRootPath { get; set; }
        public IFileProvider ContentRootFileProvider { get; set; }

        public WheelEnvironment(Assembly applicationAssembly = null)
        {
            ApplicationName = (applicationAssembly ?? ReflectionUtils.GetApplicationAssembly()).GetName().Name;

            var lower = ApplicationName.ToLower();
            if (lower.Contains(".test") || lower.StartsWith("test"))
                EnvironmentName = "Testing";
            else
                EnvironmentName = GetDefaultEnvironmentName();

            ContentRootPath = GetDefaultContentRootPath();
        }

        private static string GetDefaultEnvironmentName()
        {
            var environmentName = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT")?.Trim();
            if (string.IsNullOrEmpty(environmentName))
                environmentName = "Production";
            return environmentName;
        }

        private static string GetDefaultContentRootPath()
        {
            var contentRootPath = Environment.GetEnvironmentVariable("ASPNETCORE_CONTENTROOT")?.Trim();
            if (string.IsNullOrEmpty(contentRootPath))
                contentRootPath = Directory.GetCurrentDirectory();
            return contentRootPath;
        }
    }
}
