﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;
using Wheel.Commands.Tests.Samples;
using Wheel.Testing;
using Xunit;
using Xunit.Abstractions;

namespace Wheel.Commands.Tests
{
    public class ReportTests
    {
        private readonly TestLogger _logger;

        public ReportTests(ITestOutputHelper output)
        {
            _logger = new TestLogger(output);
        }

        [ReportFact]
        public void Send()
        {
            var app = CommandApplication.CreateDefaultBuilder()
                .UseStartup<Startup>()
                .ConfigureLogging(builder =>
                {
                    builder.SetMinimumLevel(LogLevel.Trace);
                    builder.AddProvider(new SingleLoggerProvider(_logger));
                })
                .ConfigureOptions(o =>
                {
                    o.ReportEmailEnabled = true;
                    o.ReportEmailTo = TestsInit.Configuration["CommandApplication:ReportEmailTo"];
                })
                .ConfigureServices((context, services) =>
                {
                    services.AddOptions();
                    services.Configure<SmtpOptions>(context.Configuration.GetSection("Smtp"));
                    services.AddSingleton<IEmailSender, EmailSender>();
                    services.AddSingleton<ICssInliner, CssInliner>();
                })
                .Build()
                .Run("direct");
            
            Assert.Contains("[ERROR] Test direct", _logger.Messages);
            Assert.Equal(2, _logger.CountProblems);
        }
    }
}
