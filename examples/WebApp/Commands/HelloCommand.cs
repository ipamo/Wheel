﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wheel.Commands;

namespace WebApp.Commands
{
    class HelloCommand
    {
        public static void ConfigureCommand(ICommandBuilder cmd)
        {
            cmd.Description = "Example of a command in the same assembly as the web application";
            cmd.Action = () => Console.WriteLine("Hello, world!");
        }
    }
}
