﻿using System;

namespace Wheel.Commands
{
    /// <summary>
    /// Exception raised during command line parsing.
    /// </summary>
    public class CommandParserException : MessageException
    {
        public CommandParserException(string message)
            : base(message)
        {

        }
    }
}
