﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.CompilerServices;
using Npgsql;
using Microsoft.Extensions.Logging;

namespace Wheel.Data
{
    public static class PostgreSqlLoggingUtils
    {
        private static LogLevel GetLevel(string level)
        {
            switch (level)
            {
                case "DEBUG":
                    return LogLevel.Trace;
                case "LOG":
                    return LogLevel.Debug;
                case "INFO":
                case "NOTICE":
                    return LogLevel.Information;
                case "WARNING":
                case "ATTENTION":
                    return LogLevel.Warning;
                case "EXCEPTION":
                    return LogLevel.Critical;
                default:
                    return LogLevel.Error;
            }
        }

        /// <summary>
        /// Write a C# log from a Npgsql notice.
        /// </summary>
        /// <remarks>
        /// Conventions:
        /// 
        /// 	RAISE DEBUG 'format', expression, ...;
        /// 	RAISE LOG 'format', expression, ...;
        /// 	RAISE INFO 'format', expression, ...;
        /// 	RAISE WARNING 'format', expression, ...;
        /// 	RAISE EXCEPTION 'format', expression, ...;
        /// 
        /// Cf. https://www.postgresql.org/docs/current/static/plpgsql-errors-and-messages.html
        /// 
        /// </remarks>
        public static void LogPostgresSql(this ILogger logger, PostgresNotice e, string prefix = null)
        {
            var level = GetLevel(e.Severity);

            string fullPrefix;
            if (prefix == "")
                fullPrefix = "";
            else if (prefix != null)
                fullPrefix = $"{prefix}";
            //else if (!string.IsNullOrEmpty(e.Routine) && !e.Routine.StartsWith("p_log"))
            //    fullPrefix = $"[{e.Routine}, line {e.Line}] ";
            else
                fullPrefix = "";

            var message = e.MessageText;
            if (message != null)
            {
                if (level == LogLevel.Trace && message.StartsWith("[TRACE] "))
                    message = message.Substring(8);
                else if (level == LogLevel.Debug && message.StartsWith("[DEBUG] "))
                    message = message.Substring(8);
                else if (level == LogLevel.Information && message.StartsWith("[ INFO] "))
                    message = message.Substring(8);
                else if (level == LogLevel.Warning && message.StartsWith("[ WARN] "))
                    message = message.Substring(8);
                else if (level == LogLevel.Error && message.StartsWith("[ERROR] "))
                    message = message.Substring(8);
                else if (level == LogLevel.Critical && message.StartsWith("[CRITICAL] "))
                    message = message.Substring(8);
            }
            
            logger.Log(level, default(EventId), $"{fullPrefix}{message}", null, MessageFormatter);
        }

        /// <summary>
        /// Write a C# log from a Npgsql exception.
        /// </summary>
        /// <remarks>
        /// Conventions:
        /// 
        /// 	RAISE DEBUG 'format', expression, ...
        /// 	RAISE LOG 'format', expression, ...
        /// 	RAISE INFO 'format', expression, ...
        /// 	RAISE WARNING 'format', expression, ...
        /// 	RAISE EXCEPTION 'format', expression, ...
        /// 
        /// Cf. https://www.postgresql.org/docs/current/static/plpgsql-errors-and-messages.html
        /// 
        /// </remarks>
        public static void LogPostgresSql(this ILogger logger, PostgresException e, string prefix = null)
        {
            var level = GetLevel(e.Severity);

            string fullPrefix;
            if (prefix == "")
                fullPrefix = "";
            else if (prefix != null)
                fullPrefix = $"{prefix}";
            else if (!string.IsNullOrEmpty(e.Routine) && !e.Routine.StartsWith("p_log"))
                fullPrefix = $"[{e.Routine}, line {e.Line}] ";
            else
                fullPrefix = "";
            
            logger.Log(level, default(EventId), $"{fullPrefix}{e.Message}", null, MessageFormatter);
        }


        //------------------------------------------Helpers------------------------------------------

        private static string MessageFormatter(object state, Exception error)
        {
            return state.ToString();
        }
    }
}
