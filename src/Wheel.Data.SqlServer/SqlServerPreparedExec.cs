﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Data.Common;
using Microsoft.Data.SqlClient;
using System.Text;

namespace Wheel.Data
{
    internal sealed class SqlServerPreparedExec : IPreparedExec
    {
        public SqlConnection Connection { get; }

        public ILogger Logger { get; }

        public string LogPrefix { get; }

        public bool CatchExceptions { get; }

        public SqlServerPreparedExec(SqlConnection connection, ILogger logger, string logPrefix, bool catchExceptions)
        {
            Connection = connection;
            Logger = logger;
            LogPrefix = logPrefix;
            CatchExceptions = catchExceptions;
        }
    }
}
