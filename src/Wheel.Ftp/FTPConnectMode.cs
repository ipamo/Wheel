﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Wheel.Ftp
{

    /// <summary>
    /// Enumerates the connect modes that are possible, active and passive.
    /// </summary>
    /// <remarks>
    /// The mode describes the behaviour of the server. In active mode, the server
    /// actively connects to the client to establish a data connection. In passive mode
    /// the client connects to the server.
    /// </remarks>
    public enum FTPConnectMode
    {
        /// <summary>   
        /// Represents active - PORT - connect mode. The server connects to the client
        /// for data transfers.
        /// </summary>
        ACTIVE = 1,

        /// <summary>   
        /// Represents passive - PASV - connect mode. The client connects to the server 
        /// for data transfers.
        /// </summary>
        PASV = 2
    }
}
