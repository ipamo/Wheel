﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Text;

namespace Wheel.Data
{
    /// <summary>
    /// The result of <see cref="IDbms.PrepareExec"/>.
    /// </summary>
    public interface IPreparedExec
    {

    }
}
