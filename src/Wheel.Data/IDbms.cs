﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Common;
using System.Text;

namespace Wheel.Data
{
    /// <summary>
    /// Configuration of features related to the database management system.
    /// </summary>
    public interface IDbms
    {
        /// <summary>
        /// DbConnection type.
        /// </summary>
        Type DbConnectionType { get; }

        /// <summary>
        /// The connection string key to use when <c>cskey</c> arguments are null.
        /// </summary>
        string DefaultCskey { get; }
        
        /// <summary>
        /// Unsanitized default schema for the DBMS.
        /// <para>Can be <c>null</c> (for example: Oracle).</para>
        /// </summary>
        string DefaultSchema { get; }

        /// <summary>
        /// Default timeout in seconds for Exec methods.
        /// </summary>
        int DefaultExecTimeout { get; }

        /// <summary>
        /// Return the table name for the given entity type.
        /// </summary>
        string GetTableName(Type entityType);

        /// <summary>
        /// Return the view name for the given entity type (annotated with <see cref="ViewAttribute"/>).
        /// </summary>
        string GetViewName(Type entityType);

        /// <summary>
        /// Return the schema name for the given entity type.
        /// <para>Can be <c>null</c> (for example: Oracle).</para>
        /// </summary>
        string GetSchemaName(Type entityType);

        /// <summary>
        /// Return the column name for the given property.
        /// </summary>
        string GetColumnName(Type entityType, string propertyName);

        /// <summary>
        /// Return the column database type for the given property, or null to leave the defaults.
        /// </summary>
        string GetColumnType(Type entityType, string propertyName, Type propertyType);

        /// <summary>
        /// Name of the column used for insertion timestamps in entities with <see cref="AutoColumnsAttribute"/>.
        /// <para>
        /// Not used if null.
        /// </para>
        /// </summary>
        string InsertedAtProperty { get; }

        /// <summary>
        /// Name of the properties that will have the same behaviour as <see cref="InsertedAtProperty"/>
        /// for SaveChanges() in DbContext.
        /// <para>
        /// Properties must have the same type as <see cref="InsertedAtProperty"/>.
        /// </para>
        /// </summary>
        string[] InsertedAtAlternatives { get; }

        /// <summary>
        /// Name of the column used for insertion users in entities with <see cref="AutoColumnsAttribute"/>.
        /// <para>
        /// Not used if null.
        /// </para>
        /// </summary>
        string InsertedByIdProperty { get; }

        /// <summary>
        /// Name of the column used for insertion components in entities with <see cref="AutoColumnsAttribute"/>.
        /// <para>
        /// Not used if null.
        /// </para>
        /// </summary>
        string InsertedThroughIdProperty { get; }

        /// <summary>
        /// Name of the column used for update timestamps in entities with <see cref="AutoColumnsAttribute"/>.
        /// <para>
        /// Not used if null.
        /// </para>
        /// </summary>
        string UpdatedAtProperty { get; }

        /// <summary>
        /// Name of the properties that will have the same behaviour as <see cref="UpdatedAtProperty"/>
        /// for SaveChanges() in DbContext.
        /// <para>
        /// Properties must have the same type as <see cref="InsertedAtProperty"/>.
        /// </para>
        /// </summary>
        string[] UpdatedAtAlternatives { get; }

        /// <summary>
        /// Name of the column used for update users in entities with <see cref="AutoColumnsAttribute"/>.
        /// <para>
        /// Not used if null.
        /// </para>
        /// </summary>
        string UpdatedByIdProperty { get; }

        /// <summary>
        /// Name of the column used for update components in entities with <see cref="AutoColumnsAttribute"/>.
        /// <para>
        /// Not used if null.
        /// </para>
        /// </summary>
        string UpdatedThroughIdProperty { get; }

        /// <summary>
        /// Indicate that <see cref="UpdatedAtProperty"/>, <see cref="UpdatedByIdProperty"/> and <see cref="UpdatedThroughIdProperty"/>
        /// will also be set when the entity is first inserted.
        /// </summary>
        bool AssignUpdatedPropertiesAtInsertion { get; }

        /// <summary>
        /// Type of the user id pointed by of <see cref="InsertedByIdProperty"/> and <see cref="UpdatedByIdProperty"/>.
        /// <para>
        /// By default: int.
        /// </para>
        /// </summary>
        Type ByIdType { get; }

        /// <summary>
        /// Type of the entity pointed by <see cref="InsertedByIdProperty"/> and <see cref="UpdatedByIdProperty"/>.
        /// <para>
        /// If set, a foreign key constraint will be created.
        /// </para>
        /// </summary>
        Type ByIdRelatedEntity { get; }

        /// <summary>
        /// Type of the component id pointed by of <see cref="InsertedThroughIdProperty"/> and <see cref="UpdatedThroughIdProperty"/>.
        /// <para>
        /// By default: int.
        /// </para>
        /// </summary>
        Type ThroughIdType { get; }

        /// <summary>
        /// Name of the properties that will be mapped to <see cref="DateOnlySqlType"/>.
        /// <para>
        /// Properties must be of type "DateTime" or "DateTime?" (nullable).
        /// </para>
        /// </summary>
        string[] DateOnlyProperties { get; }

        /// <summary>
        /// Default Sql type for date-only columns.
        /// <para>
        /// If null, use default Sql type mapped to CLR type.
        /// </para>
        /// </summary>
        string DateOnlySqlType { get; }

        /// <summary>
        /// Default Sql type for date-time columns.
        /// <para>
        /// If null, use default Sql type mapped to CLR type.
        /// </para>
        /// </summary>
        string DateTimeSqlType { get; }

        /// <summary>
        /// Default value to use for insertion (or update) dates (when type is not nullable).
        /// </summary>
        string DateOnlyNowSql { get; }

        /// <summary>
        /// Default value to use for insertion (or update) datetimes (when type is not nullable).
        /// </summary>
        string DateTimeNowSql { get; }

        /// <summary>
        /// String Sql type for the given max length.
        /// <para>
        /// If null, use default Sql type mapped to CLR type.
        /// </para>
        /// </summary>
        string StringSqlType(int maxLength = -1);

        /// <summary>
        /// Decimal Sql type for the given precision and scale.
        /// <para>
        /// If null, use default Sql type mapped to CLR type.
        /// </para>
        /// </summary>
        string DecimalSqlType(int precision, int scale);

        /// <summary>
        /// Get the connection string associated to the given settings key (in the [ConnectionStrings] section).
        /// If no key is given, <see cref="DefaultCskey"/> is used.
        /// </summary>
        string GetConnectionString(string cskey = null);

        /// <summary>
        /// Instanciate a new connection.
        /// </summary>
        /// <param name="connectionString">A full connection string.</param>
        DbConnection InstanciateConnection(string connectionString);

        /// <summary>
        /// Create and open new connection.
        /// </summary>
        /// <param name="cskey">The settings key of the connection string in the [ConnectionStrings] section. If not provided, <see cref="DefaultCskey"/> is used.</param>
        DbConnection OpenConnection(string cskey = null);

        /// <summary>
        /// Retrieve (or create) a connection.
        /// </summary>
        /// <param name="cskey">The settings key of the connection string in the [ConnectionStrings] section. If not provided, <see cref="DefaultCskey"/> is used.</param>
        /// <param name="oid">An optional identifier to distinguish between several connections pointing on the same cskey. If not provided, <paramref name="cskey"/> is used.</param>
        DbConnection RetrieveConnection(string cskey = null, string oid = null);

        /// <summary>
        /// Retrieve or open a connection associated to the <see cref="DefaultCskey"/> connection string.
        /// </summary>
        DbConnection DefaultConnection { get; }

        /// <summary>
        /// Query the database name of the given connection.
        /// <para>For oracle: instance name.</para>
        /// </summary>
        string GetDatabaseName(DbConnection connection);
        
        /// <summary>
        /// Protect an identifier against Sql injections.
        /// </summary>
        string SanitizeIdentifier(string identifier);

        /// <summary>
        /// Protect an identifier against Sql injections.
        /// </summary>
        string SanitizeIdentifier(DbIdentifier identifier);

        /// <summary>
        /// Parse an identifier.
        /// </summary>
        DbIdentifier ParseIdentifier(string identifier);
                
        /// <summary>
        /// Return the <see cref="Component"/> id associated to the given string.
        /// </summary>
        short GetComponentId(string name);

        /// <summary>
        /// Return the <see cref="Component"/> id associated to the full name of the given type.
        /// </summary>
        short GetComponentId(Type type);

        /// <summary>
        /// Return the <see cref="Component"/> id associated to the full name of the type parameter.
        /// </summary>
        short GetComponentId<T>();

        /// <summary>
        /// Return the <see cref="Dict"/> value associated to the given key, or <c>null</c> if the key does not exist
        /// or if the value is empty.
        /// </summary>
        string GetDictValue(string key);

        /// <summary>
        /// Return the <see cref="Dict"/> value associated to the given key. Raise a KeyNotFoundException
        /// if the key does not exist or if the value is empty.
        /// </summary>
        TValue GetDictValue<TValue>(string key);

        /// <summary>
        /// Set a <see cref="Dict"/> value.
        /// </summary>
        void SetDictValue(string key, object value);

        /// <summary>
        /// Remove a <see cref="Dict"/> value. Do nothing if the key did not exist.
        /// </summary>
        void RemoveDictValue(string key);

        /// <summary>
        /// Add position parameters.
        /// </summary>
        DbParameterCollection AddParameters(DbCommand cmd, params object[] parameters);

        /// <summary>
        /// Add named parameters.
        /// </summary>
        DbParameterCollection AddParameters(DbCommand cmd, Dictionary<string, object> parameters);

        /// <summary>
        /// Execute a SQL statement that returns nothing, with optional positionnal parameters.
        /// <para>
        /// Note: positional parameters are marked with the <c>@1</c>, <c>@2</c>, <c>@3</c> (...) syntax (for SqlServer and PostgreSql).
        /// </para>
        /// </summary>
        void NonQuery(string sql, params object[] parameters);

        /// <summary>
        /// Execute a SQL statement that returns nothing, with named parameters.
        /// <para>
        /// Note: named parameters are marked with the <c>@paramname</c> syntax (for SqlServer and PostgreSql).
        /// </para>
        /// </summary>
        void NonQuery(string sql, Dictionary<string, object> parameters);

        /// <summary>
        /// Execute a SQL statement that returns a scalar object, with optional positionnal parameters.
        /// <para>
        /// Note: positional parameters are marked with the <c>@1</c>, <c>@2</c>, <c>@3</c> (...) syntax (for SqlServer and PostgreSql).
        /// </para>
        /// </summary>
        object QueryScalar(string sql, params object[] parameters);

        /// <summary>
        /// Execute a SQL statement that returns a scalar object, with named parameters.
        /// <para>
        /// Note: named parameters are marked with the <c>@paramname</c> syntax (for SqlServer and PostgreSql).
        /// </para>
        /// </summary>
        object QueryScalar(string sql, Dictionary<string, object> parameters);

        /// <summary>
        /// Execute a SQL statement that returns a string, with optional positionnal parameters.
        /// <para>
        /// Note: positional parameters are marked with the <c>@1</c>, <c>@2</c>, <c>@3</c> (...) syntax (for SqlServer and PostgreSql).
        /// </para>
        /// </summary>
        string QueryString(string sql, params object[] parameters);

        /// <summary>
        /// Execute a SQL statement that returns a string, with named parameters.
        /// <para>
        /// Note: named parameters are marked with the <c>@paramname</c> syntax (for SqlServer and PostgreSql).
        /// </para>
        /// </summary>
        string QueryString(string sql, Dictionary<string, object> parameters);

        /// <summary>
        /// Execute a SQL statement that returns a long integer, with optional positionnal parameters.
        /// <para>
        /// Note: positional parameters are marked with the <c>@1</c>, <c>@2</c>, <c>@3</c> (...) syntax (for SqlServer and PostgreSql).
        /// </para>
        /// </summary>
        long? QueryLong(string sql, params object[] parameters);

        /// <summary>
        /// Execute a SQL statement that returns a long integer, with named parameters.
        /// <para>
        /// Note: named parameters are marked with the <c>@paramname</c> syntax (for SqlServer and PostgreSql).
        /// </para>
        /// </summary>
        long? QueryLong(string sql, Dictionary<string, object> parameters);
        
        /// <summary>
        /// Prepare a connection for the execution of stored procedures:
        /// open the connection (if not already done), associate 
        /// </summary>
        /// <param name="connection">A connection (will be opened if it is not already).</param>
        /// <param name="logger">The logger to use for the execution.</param>
        /// <param name="logPrefix">An optional prefix to use for messages emitted from the stored procedure execution.</param>
        /// <param name="catchExceptions">Indicate whether exceptions will be caught.</param>
        IPreparedExec PrepareExec(DbConnection connection, ILogger logger, string logPrefix = null, bool catchExceptions = false);

        /// <summary>
        /// Execute a stored procedure with arguments given in the order of their definition in the procedure.
        /// A connection is created and used only for this execution.
        /// </summary>
        /// <param name="procedure">Name of the procedure (with optional schema and database names).</param>
        /// <param name="args">Arguments given in the order of their definition in the procedure.</param>
        /// <param name="timeout">Timeout in seconds.</param>
        /// <param name="ifNoReturn">Value to use as the method return if the procedure did not return any value. Null to raise an exception.</param>
        /// <param name="logger">An optional logger to use for the execution. By default, this is the logger of the Db class.</param>
        /// <param name="logPrefix">An optional prefix to use for messages emitted from the stored procedure execution.</param>
        /// <param name="catchExceptions">Indicate whether exceptions will be caught.</param>
        /// <returns>Code returned by the procedure (-1 in case of caught exception).</returns>
        int Exec(string procedure, object[] args, int timeout = -1, int? ifNoReturn = null, ILogger logger = null, string logPrefix = null, bool catchExceptions = false);

        /// <summary>
        /// Execute a stored procedure with arguments given in the order of their definition in the procedure.
        /// The connection must have been previously prepared with <see cref="PrepareExec"/>.
        /// </summary>
        /// <param name="prepared">Previously prepared with <see cref="PrepareExec"/>.</param>
        /// <param name="procedure">Name of the procedure (with optional schema and database names).</param>
        /// <param name="args">Arguments given in the order of their definition in the procedure.</param>
        /// <param name="timeout">Timeout in seconds.</param>
        /// <param name="ifNoReturn">Value to use as the method return if the procedure did not return any value. Null to raise an exception.</param>
        /// <returns>Code returned by the procedure (-1 in case of caught exception).</returns>
        int Exec(IPreparedExec prepared, string procedure, object[] args, int timeout = -1, int? ifNoReturn = null);

        /// <summary>
        /// Execute a stored procedure with named arguments.
        /// A connection is created and used only for this execution.
        /// </summary>
        /// <param name="procedure">Name of the procedure (with optional schema and database names).</param>
        /// <param name="args">Named arguments.</param>
        /// <param name="timeout">Timeout in seconds.</param>
        /// <param name="ifNoReturn">Value to use as the method return if the procedure did not return any value. Null to raise an exception.</param>
        /// <param name="logger">An optional logger to use for the execution. By default, this is the logger of the Db class.</param>
        /// <param name="logPrefix">An optional prefix to use for messages emitted from the stored procedure execution.</param>
        /// <param name="catchExceptions">Indicate whether exceptions will be caught.</param>
        /// <returns>Code returned by the procedure (-1 in case of caught exception).</returns>
        int Exec(string procedure, Dictionary<string, object> args = null, int timeout = -1, int? ifNoReturn = null, ILogger logger = null, string logPrefix = null, bool catchExceptions = false);

        /// <summary>
        /// Execute a stored procedure with named arguments.
        /// The connection must have been previously prepared with <see cref="PrepareExec"/>.
        /// </summary>
        /// <param name="prepared">Previously prepared with <see cref="PrepareExec"/>.</param>
        /// <param name="procedure">Name of the procedure (with optional schema and database names).</param>
        /// <param name="args">Named arguments.</param>
        /// <param name="timeout">Timeout in seconds.</param>
        /// <param name="ifNoReturn">Value to use as the method return if the procedure did not return any value. Null to raise an exception.</param>
        /// <returns>Code returned by the procedure (-1 in case of caught exception).</returns>
        int Exec(IPreparedExec prepared, string procedure, Dictionary<string, object> args = null, int timeout = -1, int? ifNoReturn = null);

        /// <summary>
        /// Execute a stored procedure with arguments given in the order of their definition in the procedure.
        /// The procedure must contain a varchar output argument named @StatusMessage (which will be returned by this method).
        /// A connection is created and used only for this execution.
        /// </summary>
        /// <param name="procedure">Name of the procedure (with optional schema and database names).</param>
        /// <param name="args">Arguments given in the order of their definition in the procedure.</param>
        /// <param name="timeout">Timeout in seconds.</param>
        /// <param name="ifNoReturn">Value to use as the method return if the procedure did not return any value. Null to raise an exception.</param>
        /// <param name="logger">An optional logger to use for the execution. By default, this is the logger of the Db class.</param>
        /// <param name="logPrefix">An optional prefix to use for messages emitted from the stored procedure execution.</param>
        /// <param name="catchExceptions">Indicate whether exceptions will be caught.</param>
        /// <returns>Code returned by the procedure (-1 in case of caught exception).</returns>
        (int returnCode, string statusMessage) ExecStatusMessage(string procedure, object[] args, int timeout = -1, int? ifNoReturn = null, ILogger logger = null, string logPrefix = null, bool catchExceptions = false);

        /// <summary>
        /// Execute a stored procedure with arguments given in the order of their definition in the procedure.
        /// The procedure must contain a varchar output argument named @StatusMessage (which will be returned by this method).
        /// The connection must have been previously prepared with <see cref="PrepareExec"/>.
        /// </summary>
        /// <param name="prepared">Previously prepared with <see cref="PrepareExec"/>.</param>
        /// <param name="procedure">Name of the procedure (with optional schema and database names).</param>
        /// <param name="args">Arguments given in the order of their definition in the procedure.</param>
        /// <param name="timeout">Timeout in seconds.</param>
        /// <param name="ifNoReturn">Value to use as the method return if the procedure did not return any value. Null to raise an exception.</param>
        /// <returns>Code returned by the procedure (-1 in case of caught exception).</returns>
        (int returnCode, string statusMessage) ExecStatusMessage(IPreparedExec prepared, string procedure, object[] args, int timeout = -1, int? ifNoReturn = null);

        /// <summary>
        /// Execute a stored procedure with named arguments.
        /// The procedure must contain a varchar output argument named @StatusMessage (which will be returned by this method).
        /// A connection is created and used only for this execution.
        /// </summary>
        /// <param name="procedure">Name of the procedure (with optional schema and database names).</param>
        /// <param name="args">Named arguments.</param>
        /// <param name="timeout">Timeout in seconds.</param>
        /// <param name="ifNoReturn">Value to use as the method return if the procedure did not return any value. Null to raise an exception.</param>
        /// <param name="logger">An optional logger to use for the execution. By default, this is the logger of the Db class.</param>
        /// <param name="logPrefix">An optional prefix to use for messages emitted from the stored procedure execution.</param>
        /// <param name="catchExceptions">Indicate whether exceptions will be caught.</param>
        /// <returns>Code returned by the procedure (-1 in case of caught exception).</returns>
        (int returnCode, string statusMessage) ExecStatusMessage(string procedure, Dictionary<string, object> args = null, int timeout = -1, int? ifNoReturn = null, ILogger logger = null, string logPrefix = null, bool catchExceptions = false);

        /// <summary>
        /// Execute a stored procedure with named arguments.
        /// The procedure must contain a varchar output argument named @StatusMessage (which will be returned by this method).
        /// The connection must have been previously prepared with <see cref="PrepareExec"/>.
        /// </summary>
        /// <param name="prepared">Previously prepared with <see cref="PrepareExec"/>.</param>
        /// <param name="procedure">Name of the procedure (with optional schema and database names).</param>
        /// <param name="args">Named arguments.</param>
        /// <param name="timeout">Timeout in seconds.</param>
        /// <param name="ifNoReturn">Value to use as the method return if the procedure did not return any value. Null to raise an exception.</param>
        /// <returns>Code returned by the procedure (-1 in case of caught exception).</returns>
        (int returnCode, string statusMessage) ExecStatusMessage(IPreparedExec prepared, string procedure, Dictionary<string, object> args = null, int timeout = -1, int? ifNoReturn = null);
    }
}
