﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Reflection;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Wheel.Data
{
    /// <summary>
    /// Helper for <see cref="DbContext.OnModelCreating(ModelBuilder)"/> method.
    /// </summary>
    public class ModelConfigurator
    {
        protected readonly IDbms _dbms;

        // may be overriden in inherited class from ModelConfigurator
        protected bool _removeCascadeDelete = true;
        protected bool _autoColumnsForAllEntities = false;

        public ModelConfigurator(IDbms dbms)
        {
            _dbms = dbms;
        }

        public virtual void OnModelCreating(ModelBuilder builder)
        {
            foreach (var entity in builder.Model.GetEntityTypes())
            {
                ConfigureEntity(builder, entity);

                foreach (var property in entity.GetProperties())
                {
                    ConfigureProperty(builder, entity, property);

                    foreach (var foreignKey in property.GetContainingForeignKeys())
                    {
                        ConfigureForeignKey(builder, entity, property, foreignKey);
                    }
                }

                foreach (var key in entity.GetKeys())
                {
                    ConfigureKey(builder, entity, key);
                }

                foreach (var index in entity.GetIndexes())
                {
                    ConfigureIndex(builder, entity, index);
                }
            }
        }

        protected virtual void ConfigureEntity(ModelBuilder builder, IMutableEntityType entity)
        {
            SetTableOrViewNameAndSchema(builder, entity);

            ApplyAutoColumnAttribute(builder, entity);

            CallEntityOnModelCreatingMethod(builder, entity);
        }

        protected virtual void ConfigureProperty(ModelBuilder builder, IMutableEntityType entity, IMutableProperty property)
        {
            SetColumnName(builder, entity, property);

            SetColumnType(builder, entity, property);

            SetColumnDefaultValue(builder, entity, property);
        }

        protected virtual void ConfigureForeignKey(ModelBuilder builder, IMutableEntityType entity, IMutableProperty property, IMutableForeignKey foreignKey)
        {
            if (_removeCascadeDelete)
                RemoveCascadeDelete(builder, foreignKey);
        }

        protected virtual void ConfigureKey(ModelBuilder builder, IMutableEntityType entity, IMutableKey key)
        {

        }

        protected virtual void ConfigureIndex(ModelBuilder builder, IMutableEntityType entity, IMutableIndex index)
        {

        }

        /// <summary>
        /// Set table and schema names for the entity.
        /// </summary>
        protected virtual void SetTableOrViewNameAndSchema(ModelBuilder builder, IMutableEntityType entity)
        {
            // Get name/schema from attributes
            string name = null;
            string schema = null;
            bool view = false;

            var viewAttr = entity.ClrType.GetCustomAttribute<ViewAttribute>(inherit: false);
            if (viewAttr != null)
            {
                name = viewAttr.Name?.TrimOrNull();
                schema = viewAttr.Schema?.TrimOrNull();
                view = true;
            }
            else
            {
                var tableAttr = entity.ClrType.GetCustomAttribute<TableAttribute>(inherit: false);
                if (tableAttr != null)
                {
                    name = tableAttr.Name?.TrimOrNull();
                    schema = tableAttr.Schema?.TrimOrNull();
                }
            }

            if (schema == null)
            {
                var schemaAttr = entity.ClrType.GetCustomAttribute<SchemaAttribute>(inherit: false);
                if (schemaAttr != null)
                {
                    schema = schemaAttr.Name?.TrimOrNull();
                }
            }

            // Use default names if necessary
            if (string.IsNullOrWhiteSpace(name))
                name = view ? _dbms.GetViewName(entity.ClrType) : _dbms.GetTableName(entity.ClrType);
            if (string.IsNullOrWhiteSpace(schema))
                schema = _dbms.GetSchemaName(entity.ClrType) ?? _dbms.DefaultSchema;

            // Apply table name and schema
            if (view)
            {
                // Associate entity to view
                builder.Entity(entity.Name)
                    .HasNoKey()
                    .ToView(name, schema);
            }
            else
            {
                // Configure table name and schema
                if (name != null)
                    entity.SetTableName(name);
                if (schema != null)
                    entity.SetSchema(schema);
            }
        }

        protected virtual void ApplyAutoColumnAttribute(ModelBuilder builder, IMutableEntityType entity)
        {
            if (!_autoColumnsForAllEntities)
            {
                var a = entity.ClrType.GetTypeInfo().GetCustomAttribute<AutoColumnsAttribute>();
                if (a == null)
                    return;
            }

            var e = builder.Entity(entity.ClrType);

            // Inserted

            if (_dbms.InsertedAtProperty != null)
                e.Property<DateTime>(_dbms.InsertedAtProperty);

            if (_dbms.InsertedByIdProperty != null)
            {
                e.Property(_dbms.ByIdType ?? typeof(long), _dbms.InsertedByIdProperty);

                if (_dbms.ByIdRelatedEntity != null)
                {
                    builder.Entity(entity.ClrType)
                        .HasOne(_dbms.ByIdRelatedEntity, null)
                        .WithMany()
                        .HasForeignKey(_dbms.InsertedByIdProperty);
                }
            }

            if (_dbms.InsertedThroughIdProperty != null)
                e.Property(_dbms.ThroughIdType ?? typeof(long), _dbms.InsertedThroughIdProperty);

            // Updated

            if (_dbms.UpdatedAtProperty != null)
            {
                e.Property(_dbms.AssignUpdatedPropertiesAtInsertion ? typeof(DateTime) : typeof(DateTime?), _dbms.UpdatedAtProperty);
            }

            if (_dbms.UpdatedByIdProperty != null)
            {
                var type = _dbms.ByIdType ?? typeof(long);
                if (!_dbms.AssignUpdatedPropertiesAtInsertion)
                    type = type.GetNullableType();
                e.Property(type, _dbms.UpdatedByIdProperty);

                if (_dbms.ByIdRelatedEntity != null)
                {
                    builder.Entity(entity.ClrType)
                        .HasOne(_dbms.ByIdRelatedEntity, null)
                        .WithMany()
                        .HasForeignKey(_dbms.UpdatedByIdProperty);
                }
            }

            if (_dbms.UpdatedThroughIdProperty != null)
            {
                var type = _dbms.ThroughIdType ?? typeof(long);
                if (!_dbms.AssignUpdatedPropertiesAtInsertion)
                    type = type.GetNullableType();
                e.Property(type, _dbms.UpdatedThroughIdProperty);
            }
        }

        /// <summary>
        /// Call the entity's OnModelCreating(ModelBuilder) static method if it exists.
        /// </summary>
        /// <return>true if the OnModelCreating(ModelBuilder) method existed.</return>
        protected virtual bool CallEntityOnModelCreatingMethod(ModelBuilder builder, IMutableEntityType entity)
        {
            var methods = entity.ClrType.GetTypeInfo()
                .DeclaredMethods
                .Where(f => f.Name == "OnModelCreating");

            if (!methods.Any())
                return false;

            if (methods.Count() > 1)
                throw new InvalidOperationException(string.Format("Entity type {0} has several OnModelCreating methods", entity.ClrType));
            var method = methods.First();

            if (!method.IsStatic)
                throw new InvalidOperationException(string.Format("OnModelCreating method for entity type {0} is not static", entity.ClrType));

            var parameters = method.GetParameters();
            if (parameters.Length != 1 || parameters[0].ParameterType != builder.GetType())
                throw new InvalidOperationException(string.Format("OnModelCreating method for entity type {0} does not accept a single parameter of type {1}", entity.ClrType, builder.GetType()));

            // Call the method
            try
            {
                method.Invoke(null, new object[] { builder });
            }
            catch (TargetInvocationException ex) when (ex.InnerException != null)
            {
                var e = ex.InnerException;
                var message = string.Format("{0} raised in method {1} of {2}: {3}", e.GetType().Name, method.Name, entity.ClrType, e.Message);
                throw new MessageException(message, e);
            }

            return true;
        }

        protected virtual void SetColumnName(ModelBuilder builder, IMutableEntityType entity, IMutableProperty property)
        {
            property.SetColumnName(_dbms.GetColumnName(entity.ClrType, property.Name));
        }

        protected virtual void SetColumnType(ModelBuilder builder, IMutableEntityType entity, IMutableProperty property)
        {
            var type = _dbms.GetColumnType(entity.ClrType, property.Name, property.ClrType);
            if (type == null)
            {
                if (property.ClrType == typeof(string))
                {
                    var attr = property.PropertyInfo?.GetCustomAttribute<MaxLengthAttribute>();
                    var maxLength = -1;
                    if (attr != null)
                    {
                        maxLength = attr.Length;
                    }
                    else
                    {
                        var attr2 = property.PropertyInfo?.GetCustomAttribute<StringLengthAttribute>();
                        if (attr2 != null)
                        {
                            maxLength = attr2.MaximumLength;
                        }
                    }
                    type = _dbms.StringSqlType(maxLength);
                }
                else if (property.ClrType == typeof(DateTime) || property.ClrType == typeof(DateTime?))
                {
                    var attr = property.PropertyInfo?.GetCustomAttribute<DataTypeAttribute>();
                    if (attr != null && attr.DataType == DataType.Date)
                        type = _dbms.DateOnlySqlType;
                    else if (_dbms.IsDateOnlyProperty(property.Name))
                        type = _dbms.DateOnlySqlType;
                    else
                        type = _dbms.DateTimeSqlType;
                }
                else
                {
                    // DecimalAttribute (and derived: AmountAttribute, CoordinateAttribute)
                    var customAttributes = property.PropertyInfo?.GetCustomAttributes();
                    if (customAttributes != null)
                    {
                        foreach (var attr in customAttributes)
                        {
                            if (attr is DecimalAttribute a)
                            {
                                type = _dbms.DecimalSqlType(a.Precision, a.Scale);
                                break;
                            }
                        }
                    }
                }
            }

            // Apply the type
            if (type != null)
                property.SetColumnType(type);
        }

        protected virtual void SetColumnDefaultValue(ModelBuilder builder, IMutableEntityType entity, IMutableProperty property)
        {
            string sql = null;

            if (_dbms.IsDefaultNowProperty(property.Name))
            {
                if (_dbms.IsDateOnlyProperty(property.Name))
                    property.SetDefaultValueSql(_dbms.DateOnlyNowSql);
                else
                    property.SetDefaultValueSql(_dbms.DateTimeNowSql);
            }

            // Apply the default value
            if (sql != null)
                property.SetDefaultValueSql(sql);
        }

        /// <summary>
        /// Disable cascade delete.
        /// </summary>
        protected virtual void RemoveCascadeDelete(ModelBuilder builder, IMutableForeignKey foreignKey)
        {
            if (foreignKey.DeleteBehavior == DeleteBehavior.Cascade && !foreignKey.IsOwnership)
            {
                foreignKey.DeleteBehavior = DeleteBehavior.Restrict;
            }
        }
    }
}
