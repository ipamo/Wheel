﻿using Wheel.Data.Tests.DbContextIntegration.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Wheel.Data;

namespace Wheel.Data.Tests.DbContextIntegration.Data
{
    public class ApplicationDbContext : DbContext
    {
        private readonly DbContextHelper _helper;

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options,
            DbContextHelper helper)
            : base(options)
        {
            _helper = helper;
        }

        public DbSet<User> Users { get; set; }
        public DbSet<Post> Posts { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            _helper.OnModelCreating(builder);
        }

        public override int SaveChanges(bool acceptAllChangesOnSuccess)
        {
            _helper.SaveChanges_AssignEdition(ChangeTracker);
            return base.SaveChanges(acceptAllChangesOnSuccess);
        }

        public override async Task<int> SaveChangesAsync(bool acceptAllChangesOnSuccess, CancellationToken cancellationToken = default(CancellationToken))
        {
            _helper.SaveChanges_AssignEdition(ChangeTracker);
            return await base.SaveChangesAsync(acceptAllChangesOnSuccess, cancellationToken);
        }
    }
}
