﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit.Abstractions;

namespace Wheel.Testing
{
    /// <summary>
    /// A logger writing messages in xunit's ITestOutputHelper, and that keeps the number of messages and the content of the messages in memory (allows usage of xunit's Assert.Equal or Assert.Contains on log messages).
    /// </summary>
    public class TestLogger : LogCounter
    {
        private readonly ITestOutputHelper _output;
        private readonly LogLevel _messageLevel;

        public TestLogger(ITestOutputHelper output,
            LogLevel messageLevel = LogLevel.Trace,
            LogLevel countLevel = LogLevel.Trace)
            : base(messageLevel < countLevel ? messageLevel : countLevel)
        {
            _output = output;
            _messageLevel = messageLevel;
        }

        public override void Clear()
        {
            base.Clear();
            Messages = null;
        }

        public string Messages { get; private set; }
        
        public override void Log<TState>(LogLevel logLevel, EventId eventId, TState state, Exception exception, Func<TState, Exception, string> formatter)
        {
            base.Log(logLevel, eventId, state, exception, formatter);

            if (logLevel < _messageLevel)
                return;

            var message = formatter(state, exception);
            var line = string.Format("[{0}] {1}", logLevel.ToString().ToUpperInvariant(), message);
            _output.WriteLine(line);
            Messages = (Messages == null ? "" : Messages + Environment.NewLine) + line;
        }

        public void WriteTest()
        {
            LogLevel minLevel = LogLevel.None;
            foreach (LogLevel level in Enum.GetValues(typeof(LogLevel)))
            {
                if (IsEnabled(minLevel) && level < minLevel)
                    minLevel = level;
            }

            _output.WriteLine(@"{0} #{1}: level {2}", GetType().Name, GetHashCode(), minLevel);
            foreach (LogLevel level in Enum.GetValues(typeof(LogLevel)))
            {
                if (level == LogLevel.None)
                    continue;
                LoggingUtils.Log(this, level, "Test {0}", level);
            }
        }
    }
}
