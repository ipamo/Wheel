﻿using Xunit;

// Turn off parallelism inside the assembly
// Usefull for tests based on Wheel.Testing.ConsoleCapture.
[assembly: CollectionBehavior(DisableTestParallelization = true)]
