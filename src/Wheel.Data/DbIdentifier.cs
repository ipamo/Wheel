﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Wheel.Data
{
    /// <summary>
    /// Parse and sanitize database identifiers (tables, procedures, columns, etc).
    /// </summary>
    public struct DbIdentifier
    {
        private readonly char _separator;
        private readonly char _protectBegin;
        private readonly char _protectEnd;
        private readonly string _defaultSchema; // Can be null
        
        /// <summary>
        /// Build a new DbIdentifier object. Should not be used directly. Instead, use <see cref="IDbms.ParseIdentifier(string)"/>
        /// </summary>
        internal DbIdentifier(char protectBegin, char protectEnd, string defaultSchema, char separator = '.', string name = null, string schema = null, string catalog = null)
        {
            _separator = separator;
            _protectBegin = protectBegin;
            _protectEnd = protectEnd;
            _defaultSchema = defaultSchema;
            Name = name;
            Schema = schema;
            Catalog = catalog;
        }
        
        /// <summary>
        /// Name of the database object (table, procedure, etc).
        /// </summary>
        public string Name { get; set; }
        
        /// <summary>
        /// Schema of the database object.
        /// </summary>
        public string Schema { get; set; }

        /// <summary>
        /// Catalog of the database object.
        /// </summary>
        public string Catalog { get; set; }

        public string SanitizedName
            => Sanitize(Name);

        public string SanitizedSchema
            => Sanitize(Schema);

        public string SanitizedCatalog
            => Sanitize(Catalog);

        /// <summary>
        /// Sanitize a part of the identifier.
        /// </summary>
        public string Sanitize(string part)
        {
            if (string.IsNullOrEmpty(part))
                return null;

            if (part.Contains(" ") || part.Contains(_separator))
                return _protectBegin + part + _protectEnd;
            else
                return part;
        }


        /// <summary>
        /// Sanitize the identifier.
        /// </summary>
        public override string ToString()
            => Sanitize();
        
        /// <summary>
        /// Sanitize the identifier.
        /// </summary>
        public string Sanitize()
        {
            if (string.IsNullOrEmpty(Name))
                return null;
            
            string full;

            // Name
            full = SanitizedName;

            // Schema
            if (!string.IsNullOrEmpty(Schema) || !string.IsNullOrEmpty(Catalog))
            {
                var sanitizedSchema = string.IsNullOrEmpty(Schema) ? Sanitize(_defaultSchema) : SanitizedSchema;
                // sanitizedSchema can be null (if _defaultSchema is null, for example: Oracle)
                // in this case we would have CATALOG..NAME (but normally that should not happen with Oracle)
                full = sanitizedSchema + _separator + full;
            }

            // Catalog
            if (! string.IsNullOrEmpty(Catalog))
                full = SanitizedCatalog + _separator + full;

            return full;
        }

        /// <summary>
        /// Clear the identifier (assign null to Name, Schema and Catalog).
        /// </summary>
        public void Clear()
        {
            Name = null;
            Schema = null;
            Catalog = null;
        }

        /// <summary>
        /// Parse an identifier.
        /// </summary>
        public DbIdentifier Parse(string rawIdentifier)
        {
            if (rawIdentifier == null)
                throw new ArgumentNullException(nameof(rawIdentifier));
            
            rawIdentifier = rawIdentifier.Trim();
            if (rawIdentifier == "")
                throw new ArgumentException($"Argument '{nameof(rawIdentifier)}' is blank", nameof(rawIdentifier));

            Clear();

            // --------------------------------------------------------------------
            // Determine parts
            var parts = new List<string>();
            var part = "";

            int i = 0;
            int iProtectedStart = -1;
            var c = rawIdentifier[i];

            // Console.WriteLine("i=" + i + ", c=" + c);

            // Start of a new part
            ParseStatus status;
            if (c == _protectBegin)
            {
                iProtectedStart = i;
                status = ParseStatus.InProtectedPart;
                // Console.WriteLine("NewStatus: InProtectedPart");
            }
            else
            {
                status = ParseStatus.InUnprotectedPart;
                //Console.WriteLine("NewStatus: InUnprotectedPart");
                part += c;
                //Console.WriteLine("AddToPart: " + c);
            }

            i++;
            while (i < rawIdentifier.Length)
            {
                c = rawIdentifier[i];
                //Console.WriteLine("i=" + i + ", c=" + c);

                if (c == ' ')
                {
                    if (status == ParseStatus.InProtectedPart)
                    {
                        part += c;
                        //Console.WriteLine("AddToPart: " + c);
                    }
                    else if (status == ParseStatus.InUnprotectedPart)
                        throw new InvalidOperationException($"DbIdentifier cannot have space in unprotected part [at index {i}]");
                    else
                    {
                        // ignored spaces
                    }
                }
                else if (c == _separator)
                {
                    if (status == ParseStatus.InProtectedPart)
                    {
                        part += c;
                        //Console.WriteLine("AddToPart: " + c);
                    }
                    else
                    {
                        if (status == ParseStatus.InUnprotectedPart)
                        {
                            //Console.WriteLine("EndOfPart");
                            // End of a part
                            parts.Add(part);
                            part = "";
                        }

                        // Start of new part
                        var orig = i;
                        i ++;
                        c = GetFirstNotBlank(rawIdentifier, ref i);
                        //Console.WriteLine("GetFirstNotBlank i="+ i + ", c=" + c);
                        if (c == '\0')
                            throw new InvalidOperationException($"DbIdentifier separator is not followed by any non blank character [at index {i}]");
                        if (c == _separator)
                            throw new InvalidOperationException($"DbIdentifier separator cannot be followed by another separator [at index {i}]");
                        if (_protectEnd != _protectBegin && c == _protectEnd)
                            throw new InvalidOperationException($"DbIdentifier separator cannot be followed by {_protectEnd} [at index {i}]");
                        if (c == _protectBegin)
                        {
                            iProtectedStart = i;
                            status = ParseStatus.InProtectedPart;
                            //Console.WriteLine("NewStatus: InProtectedPart");
                        }
                        else
                        {
                            status = ParseStatus.InUnprotectedPart;
                            //Console.WriteLine("NewStatus: InUnprotectedPart");
                            part += c;
                            //Console.WriteLine("AddToPart: " + c);
                        }
                    }
                }
                else if (c == _protectEnd)
                {
                    if (status == ParseStatus.InProtectedPart) // End of part
                    {
                        parts.Add(part);
                        part = "";
                        //Console.WriteLine("EndOfPart");
                        status = ParseStatus.BetweenParts;
                        //Console.WriteLine("NewStatus: BetweenParts");
                    }
                    else if (_protectEnd != _protectBegin)
                    {
                        throw new InvalidOperationException($"DbIdentifier char '{_protectEnd}' has no matching '{_protectBegin}' [at index {i}]");
                    }
                }
                else if (c == _protectBegin) // and protectBegin != protectEnd (otherwise would match the previous condition)
                {
                    // can be only at the beginning of the string, or just after a separator
                    throw new InvalidOperationException($"DbIdentifier char '{_protectBegin}' unexpected [at index {i}]");
                }
                else
                {
                    if (status == ParseStatus.BetweenParts)
                    {
                        throw new InvalidOperationException($"DbIdentifier char '{c}' unexpected (between two parts) [at index {i}]");
                    }
                    else
                    {
                        part += c;
                        //Console.WriteLine("AddToPart: " + c);
                    }
                }
                
                i++;
            }

            if (status == ParseStatus.InProtectedPart)
                throw new InvalidOperationException($"DbIdentifier protected part not terminated by char '{_protectEnd}' [at index {iProtectedStart}]'");
            else if (status == ParseStatus.InUnprotectedPart)
                parts.Add(part);

            // --------------------------------------------------------------------
            // Analyze parts

            if (parts.Count >= 1)
                Name = parts[parts.Count - 1];
            if (parts.Count >= 2)
                Schema = parts[parts.Count - 2];
            if (parts.Count >= 3)
                Catalog = parts[parts.Count - 3];
            if (parts.Count > 3)
                throw new InvalidOperationException($"DbIdentifier has {parts} parts (max is 3).");

            return this;
        }

        private enum ParseStatus
        {
            BetweenParts,
            InUnprotectedPart,
            InProtectedPart
        }

        /// <summary>
        /// Get the first not blank character, or \0 if none were found.
        /// </summary>
        private static char GetFirstNotBlank(string str, ref int i)
        {
            while (i < str.Length)
            {
                var c = str[i];
                if (c != ' ')
                    return c;
            }

            return '\0';
        }
    }
}

