﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Npgsql;
using NpgsqlTypes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Text;

namespace Wheel.Data
{
    /// <summary>
    /// Default DBMS features for PostgreSql.
    /// </summary>
    public class PostgreSqlDbms : BaseDbms
    {
        public PostgreSqlDbms(IConfiguration config,
                 ILoggerFactory loggerFactory,
                string defaultCskey = null)
            : base(config,
                  loggerFactory,
                  defaultCskey)
        {

        }
        
        public override Type DbConnectionType => typeof(NpgsqlConnection);

        public override string DefaultSchema => "public";
        public override string DateOnlySqlType => "date";
        public override string DateOnlyNowSql => "CURRENT_DATE";
        public override string DateTimeNowSql => "now()";

        protected override char IdentifierProtectBegin => '\"';
        protected override char IdentifierProtectEnd => '\"';

        public override string GetTableName(Type entityType) => GetBaseTableName(entityType).ToLowerSnake();
        
        public override string GetColumnName(Type entityType, string propertyName) => propertyName.ToLowerSnake();

        public override DbConnection InstanciateConnection(string connectionString) => new NpgsqlConnection(connectionString);

        public override string GetDatabaseName(DbConnection connection)
        {
            using (var cmd = new NpgsqlCommand("SELECT current_database();", (NpgsqlConnection)connection))
            {
                return (string)cmd.ExecuteScalar();
            }
        }


        #region Component
        
        private string _componentGetSql;

        private void PrepareComponentSql()
        {
            if (_componentGetSql == null)
            {
                var table = GetTableName(typeof(Wheel.Data.Component));
                var idColumn = GetColumnName(typeof(Wheel.Data.Component), nameof(Wheel.Data.Component.Id));
                var nameColumn = GetColumnName(typeof(Wheel.Data.Component), nameof(Wheel.Data.Component.Name));

                _componentGetSql = $@"WITH new_row AS (
INSERT INTO {table} ({nameColumn})
SELECT @name
WHERE NOT EXISTS (SELECT 1 FROM {table} WHERE {nameColumn} = @name)
RETURNING {idColumn}
)
SELECT {idColumn} FROM new_row
UNION
SELECT {idColumn} FROM {table} WHERE {nameColumn} = @name;";
            }
        }

        /// <summary>
        /// Renvoit l'identifiant du composant (table TT_Composant) portant le libellé donné.
        /// Le composant est créé s'il n'existe pas déjà.
        /// </summary>
        public override short GetComponentId(string name)
        {
            PrepareComponentSql();

            using (var cmd = new NpgsqlCommand(_componentGetSql, (NpgsqlConnection)DefaultConnection))
            {
                cmd.Parameters.AddWithValue("@name", name);
                return Convert.ToInt16(cmd.ExecuteScalar());
            }
        }

        #endregion


        #region Dict
        
        private string _dictSetSql;
        private string _dictGetSql;
        private string _dictRemoveSql;

        private void PrepareDictSql()
        {
            if (_dictSetSql == null)
            {
                var componentId = GetComponentId<PostgreSqlDbms>();
                var table = GetTableName(typeof(Wheel.Data.Dict));
                var idColumn = GetColumnName(typeof(Wheel.Data.Dict), nameof(Wheel.Data.Dict.Id));
                var valueColumn = GetColumnName(typeof(Wheel.Data.Dict), nameof(Wheel.Data.Dict.Value));
                var insertedByComponentIdColumn = GetColumnName(typeof(Wheel.Data.Dict), InsertedThroughIdProperty);
                var updatedByComponentIdColumn = GetColumnName(typeof(Wheel.Data.Dict), UpdatedThroughIdProperty);
                var updatedAtColumn = GetColumnName(typeof(Wheel.Data.Dict), UpdatedAtProperty);

                _dictSetSql = $@"INSERT INTO {table} ({idColumn}, {valueColumn}, {insertedByComponentIdColumn})
VALUES (@id, @value, {componentId})
ON CONFLICT ({idColumn}) DO UPDATE 
SET {valueColumn} = excluded.value, {updatedByComponentIdColumn} = {componentId}, {updatedAtColumn} = {DateTimeNowSql}";

                _dictGetSql = $@"SELECT {valueColumn} FROM {table} WHERE {idColumn} = @id";

                _dictRemoveSql = $@"DELETE FROM {table} WHERE {idColumn} = @id";
            }
        }
        
        protected override void SetDictValue(string key, string value)
        {
            PrepareDictSql();

            using (var cmd = new NpgsqlCommand(_dictSetSql, (NpgsqlConnection)DefaultConnection))
            {
                cmd.Parameters.AddWithValue("@id", key);
                cmd.Parameters.AddWithValue("@value", value);
                cmd.ExecuteNonQuery();
            }
        }

        public override string GetDictValue(string key)
        {
            PrepareDictSql();

            using (var cmd = new NpgsqlCommand(_dictGetSql, (NpgsqlConnection)DefaultConnection))
            {
                cmd.Parameters.AddWithValue("@id", key);
                var r = cmd.ExecuteScalar();
                if (r == null || r == DBNull.Value || (string)r == "")
                    return null;
                else
                    return (string)r;
            }
        }

        public override void RemoveDictValue(string key)
        {
            PrepareDictSql();

            using (var cmd = new NpgsqlCommand(_dictRemoveSql, (NpgsqlConnection)DefaultConnection))
            {
                cmd.Parameters.AddWithValue("@id", key);
                cmd.ExecuteNonQuery();
            }
        }

        #endregion


        #region Stored procedures

        protected override Dictionary<string, object> GetNamedArgs(IPreparedExec prepared, DbIdentifier procedure, object[] args)
        {
            if (prepared == null)
                throw new ArgumentNullException(nameof(prepared));
            if (!(prepared is PostgreSqlPreparedExec))
                throw new ArgumentException("Argument 'prepared' must be of type PostgreSqlPreparedExec", nameof(prepared));
            var preparedCast = (PostgreSqlPreparedExec)prepared;
            
            Dictionary<string, object> argDictionary = null;
            if (args != null && args.Length >= 1)
            {
                var databaseSqlPart = procedure.Catalog != null ? procedure.SanitizedCatalog + "." : "";

                var sql = $@"SELECT proargnames
FROM {databaseSqlPart}pg_catalog.pg_proc p
INNER JOIN {databaseSqlPart}pg_catalog.pg_namespace n ON (p.pronamespace = n.oid)
WHERE proname = @procedure AND nspname = @schema AND pronargs >= @nbargs";

                // Détermine le nom des paramètres
                string[] argNames;
                using (var cmd = new NpgsqlCommand(sql, preparedCast.Connection))
                {
                    cmd.Parameters.AddWithValue("@procedure", procedure.Name);
                    cmd.Parameters.AddWithValue("@schema", procedure.Schema ?? DefaultSchema);
                    cmd.Parameters.AddWithValue("@nbargs", args.Length);

                    using (var r = cmd.ExecuteReader())
                    {
                        if (! r.Read())
                            throw new InvalidOperationException($"No stored procedure '{procedure.Name}' found with at least {args.Length} arguments.");
                        
                        argNames = (string[]) r.GetValue(0);

                        if (r.Read())
                            throw new InvalidOperationException($"Several stored procedure '{procedure.Name}' found with at least {args.Length} arguments.");
                    }
                }
                
                argDictionary = new Dictionary<string, object>();
                int i = 0;
                while (i < argNames.Length && i < args.Length)
                {
                    argDictionary.Add(argNames[i], args[i]);
                    i++;
                }
                
                if (args.Length > argNames.Length)
                {
                    var s1 = args.Length - argNames.Length > 1 ? "s" : "";
                    _logger.LogWarning($"{procedure.Name}: {(args.Length - argNames.Length)} argument{s1} ignored ({args.Length} given, {argNames.Length} expected)");
                }
            }

            return argDictionary;
        }

        protected override (int returnCode, string statusMessage) Exec(IPreparedExec prepared, DbIdentifier procedure, Dictionary<string, object> args, int timeout, int? ifNoReturn, bool withStatusMessage)
        {
            if (prepared == null)
                throw new ArgumentNullException(nameof(prepared));
            if (!(prepared is PostgreSqlPreparedExec))
                throw new ArgumentException("Argument 'prepared' must be of type PostgreSqlPreparedExec", nameof(prepared));
            var preparedCast = (PostgreSqlPreparedExec)prepared;

            if (timeout < 0)
                timeout = DefaultExecTimeout;

            var sanitized = procedure.Sanitize();
            using (NpgsqlCommand cmd = preparedCast.Connection.CreateCommand())
            {
                cmd.CommandText = sanitized;
                cmd.CommandTimeout = timeout;
                cmd.CommandType = CommandType.StoredProcedure;

                if (args != null)
                {
                    foreach (var entry in args)
                    {
                        NpgsqlParameter p = new NpgsqlParameter(entry.Key, entry.Value);
                        cmd.Parameters.Add(p);
                    }
                }

                NpgsqlParameter statusMessageParam;
                if (withStatusMessage)
                {
                    statusMessageParam = cmd.Parameters.Add("status_message", NpgsqlDbType.Text, -1);
                    statusMessageParam.Direction = ParameterDirection.Output;
                }
                else
                {
                    statusMessageParam = null;
                }

                object oReturn = null;
                _logger.LogInformation($"Exec procedure {sanitized} ...");
                try
                {
                    oReturn = cmd.ExecuteScalar();
                }
                catch (NpgsqlException e) when (preparedCast.CatchExceptions)
                {
                    if (e is PostgresException)
                        preparedCast.Logger.LogPostgresSql((PostgresException)e, prefix: preparedCast.LogPrefix);
                    else
                        preparedCast.Logger.LogCritical($"{preparedCast.LogPrefix}{e.Message}");
                }

                string statusMessage;
                if (withStatusMessage && statusMessageParam != null)
                {
                    statusMessage = Convert.ToString(statusMessageParam.Value);
                    return (-1, statusMessage); // Note: postgresql functions with OUT parameters cannot return values: we return -1.
                }
                else
                {
                    statusMessage = null;
                }

                if (oReturn == null)
                {
                    var message = $"Procedure {sanitized} returned no value.";

                    if (ifNoReturn.HasValue)
                    {
                        _logger.LogDebug(message);
                        return (ifNoReturn.Value, statusMessage);
                    }
                    if (preparedCast.CatchExceptions)
                    {
                        _logger.LogError(message);
                        return (-1, statusMessage);
                    }
                    else
                    {
                        throw new MessageException(message, level: LogLevel.Critical);
                    }
                }
                
                // Convert to int
                try
                {
                    return (Convert.ToInt32(oReturn), statusMessage);
                }
                catch (Exception)
                {
                    var message = $"Procedure {sanitized} returned a {oReturn.GetType()} value (\"{oReturn.ToString()}\") which cannot be converted to int.";
                    if (preparedCast.CatchExceptions)
                    {
                        _logger.LogError(message);
                        return (-1, statusMessage);
                    }
                    else
                    {
                        throw new MessageException(message, level: LogLevel.Critical);
                    }
                }
            }
        }
        
        private Dictionary<IPreparedExec, string> _lastSqlErrorCodes = new Dictionary<IPreparedExec, string>();

        public override IPreparedExec PrepareExec(DbConnection connection, ILogger logger, string logPrefix = null, bool catchExceptions = false)
        {
            if (connection == null)
                throw new ArgumentNullException(nameof(connection));
            if (!(connection is NpgsqlConnection))
                throw new ArgumentException($"NpgsqlConnection required, got {connection.GetType()}", nameof(connection));

            if (logger == null)
                throw new ArgumentNullException(nameof(logger));

            if (connection.State == ConnectionState.Closed)
                connection.Open();

            var prepared = new PostgreSqlPreparedExec((NpgsqlConnection)connection, logger, logPrefix, catchExceptions);

            ((NpgsqlConnection)connection).Notice += delegate (object sender, NpgsqlNoticeEventArgs e)
            {
                _lastSqlErrorCodes[prepared] = e.Notice.SqlState;
                logger.LogPostgresSql(e.Notice, prefix: logPrefix);
            };
            
            return prepared;
        }

        #endregion
    }
}
