Create a command-line application
=================================

## Application startup

Command applications are defined with [CommandApplication.CreateDefaultBuilder](xref:Wheel.Commands.CommandApplication) using a _Startup_ class.

Startup classes must have a `ConfigureCommands` (or `ConfigureCommand`, which is an alias) method with a @Wheel.Commands.ICommandBuilder argument to define the commands. They may also define a `ConfigureServices` method with a `IServiceCollection` argument to add services to the application.

In the following example, the Program class is used itself as the Startup class.

```c#
class Program
{
	static int Main(string[] args)
		=> CommandApplication.CreateDefaultBuilder()
			.UseStartup<Program>()
			.Build()
			.Run(args);
	
	public void ConfigureServices(IServiceCollection services)
	{
		services.AddOptions();
		services.Configure<SmtpOptions>(_config.GetSection("Smtp"));
		services.AddSingleton<IEmailSender, EmailSender>();
		
		// ...
		// Add other services
		// ...
	}

	public void ConfigureCommands(ICommandBuilder root)
	{
		root.AddCommands(); // Define commands with attributes
		root.Command<MyCommand>(); // Define command with ConfigureCommand method
		root.Command("hello", cmd => ...); // Define command explicitly
	}
}
```

## Define commands with attributes

In the above example, `root.AddCommands()` registers all classes having the [Command](xref:Wheel.Commands.CommandAttribute) attribute, in the application assembly. As an alternative, commands could have been added one by one with `root.Command<MyCommand>()`.

These classes must define a method named `Run`, taking the command arguments as parameters, and returning either an integer or void. For example:

```c#
public int Run(string[] args)
{
	//...
}
```

The arguments may also be splitted and/or have target types, such as:

```c#
public int Run(string arg1, int arg2, string[] remainingArgs)
{
	//...
}
```

The `Required` argument (from System.ComponentModel.DataAnnotations) may be used to indicate that an argument is required: an exception will be raised if it is missing. Note that if the argument is missing and is not required, the default value of the argument type will be used (0 for int, null for string).

The command classes may also define command-line options, using properties with the [CommandOption](xref:Wheel.Commands.CommandOptionAttribute) attribute.

A public constructor may also be used to retrieve application services.

Example:

```c#
[Command(Description = "Add or multiply two integers")]
class AddCommand
{
	private ILogger _logger;

	public AddCommand(ILogger<AddCommand> logger)
	{
		_logger = logger;
	}

	[CommandOption("-m|--multiply", Description = "If set, multiply instead of add")]
	public bool Multiply { get; set; }

	public int Run([Required] int num1, [Required] int num2)
	{
		int result;
		if (Multiply)
			result = num1 * num2;
		else
			result = num1 + num2;
		
		_logger.LogInformation("Result is: {result}", result);
		return result;
	}
}
```

## Define commands with ConfigureCommand method

The instruction `root.Command<MyCommand>()` can also be used in the startup ConfigureCommands method for commands which **do not** have the `[Command]` attribute.

In this case, `MyCommand` must be defined with a `ConfigureCommand` (or `ConfigureCommands`, which is an alias) method inside the `MyCommand` method. These methods are actually the same as in the Startup class.

It is possible to create a hierarchical application where the Startup.ConfigureCommands method defines commands, which themselve define sub-commands with their own ConfigureCommands method, which again define sub-commands, etc.

Note that the ConfigureCommands method may be static (in this case, the class will not be implicitly instanciated by the command runner).

Example:

```c#
class MyCommand
{
	public static void ConfigureCommand(ICommandBuilder cmd, IServiceCollection services)
	{
		services.AddTransient<MyCommand>();

		var numOpt = cmd.Option("-n|--num", "Num", required: true);
		cmd.Action = () =>
		{
			var o = cmd.Services.GetRequiredService<MyCommand>();
			o.SayHello(numOpt.IntValue);
		};
	}

	private readonly ILogger _logger;

	public MyCommand(ILogger<MyCommand> logger)
	{
		_logger = logger;
	}

	public void SayHello(int num)
	{
	   _logger.LogError($"Test direct {num}");
	}
}
```

## Define commands explicitly

Commands may also be defined explicitly in the Startup.ConfigureCommands method, using the @Wheel.Commands.ICommandBuilder interface.

Example:

```c#
class Program
{
	// ...

	public void ConfigureCommands(ICommandBuilder root)
	{
		// ...
		
		root.Command("hello", cmd =>
		{
			cmd.Description = "Say hello";
			var toArg = cmd.Argument("to", "Who do you want to say hello to?");
			cmd.Action = () =>
			{
				Console.WriteLine("Hello, {0}", toArg.HasValue ? toArg.Value : "world");
			};
		});
	}
}
```

