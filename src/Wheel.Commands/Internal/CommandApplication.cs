﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.DependencyInjection;
using System.Threading;

namespace Wheel.Commands.Internal
{
    internal sealed class CommandApplication : ICommandApplication
    {
        private readonly WheelInfra _infra;
        private readonly CommandApplicationOptions _options;
        private readonly ILogger _logger;
        private readonly ICommandBuilder _rootCommand;

        public CommandApplication(ICommandBuilder rootCommand, CommandApplicationOptions options, WheelInfra infra)
        {
            _infra = infra;
            _options = options;
            _logger = _infra.LoggerFactory.CreateLogger<CommandApplication>();
            _rootCommand = rootCommand;
        }

        public IServiceProvider Services => _infra.ApplicationServices;
        
        public int Run(string[] args)
        {
            if (_rootCommand == null)
                throw new MessageException("No root command has been defined for the application");

            bool interactive = args.Length == 1 && args[0] == "?";

            try
            {
                if (interactive)
                    return RunInteractive();
                else
                    return RunDirect(args);
            }
            catch (Exception e) when (e is MessageException || !Debugger.IsAttached)
            {
                // We do not catch non-message exceptions if debugger is attached so that we can spot the origin of the exception with the debugger
                return HandleException(e);
            }
            finally
            {
                NLog.LogManager.Flush();
                if (! interactive && Debugger.IsAttached)
                {
                    Console.Error.WriteLine("Press any key to terminate ...");
                    Console.ReadKey();
                }
            }
        }
        
        private int RunDirect(params string[] args)
        {
            StartCommand(_rootCommand, args);
            return WaitCommands();
        }

        private int RunInteractive()
        {
            int returnCode = 0;
            Console.WriteLine("Interactive mode (help: h, quit: q)");
            while (true)
            {
                Console.Write("> ");
                var argsLine = (Console.ReadLine() ?? "").Trim();

                try
                {
                    if (argsLine == "q" || argsLine == "quit" || argsLine == "exit")
                    {
                        return returnCode;
                    }
                    else if (argsLine == "?" || argsLine == "h" || argsLine == "help")
                    {
                        returnCode = RunDirect(new string[] { "-h" });
                    }
                    else
                    {
                        var args = StringUtils.SplitCommandLine(argsLine);
                        returnCode = RunDirect(args);
                    }
                }
                catch (Exception e) when (e is MessageException || !Debugger.IsAttached)
                {
                    // We do not catch non-message exceptions if debugger is attached so that we can spot the origin of the exception with the debugger
                    returnCode = HandleException(e);
                }
            }
        }
        
        private void StartCommand(ICommandBuilder command, params string[] args)
        {
            if (command == null)
                throw new ArgumentNullException(nameof(command));
            if (!(command is CommandBuilder))
                throw new InvalidOperationException(string.Format("Cannot start command builders of type {0}.", command.GetType()));
            
            var commandToExecute = CommandParser.Parse((CommandBuilder)command, args);

            if (commandToExecute == null)
            {
                RegisterForWaitCommands(ReturnCode.OK);
            }
            else if (commandToExecute.Func == null)
            {
                if (commandToExecute.Parent == null)
                    _logger.LogError("No command have been provided");
                else
                    _logger.LogError("No implementation given for command {0}", commandToExecute.Name);

                RegisterForWaitCommands(ReturnCode.ERROR);
            }
            else
            {
                var status = new CommandRunnerStatus
                {
                    CommandName = commandToExecute.Name
                };
                status.Thread = new Thread(() =>
                {
                    using (var context = new CommandRunner(_infra, _options, commandToExecute))
                    {
                        status.ReturnCode = context.Run();
                    }
                });
                status.Thread.Start();
                RegisterForWaitCommands(status);
            }
        }


        private readonly List<CommandRunnerStatus> _waitedRuns = new List<CommandRunnerStatus>();
        private int _maxWaitedReturnCode = ReturnCode.OK;

        private void RegisterForWaitCommands(int returnCode)
        {
            if (returnCode > _maxWaitedReturnCode)
                _maxWaitedReturnCode = returnCode;
        }

        private void RegisterForWaitCommands(CommandRunnerStatus run)
        {
            _waitedRuns.Add(run);
        }

        private int WaitCommands()
        {
            int returnCode = _maxWaitedReturnCode;

            foreach (var run in _waitedRuns)
            {
                // Console.WriteLine("Will join for {0} ({1} / {2}): {2}", run.CommandName, run.GetHashCode(), run.ReturnCode.GetHashCode(), run.ReturnCode);
                run.Thread.Join();
                // Console.WriteLine("Joined for {0} ({1} / {2}): {2}", run.CommandName, run.GetHashCode(), run.ReturnCode.GetHashCode(), run.ReturnCode);
            }

            foreach (var run in _waitedRuns)
            {
                if (run.ReturnCode == null)
                {
                    _logger.LogError("Command {0} did not return any value", run.CommandName);
                    if (returnCode < ReturnCode.ERROR)
                        returnCode = ReturnCode.ERROR;
                }
                else if (run.ReturnCode.Value > returnCode)
                {
                    returnCode = run.ReturnCode.Value;
                }
            }

            return returnCode;
        }

        /// <summary>
        /// Log exception raised during application execution.
        /// </summary>
        /// <remarks>
        /// These exceptions were not caught by <see cref="CommandRunner"/>.
        /// They include command parsing exceptions (as MessageException).
        /// </remarks>
        private int HandleException(Exception e)
        {
            if (e is MessageException)
            {
                var level = ((MessageException)e).Level;

                if (e.InnerException != null)
                    LoggingUtils.SafeLog(_logger, level, e.InnerException, e.Message);
                else
                    LoggingUtils.SafeLog(_logger, level, e.Message);

                if (level <= LogLevel.Information)
                    return ReturnCode.OK;
                else
                    return ReturnCode.GENERAL_ERROR; // the report email might not have arrived
            }
            else
            {
                LoggingUtils.SafeLog(_logger, LogLevel.Critical, e, e.Message);
                return ReturnCode.GENERAL_ERROR; // the report email might not have arrived
            }
        }
    }
}
