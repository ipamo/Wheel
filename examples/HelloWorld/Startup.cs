﻿using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using Wheel;
using Wheel.Commands;

namespace HelloWorld
{
    class Startup
    {
        private readonly IHostEnvironment _env;
        private readonly IConfiguration _config;

        public Startup(IHostEnvironment env, IConfiguration config)
        {
            _env = env;
            _config = config;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddOptions();

            services.Configure<SmtpOptions>(_config.GetSection("Smtp"));
            services.AddSingleton<IEmailSender, EmailSender>();
            services.AddSingleton<ICssInliner, CssInliner>();
        }

        public void ConfigureCommands(ICommandBuilder root)
        {
            root.DefaultCommand<HelloCommand>("hello");
            // ... add other definitions here ...
        }
    }
}
