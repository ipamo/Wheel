Wheel
=====

Wheel provides a standardized way to use basic infrastructure services (logging, configuration, dependency injection, command system, database access, etc) on .NET console and web applications.

Most components are compatible with .NET core >= 2.0. and .NET classic framework (including the obsolete .NET framework 2.0).

## Features

### Common scenarios

- **Wheel.Data** : Abstraction layer for access to databases, execution of stored procedures, and configuration of EntityFrameworkCore DbContext classes. Packages **Wheel.Data.PostgreSql**, **Wheel.Data.SqlServer** and **Wheel.Data.Oracle** are implementations of Wheel.Data on particular database systems.

- **Wheel.Commands** : Command system compatible with .NET 2.0 : definition of commands, parsing of command-line arguments, execution of commands. Commands are added to the application using a Startup class (similarly to ASP.NET Core). Each command can be defined either explicitly (using [ICommandBuilder](http://ipamo.net/Wheel/obj/api/Wheel.Commands.ICommandBuilder.html) interface), or with a class containing a _ConfigureCommand_ method, or with a class using [Command](http://ipamo.net/Wheel/obj/api/Wheel.Commands.CommandAttribute.html) and [CommandOption](http://ipamo.net/Wheel/obj/api/Wheel.Commands.CommandOptionAttribute.html) attributes.

- **Wheel.Testing** : Utilities for unit testing using [xunit](https://xunit.github.io/) framework.

### Specific tools

- **Wheel.Ftp** : A FTP client library compatible with .NET core >= 2.0 and .NET classic framework >= 2.0. It can resume previously interrupted uploads (tested with Microsoft FTP Service). Adapted from [edtFTPnet](https://enterprisedt.com/products/edtftpnet/), Free edition (licensed under the GNU LGPL).

### Base librairies

- **Wheel** : Common features used in most scenarios, with minimal dependencies.

- **Wheel.Plus** : Additional features that might not be usefull for every project and have add many dependencies.

- **Wheel.Backports** and **Wheel.Plus.Backports** : Adaptation of parts of Microsoft.Extensions for the obsolete .NET framework 2.0. The original namespaces and API are preserved as much as possible.

## Usage examples

- [Create a command-line application](articles/examples/command-line-application.md).
- [Create a web application with commands in the same assembly](articles/examples/web-application-with-commands.md).
- [Use IDbms to configure DbContext classes](articles/examples/configure-dbcontext.md).

## Documentation

The complete [API documentation](http://ipamo.net/Wheel/obj/api/Wheel.html) is available at the [project home](http://ipamo.net/Wheel).

## Nuget

Binaries are available as [Nuget packages](https://www.nuget.org/profiles/ipamo).

## Gitlab

Source code is available at the project [Gitlab repository](https://gitlab.com/ipamo/Wheel).

## License

Wheel is open source software licensed under the LGPLv3 (see [LICENSE.txt](LICENSE.txt)).

[![Lesser General Public License](https://www.gnu.org/graphics/lgplv3-147x51.png)](https://www.gnu.org/licenses/lgpl-3.0.en.html)
