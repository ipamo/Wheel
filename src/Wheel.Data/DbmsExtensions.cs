﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Wheel.Data
{
    public static class DbmsExtensions
    {
        public static bool IsDateOnlyProperty(this IDbms dbms, string property)
        {
            if (dbms.DateOnlyProperties == null)
                return false;

            return dbms.DateOnlyProperties.Contains(property);
        }

        public static bool IsInsertedAtProperty(this IDbms dbms, string property)
        {
            if (property == dbms.InsertedAtProperty)
                return true;

            if (dbms.InsertedAtAlternatives == null)
                return false;

            return dbms.InsertedAtAlternatives.Contains(property);
        }

        public static bool IsUpdatedAtProperty(this IDbms dbms, string property)
        {
            if (property == dbms.UpdatedAtProperty)
                return true;

            if (dbms.UpdatedAtAlternatives == null)
                return false;

            return dbms.UpdatedAtAlternatives.Contains(property);
        }

        public static bool IsDefaultNowProperty(this IDbms dbms, string property)
        {
            if (dbms.IsInsertedAtProperty(property))
                return true;

            if (dbms.IsUpdatedAtProperty(property) && dbms.AssignUpdatedPropertiesAtInsertion)
                return true;

            return false;
        }
    }
}
