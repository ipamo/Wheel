﻿using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using Wheel.Internal;

namespace Wheel
{
    public class WheelInfra
    {
        internal Func<IServiceCollection, IServiceProvider> DependencyInjectionBuilder { get; set; }
#if !NET20
            = ServiceCollectionContainerBuilderExtensions.BuildServiceProvider;
#endif

        public IServiceCollection ServiceCollection { get; private set; }

        public IApplicationInformation ApplicationInformation { get; internal set; }

        public IHostEnvironment Environment { get; internal set; }

        public HostBuilderContext BuilderContext { get; internal set; }

        public IConfiguration Configuration { get; internal set; }

        public ILoggerFactory LoggerFactory { get; private set; }

        public IReporterManager ReporterManager { get; private set; }

        public int ReturnOK { get; private set; }
        public int ReturnWarning { get; private set; }
        public int ReturnError { get; private set; }
        public int ReturnCritical { get; private set; }

        private IServiceProvider _infraServices;
        private IServiceProvider _applicationServices;

        internal WheelInfra()
        {
            ServiceCollection = new ServiceCollection();
            ReturnOK = 0;
            ReturnWarning = (int) LogLevel.Warning;
            ReturnError = (int)LogLevel.Error;
            ReturnCritical = (int)LogLevel.Critical;
        }

#if NET20
        internal void ConfigureLogging(Action<HostBuilderContext, ILoggerFactory> configureLogging)
        {
            if (configureLogging == null)
                throw new ArgumentNullException(nameof(configureLogging));

            LoggerFactory = new LoggerFactory();
            configureLogging(BuilderContext, LoggerFactory);
            ServiceCollection.AddSingleton(LoggerFactory);
            ServiceCollection.AddSingleton(typeof(ILogger<>), typeof(Logger<>));
        }
#else
        internal void ConfigureLogging(Action<HostBuilderContext, ILoggingBuilder> configureLogging)
        {
            if (configureLogging == null)
                throw new ArgumentNullException(nameof(configureLogging));

            ServiceCollection.AddLogging(builder => configureLogging(BuilderContext, builder));
            // Creation of the logger factory is delayed after the creation of the infra service provider, see EndConfigure()
        }
#endif

        internal void EndConfigure()
        {
            ServiceCollection.AddSingleton(ApplicationInformation);
            ServiceCollection.AddSingleton(Environment);
            ServiceCollection.AddSingleton(Configuration);
            ServiceCollection.AddSingleton(BuilderContext);
            
            if (DependencyInjectionBuilder != null)
            {
                _infraServices = DependencyInjectionBuilder(ServiceCollection);

#if !NET20
                LoggerFactory = _infraServices.GetRequiredService<ILoggerFactory>();
                ServiceCollection = CreateServiceCollection();
#endif
            }
            else
            {
#if !NET20
                throw new InvalidOperationException("A dependency injection builder is required");
#endif
            }

            var reporterManager = new ReporterManager();
            LoggerFactory.AddProvider(new SingleLoggerProvider(reporterManager));
            ReporterManager = reporterManager;
            ServiceCollection.AddSingleton(ReporterManager);
        }

        public IDictionary<Type, object> GetInfraServicesDictionary()
        {
            return new Dictionary<Type, object>
            {
                { typeof(IApplicationInformation), ApplicationInformation },
                { typeof(IHostEnvironment), Environment },
                { typeof(HostBuilderContext), BuilderContext },
                { typeof(IConfiguration), Configuration },
                { typeof(ILoggerFactory), LoggerFactory },
                { typeof(IReporterManager), ReporterManager },
            };
        }

        public bool IsDependencyInjectionEnabled => DependencyInjectionBuilder != null;

        public bool HasInfraServices => _infraServices != null;

        public IServiceProvider InfraServices
        {
            get
            {
                if (DependencyInjectionBuilder == null)
                    throw new InvalidOperationException("Cannot get infrastructure service provider: dependency injection is not enabled.");
                return _infraServices;
            }
        }

        public bool HasApplicationServices => _applicationServices != null;

        public IServiceProvider ApplicationServices
        {
            get
            {
                if (DependencyInjectionBuilder == null)
                    throw new InvalidOperationException("Cannot get application service provider: dependency injection is not enabled.");
                if (_applicationServices == null)
                    throw new InvalidOperationException("Application service provider has not been build yet.");
                return _applicationServices;
            }
        }

        public void RegisterReporter(IReporter reporter)
            => ReporterManager.Register(reporter);

        public void UnregisterReporter()
            => ReporterManager.Unregister();
        
        public IServiceCollection CreateServiceCollection()
        {
            var infraServices = GetInfraServicesDictionary();
            var serviceCollection = new ServiceCollection();
            foreach (var descriptor in ServiceCollection)
            {
                if (infraServices.ContainsKey(descriptor.ServiceType))
                {
                    // We reuse the instance from infrastructure services.
                    // Necessary for ILoggerFactory (for example): otherwise, any additional
                    // configuration made on the infrastructure ILoggerFactory (e.g. AddProvider)
                    // would be lost on application ILoggerFactory
                    var instance = infraServices[descriptor.ServiceType];
                    serviceCollection.AddSingleton(descriptor.ServiceType, instance);
                }
                else
                {
                    ((IList<ServiceDescriptor>)serviceCollection).Add(descriptor);
                }
            }
            return serviceCollection;
        }

        public IServiceProvider BuildApplicationServices()
        {
            if (DependencyInjectionBuilder == null)
                throw new InvalidOperationException("Cannot build service provider: dependency injection is not enabled.");
            
            _applicationServices = DependencyInjectionBuilder(ServiceCollection);
            return _applicationServices;
        }
    }
}
