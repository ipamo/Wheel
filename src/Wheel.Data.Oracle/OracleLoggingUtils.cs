﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.CompilerServices;

namespace Wheel.Data
{
    /* TODO
         
    public static class OracleLoggingUtils
    {
        private static LogLevel GetLevelFromPostgreSqlLevel(string level)
        {
            switch (level)
            {
                case "DEBUG":
                    return LogLevel.Trace;
                case "LOG":
                    return LogLevel.Debug;
                case "INFO":
                case "NOTICE":
                    return LogLevel.Information;
                case "WARNING":
                    return LogLevel.Warning;
                case "EXCEPTION":
                    return LogLevel.Critical;
                default:
                    return LogLevel.Error;
            }
        }

        /// <summary>
        /// Write a C# log from a Npgsql notice.
        /// </summary>
        /// <remarks>
        /// Conventions:
        /// 
        /// 	RAISE DEBUG 'format', expression, ...;
        /// 	RAISE LOG 'format', expression, ...;
        /// 	RAISE INFO 'format', expression, ...;
        /// 	RAISE WARNING 'format', expression, ...;
        /// 	RAISE EXCEPTION 'format', expression, ...;
        /// 
        /// </remarks>
        public static void LogPostgresSql(this ILogger logger, PostgresNotice e, string prefix = null)
        {
            var level = GetLevelFromPostgreSqlLevel(e.Severity);

            string fullPrefix;
            if (prefix == "")
                fullPrefix = "";
            else if (prefix != null)
                fullPrefix = $"{prefix}";
            //else if (!string.IsNullOrEmpty(e.Routine) && !e.Routine.StartsWith("p_log"))
            //    fullPrefix = $"[{e.Routine}, line {e.Line}] ";
            else
                fullPrefix = "";

            var message = e.MessageText;
            if (message != null)
            {
                if (level == LogLevel.Trace && message.StartsWith("[TRACE] "))
                    message = message.Substring(8);
                else if (level == LogLevel.Debug && message.StartsWith("[DEBUG] "))
                    message = message.Substring(8);
                else if (level == LogLevel.Information && message.StartsWith("[ INFO] "))
                    message = message.Substring(8);
                else if (level == LogLevel.Warning && message.StartsWith("[ WARN] "))
                    message = message.Substring(8);
                else if (level == LogLevel.Error && message.StartsWith("[ERROR] "))
                    message = message.Substring(8);
                else if (level == LogLevel.Critical && message.StartsWith("[CRITICAL] "))
                    message = message.Substring(8);
            }
            
            logger.Log(level, $"{fullPrefix}{message}");
        }

        /// <summary>
        /// Write a C# log from a Npgsql exception.
        /// </summary>
        /// <remarks>
        /// Conventions:
        /// 
        /// 	RAISE DEBUG 'format', expression, ...
        /// 	RAISE LOG 'format', expression, ...
        /// 	RAISE INFO 'format', expression, ...
        /// 	RAISE WARNING 'format', expression, ...
        /// 	RAISE EXCEPTION 'format', expression, ...
        /// 
        /// </remarks>
        public static void LogPostgresSql(this ILogger logger, PostgresException e, string prefix = null)
        {
            var level = GetLevelFromPostgreSqlLevel(e.Severity);

            string fullPrefix;
            if (prefix == "")
                fullPrefix = "";
            else if (prefix != null)
                fullPrefix = $"{prefix}";
            else if (!string.IsNullOrEmpty(e.Routine) && !e.Routine.StartsWith("p_log"))
                fullPrefix = $"[{e.Routine}, line {e.Line}] ";
            else
                fullPrefix = "";
            
            logger.Log(level, $"{fullPrefix}{e.Message}");
        }
    }
    */
}
