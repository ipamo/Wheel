﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Text;

namespace Wheel.Data
{
    /// <summary>
    /// Default DBMS features for Oracle.
    /// </summary>
    public class OracleDbms : BaseDbms
    {
        public OracleDbms(IConfiguration config,
                 ILoggerFactory loggerFactory,
                string defaultCskey = null)
            : base(config,
                  loggerFactory,
                  defaultCskey)
        {

        }

        public override Type DbConnectionType => typeof(OracleConnection);

        public override string DefaultSchema => null;
        public override string DateOnlyNowSql => "SYSDATE";
        public override string DateTimeNowSql => "SYSDATE";

        protected override char IdentifierProtectBegin => '\"';
        protected override char IdentifierProtectEnd => '\"';

        public override string GetTableName(Type entityType) => GetBaseTableName(entityType).ToUpperSnake();
        
        public override string GetColumnName(Type entityType, string propertyName) => propertyName.ToUpperSnake();

        public override DbConnection InstanciateConnection(string connectionString) => new OracleConnection(connectionString);

        public override string GetDatabaseName(DbConnection connection)
        {
            using (var cmd = new OracleCommand("SELECT SYS_CONTEXT('USERENV','INSTANCE_NAME') FROM DUAL", (OracleConnection)connection))
            {
                return (string)cmd.ExecuteScalar();
            }
        }
        
        protected override object ConvertParameterValue(object value)
        {
            if (value == null)
                return null;

            value = base.ConvertParameterValue(value);
            var type = value.GetType();
            
            if (type == typeof(ulong))
                return Convert.ChangeType(value, typeof(long));
            if (type == typeof(uint))
                return Convert.ChangeType(value, typeof(long));
            if (type == typeof(ushort))
                return Convert.ChangeType(value, typeof(long));

            return value;
        }
        
        #region Component

        /* TODO
        private string _componentGetSql;

        private void PrepareComponentSql()
        {
            if (_componentGetSql == null)
            {
                var table = SanitizeIdentifier(GetTableName(typeof(Wheel.Data.Component)));
                var idColumn = SanitizeIdentifier(GetColumnName(typeof(Wheel.Data.Component), nameof(Wheel.Data.Component.Id)));
                var nameColumn = SanitizeIdentifier(GetColumnName(typeof(Wheel.Data.Component), nameof(Wheel.Data.Component.Name)));

                _componentGetSql = $@"WITH new_row AS (
INSERT INTO {table} ({nameColumn})
SELECT @name
WHERE NOT EXISTS (SELECT 1 FROM {table} WHERE {nameColumn} = @name)
RETURNING {idColumn}
)
SELECT {idColumn} FROM new_row
UNION
SELECT {idColumn} FROM {table} WHERE {nameColumn} = @name;";
            }
        }
        */

        /// <summary>
        /// Renvoit l'identifiant du composant (table TT_Composant) portant le libellé donné.
        /// Le composant est créé s'il n'existe pas déjà.
        /// </summary>
        public override short GetComponentId(string name)
        {
            throw new NotImplementedException("TODO");
            /*
            PrepareComponentSql();

            using (var cmd = new OracleCommand(_componentGetSql, (OracleConnection)DefaultConnection))
            {
                cmd.Parameters.AddWithValue("@name", name);
                return Convert.ToInt16(cmd.ExecuteScalar());
            }
            */
        }

        #endregion


        #region Dict
        
        /*
        private string _dictSetSql;
        private string _dictGetSql;
        private string _dictRemoveSql;

        private void PrepareDictSql()
        {
            throw new NotImplementedException("TODO");

            if (_dictSetSql == null)
            {
                var componentId = GetComponentId<OracleDbms>();
                var table = SanitizeIdentifier(GetTableName(typeof(Wheel.Data.Dict)));
                var idColumn = SanitizeIdentifier(GetColumnName(typeof(Wheel.Data.Dict), nameof(Wheel.Data.Dict.Id)));
                var valueColumn = SanitizeIdentifier(GetColumnName(typeof(Wheel.Data.Dict), nameof(Wheel.Data.Dict.Value)));
                var insertedByComponentIdColumn = SanitizeIdentifier(GetColumnName(typeof(Wheel.Data.Dict), InsertedByComponentIdColumn));
                var updatedByComponentIdColumn = SanitizeIdentifier(GetColumnName(typeof(Wheel.Data.Dict), UpdatedByComponentIdColumn));
                var updatedAtColumn = SanitizeIdentifier(GetColumnName(typeof(Wheel.Data.Dict), UpdatedAtColumn));

                _dictSetSql = $@"INSERT INTO {table} ({idColumn}, {valueColumn}, {insertedByComponentIdColumn})
VALUES (@id, @value, {componentId})
ON CONFLICT ({idColumn}) DO UPDATE 
SET {valueColumn} = excluded.value, {updatedByComponentIdColumn} = {componentId}, {updatedAtColumn} = {DefaultDateTimeSql}";

                _dictGetSql = $@"SELECT {valueColumn} FROM {table} WHERE {idColumn} = @id";

                _dictRemoveSql = $@"DELETE FROM {table} WHERE {idColumn} = @id";
            }
        }
        */
        
        protected override void SetDictValue(string key, string value)
        {
            throw new NotImplementedException("TODO");

            /*
            PrepareDictSql();

            using (var cmd = new NpgsqlCommand(_dictSetSql, (NpgsqlConnection)DefaultConnection))
            {
                cmd.Parameters.AddWithValue("@id", key);
                cmd.Parameters.AddWithValue("@value", value);
                cmd.ExecuteNonQuery();
            }
            */
        }

        public override string GetDictValue(string key)
        {
            throw new NotImplementedException("TODO");

            /*
            PrepareDictSql();

            using (var cmd = new NpgsqlCommand(_dictGetSql, (NpgsqlConnection)DefaultConnection))
            {
                cmd.Parameters.AddWithValue("@id", key);
                var r = cmd.ExecuteScalar();
                if (r == null || r == DBNull.Value || (string)r == "")
                    return null;
                else
                    return (string)r;
            }
            */
        }

        public override void RemoveDictValue(string key)
        {
            throw new NotImplementedException("TODO");

            /*
            PrepareDictSql();

            using (var cmd = new NpgsqlCommand(_dictRemoveSql, (NpgsqlConnection)DefaultConnection))
            {
                cmd.Parameters.AddWithValue("@id", key);
                cmd.ExecuteNonQuery();
            }
            */
        }

        #endregion


        #region Stored procedures

        protected override Dictionary<string, object> GetNamedArgs(IPreparedExec prepared, DbIdentifier procedure, object[] args)
        {
            throw new NotImplementedException("TODO");

            /*
            if (prepared == null)
                throw new ArgumentNullException(nameof(prepared));
            if (!(prepared is PostgreSqlPreparedExec))
                throw new ArgumentException("Argument 'prepared' must be of type PostgreSqlPreparedExec", nameof(prepared));
            var preparedCast = (PostgreSqlPreparedExec)prepared;
            
            Dictionary<string, object> argDictionary = null;
            if (args != null && args.Length >= 1)
            {
                var databaseSqlPart = procedure.Catalog != null ? procedure.SanitizedCatalog + "." : "";

                var sql = $@"SELECT proargnames
FROM {databaseSqlPart}pg_catalog.pg_proc p
INNER JOIN {databaseSqlPart}pg_catalog.pg_namespace n ON (p.pronamespace = n.oid)
WHERE proname = @procedure AND nspname = @schema AND pronargs >= @nbargs";

                // Détermine le nom des paramètres
                string[] argNames;
                using (var cmd = new NpgsqlCommand(sql, preparedCast.Connection))
                {
                    cmd.Parameters.AddWithValue("@procedure", procedure.Name);
                    cmd.Parameters.AddWithValue("@schema", procedure.Schema ?? DefaultSchema);
                    cmd.Parameters.AddWithValue("@nbargs", args.Length);

                    using (var r = cmd.ExecuteReader())
                    {
                        if (! r.Read())
                            throw new InvalidOperationException($"No stored procedure '{procedure.Name}' found with at least {args.Length} arguments.");
                        
                        argNames = (string[]) r.GetValue(0);

                        if (r.Read())
                            throw new InvalidOperationException($"Several stored procedure '{procedure.Name}' found with at least {args.Length} arguments.");
                    }
                }
                
                argDictionary = new Dictionary<string, object>();
                int i = 0;
                while (i < argNames.Length && i < args.Length)
                {
                    argDictionary.Add(argNames[i], args[i]);
                    i++;
                }
                
                if (args.Length > argNames.Length)
                {
                    var s1 = args.Length - argNames.Length > 1 ? "s" : "";
                    _logger.LogWarning($"{procedure.Name}: {(args.Length - argNames.Length)} argument{s1} ignored ({args.Length} given, {argNames.Length} expected)");
                }
            }

            return argDictionary;
            */
        }

        protected override (int returnCode, string statusMessage) Exec(IPreparedExec prepared, DbIdentifier procedure, Dictionary<string, object> args, int timeout, int? ifNoReturn, bool withStatusMessage)
        {
            throw new NotImplementedException("TODO");

            /*
            if (prepared == null)
                throw new ArgumentNullException(nameof(prepared));
            if (!(prepared is PostgreSqlPreparedExec))
                throw new ArgumentException("Argument 'prepared' must be of type PostgreSqlPreparedExec", nameof(prepared));
            var preparedCast = (PostgreSqlPreparedExec)prepared;

            if (timeout < 0)
                timeout = ExecTimeout;

            var sanitized = procedure.Sanitize();
            using (NpgsqlCommand cmd = preparedCast.Connection.CreateCommand())
            {
                cmd.CommandText = sanitized;
                cmd.CommandTimeout = timeout;
                cmd.CommandType = CommandType.StoredProcedure;

                if (args != null)
                {
                    foreach (var entry in args)
                    {
                        NpgsqlParameter p = new NpgsqlParameter(entry.Key, entry.Value);
                        cmd.Parameters.Add(p);
                    }
                }

                NpgsqlParameter retval = cmd.Parameters.Add("@return", NpgsqlDbType.Integer);
                retval.Direction = ParameterDirection.ReturnValue;

                _logger.LogInformation($"Exec procedure {sanitized} ...");
                try
                {
                    cmd.ExecuteNonQuery();
                }
                catch (NpgsqlException e) when (preparedCast.CatchExceptions)
                {
                    if (e is PostgresException)
                        preparedCast.Logger.LogPostgresSql((PostgresException)e, prefix: preparedCast.LogPrefix);
                    else
                        preparedCast.Logger.LogCritical($"{preparedCast.LogPrefix}{e.Message}");
                }
                
                if (cmd.Parameters["@return"] == null || cmd.Parameters["@return"].Value == null)
                {
                    // TODO : seems to be always null. How to retrieve parameter?
                    var message = $"Procedure {sanitized} returned no value.";

                    if (ifNoReturn.HasValue)
                    {
                        _logger.LogDebug(message);
                        return ifNoReturn.Value;
                    }
                    if (preparedCast.CatchExceptions)
                    {
                        _logger.LogError(message);
                        return -1;
                    }
                    else
                    {
                        throw new MessageException(message, level: LogLevel.Critical);
                    }
                }

                return (int)cmd.Parameters["@return"].Value;
            }
            */
        }
        
        private Dictionary<IPreparedExec, string> _lastSqlErrorCodes = new Dictionary<IPreparedExec, string>();

        public override IPreparedExec PrepareExec(DbConnection connection, ILogger logger, string logPrefix = null, bool catchExceptions = false)
        {
            throw new NotImplementedException("TODO");

            /*
            if (connection == null)
                throw new ArgumentNullException(nameof(connection));
            if (!(connection is NpgsqlConnection))
                throw new ArgumentException($"NpgsqlConnection required, got {connection.GetType()}", nameof(connection));

            if (logger == null)
                throw new ArgumentNullException(nameof(logger));

            if (connection.State == ConnectionState.Closed)
                connection.Open();

            var prepared = new PostgreSqlPreparedExec((NpgsqlConnection)connection, logger, logPrefix, catchExceptions);

            ((NpgsqlConnection)connection).Notice += delegate (object sender, NpgsqlNoticeEventArgs e)
            {
                _lastSqlErrorCodes[prepared] = e.Notice.SqlState;
                logger.LogPostgresSql(e.Notice, prefix: logPrefix);
            };
            
            return prepared;
            */
        }

        #endregion
    }
}
