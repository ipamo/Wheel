@echo off
setlocal enabledelayedexpansion

:: Possible environment variables
if "!msbuild!" == "" set msbuild=C:\Program Files (x86)\Microsoft Visual Studio\2019\Community\MSBuild\Current\Bin\MSBuild.exe
if not exist "!msbuild!" set msbuild=C:\Program Files (x86)\Microsoft Visual Studio\2017\Professional\MSBuild\15.0\Bin\MSBuild.exe
if not exist "!msbuild!" set msbuild=C:\Program Files (x86)\Microsoft Visual Studio\2017\Enterprise\MSBuild\15.0\Bin\MSBuild.exe
if not exist "!msbuild!" set msbuild=C:\Program Files (x86)\Microsoft Visual Studio\2017\Community\MSBuild\15.0\Bin\MSBuild.exe

if "!nuget_packages!" == "" set nuget_packages=%USERPROFILE%\.nuget\packages
if "!nuget_localrepo!" == "" set nuget_localrepo=%USERPROFILE%\.nuget\localrepo
if "!nuget_localrepo_symbols!" == "" set nuget_localrepo_symbols=%USERPROFILE%\.nuget\localrepo-symbols

:: Local variables
set dthr=%date:~6,4%%date:~3,2%%date:~0,2%-%time:~0,2%%time:~3,2%%time:~6,2%
set build_dir=build
if not exist "!build_dir!" mkdir "!build_dir!"

:: ----------------------------------------------------------------------------
:: Main
:: ----------------------------------------------------------------------------
if "%~1" == "docfx" (
	:: ---------- pack docfx ----------
	dotnet restore
	call :build_docfx || goto :eof
) else (
if not "%~1" == "" (
	:: ---------- pack one project ----------
	dotnet restore "src\%~1"
	call :build_project "%~1" || goto :eof
) else (
	:: ---------- pack everything ----------
	echo Restore projects
	dotnet restore
	call :build_project Wheel.Backports || goto :eof
	call :build_project Wheel || goto :eof
	call :build_project Wheel.Plus.Backports || goto :eof
	call :build_project Wheel.Plus || goto :eof
	call :build_project Wheel.Commands || goto :eof
	call :build_project Wheel.Data || goto :eof
	call :build_project Wheel.Data.PostgreSql || goto :eof
	call :build_project Wheel.Data.SqlServer || goto :eof
	call :build_project Wheel.Data.Oracle || goto :eof
	call :build_project Wheel.Testing || goto :eof
	call :build_project Wheel.Ftp || goto :eof
	call :build_docfx || goto :eof
	echo Done
))

goto :eof

:: ----------------------------------------------------------------------------
:: Projects
:: ----------------------------------------------------------------------------
:build_project
	set project=%~1
		
	set version=3.1.0
	if "!project!" == "Wheel.Backports" set version=3.1.1
	
	echo ******************** Build !project! (!version!) ********************
	
	set method=dotnet
	if "!project!" == "Wheel" set method=msbuild
	if "!project!" == "Wheel.Plus" set method=msbuild
	if "!project!" == "Wheel.Backports" set method=msbuild
	if "!project!" == "Wheel.Plus.Backports" set method=msbuild
	if "!project!" == "Wheel.Commands" set method=msbuild
	if "!project!" == "Wheel.Ftp" set method=msbuild
	
	:: ------------------------------------------
	:: Pack
	call :old "!project!.!version!.nupkg" || exit /b 1
	
	if "!method!" == "msbuild" (
		if not exist "!msbuild!" (
			:: Check presence of msbuild
			echo NOT FOUND: MSBuild.exe.
			exit /b 1
		)
		"!msbuild!" "src\!project!" /t:Pack /p:Configuration=Release /verbosity:minimal /p:PackageOutputPath="..\..\!build_dir!" /p:BuildProjectReferences=false
	) else (
		dotnet pack "src\!project!" -c Release -o "..\..\!build_dir!" --no-dependencies
	)
	
	call :check "!project!.!version!.nupkg" || exit /b 1
	call :check "!project!.!version!.symbols.nupkg" || exit /b 1
	
	:: ------------------------------------------
	:: Local repository
	
	if not exist "!nuget_localrepo!" mkdir "!nuget_localrepo!"
	if not exist "!nuget_localrepo_symbols!" mkdir "!nuget_localrepo_symbols!"
	
	echo Push to local repo: !nuget_localrepo!
	dotnet nuget push "!build_dir!\!project!.!version!.nupkg" -s "!nuget_localrepo!" -ss "!nuget_localrepo_symbols!"
	
	:: ------------------------------------------
	:: Local cache
	
	set dir=!nuget_packages!\!project!\!version!
	echo Delete if exist: !dir!
	if exist "!dir!" (
		rd /s /q "!dir!"
		echo Deleted: !dir!
	)
	
	goto :eof

:: ----------------------------------------------------------------------------
:: Documentation
:: ----------------------------------------------------------------------------
:build_docfx

	set version=3.1.0
	
	echo ******************** Build docfx (!version!) ********************

	:: ------------------------------------------
	:: Generate
	
	if exist docfx (
		echo Delete existing docfx
		rd /s /q "docfx"
	)
	
	echo Generate docfx
	docfx --property TargetFramework=netstandard2.0
	move obj\site\README.html obj\site\index.html
	
	:: ------------------------------------------
	:: Move
		
	set dir=!build_dir!\Wheel-Docfx-!version!
	if exist !dir! (
		echo Delete existing !dir!
		rd /s /q "!dir!"
	)
	
	echo Move docfx to !dir!
	move obj\site "!dir!"
	
	:: ------------------------------------------
	:: Zip
	
	call :zip "Wheel-Docfx-!version!" || exit /b 1
	
	goto :eof


:: ----------------------------------------------------------------------------
:: Create zip archive
:: (version: 1, prerequisites: :old, :check, !build_dir!)
:: ----------------------------------------------------------------------------
:zip
	set zip_name=%~1

	call :check "!zip_name!" || exit /b 1
	call :old "!zip_name!.zip" || exit /b 1
	
	echo Create !build_dir!\!zip_name!.zip
	pushd !build_dir!
	7z a "!zip_name!.zip" "!zip_name!" > NUL
	popd

	call :check "!zip_name!.zip" || exit /b 1
	
	goto :eof


:: ----------------------------------------------------------------------------
:: Move to old directory if exists
:: (version: 1, prerequisites: !build_dir!, !dthr!)
:: ----------------------------------------------------------------------------
:old
	set old_name=%~1

	if exist "!build_dir!\!old_name!" (
		echo Move existing !build_dir!\!old_name!
		if not exist "!build_dir!\0-Old" mkdir "!build_dir!\0-Old"
		move "!build_dir!\!old_name!" "!build_dir!\0-Old\!old_name!_!dthr!.backup"
	)
	
	goto :eof


:: ----------------------------------------------------------------------------
:: Ensure the given file and extension exist in build directory
:: (version: 1, prerequisites: !build_dir!)
:: ----------------------------------------------------------------------------
:check
	set check_name=%~1

	if not exist "!build_dir!\!check_name!" (
		echo NOT FOUND: !build_dir!\!check_name!
		exit /b 1
	)
	
	goto :eof
