
public static int? GetUserId(this ClaimsPrincipal user)
{
    if (user == null)
        return null;
    var stringId = user.FindFirstValue(ClaimTypes.NameIdentifier);
    if (string.IsNullOrEmpty(stringId))
        return null;
    return int.Parse(stringId);
}

private static string GetClaimTypeName(string claimType)
{
    if (claimType == ClaimTypes.NameIdentifier)
        return "NameIdentifier";
    if (claimType == ClaimTypes.Name)
        return "Name";
    if (claimType == ClaimTypes.Email)
        return "Email";
    if (claimType == ClaimTypes.AuthenticationMethod)
        return "AuthenticationMethod";
    if (claimType == ClaimTypes.Role)
        return "Role";

    // WindowsAuthentication / AD
    if (claimType == ClaimTypes.PrimarySid)
        return "PrimarySid";
    if (claimType == ClaimTypes.PrimaryGroupSid)
        return "PrimaryGroupSid";
    if (claimType == ClaimTypes.GroupSid)
        return "GroupSid";
    return null;
}

private static string GetSidName(string sid)
{
    var sidClass = new System.Security.Principal.SecurityIdentifier(sid);
    return sidClass.Translate(typeof(System.Security.Principal.NTAccount))?.ToString();
}



public static string SerializeClaimsPrincipal(ClaimsPrincipal principal)
{
    if (principal == null)
        return null;

    var identitiesData = new List<object>();
    foreach (var identity in principal.Identities)
    {
        identitiesData.Add(new
        {
            name = identity.Name,
            label = identity.Label,
            actor = identity.Actor,
            bootstrapContext = identity.BootstrapContext,
            isAuthenticated = identity.IsAuthenticated,
            authenticationType = identity.AuthenticationType,
            claims = identity.Claims?.Select(x => GetClaimString(x)).ToArray(),
        });
    }

    return JsonSerializer.Serialize(identitiesData, new JsonSerializerOptions
    {
        WriteIndented = true,
        IgnoreNullValues = true
    });
}

private static string GetClaimString(Claim x)
{
    var str = GetClaimTypeName(x.Type) ?? x.Type;
    str += ": ";

    str += x.Value;

    if (x.Type == ClaimTypes.PrimarySid || x.Type == ClaimTypes.PrimaryGroupSid || x.Type == ClaimTypes.GroupSid)
        str += $" ({GetSidName(x.Value)})";

    return str;
}