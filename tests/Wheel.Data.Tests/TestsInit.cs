﻿using System;
using Microsoft.Extensions.Configuration;
using Wheel.Data;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;

namespace Wheel.Data.Tests
{
    /// <summary>
    /// The goal of this class is is to determine whether tests with extended Fact and Theory will be skipped.
    /// <para>
    /// Note: discovery of tests with the extended attributes will fail in case of exception.
    /// If this happens, TestsInit will also fail and display the TypeInitializationException.
    /// </para>
    /// </summary>
    internal static class TestsInit
    {
        static TestsInit()
        {
            var infra = WheelInfraBuilder.CreateDefaultBuilder().Build();
            Configuration = infra.Configuration;
            SetSkipDb();
        }

        public static IConfiguration Configuration { get; }
        public static string SkipDb { get; private set; }

        private static void SetSkipDb()
        {
            if (Configuration.GetValue<bool>("Tests:SkipDb"))
            {
                SkipDb = "Tests:SkipDb directive is set";
                return;
            }
        }
    }
}
