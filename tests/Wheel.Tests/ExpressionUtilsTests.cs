﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;
using Wheel;
using System.IO;

namespace Wheel.Tests
{
    public class ExpressionUtilsTests
    {
        [Fact]
        public void GetExpressionText()
        {
            Assert.Equal("ValueType", ExpressionUtils.GetExpressionText<BaseModel>(b => b.ValueType));
            Assert.Equal("ReferenceType", ExpressionUtils.GetExpressionText<BaseModel>(b => b.ReferenceType));
            Assert.Equal("Child", ExpressionUtils.GetExpressionText<BaseModel>(b => b.Child));
            Assert.Equal("Child.Name", ExpressionUtils.GetExpressionText<BaseModel>(b => b.Child.Name));
            Assert.Equal("ChildList[0].Name", ExpressionUtils.GetExpressionText<BaseModel>(b => b.ChildList[0].Name));

            for (var num = 1; num <= 2; num++)
            {
                Assert.Equal($"ChildList[{num}].Name", ExpressionUtils.GetExpressionText<BaseModel>(b => b.ChildList[num].Name));
            }
        }
        
        private class BaseModel
        {
            public string ReferenceType { get; set; }
            public int ValueType { get; set; }
            public ChildModel Child { get; set; }
            public List<ChildModel> ChildList { get; set; }

            public BaseModel()
            {
                Child = new ChildModel();
                ChildList = new List<ChildModel>();
            }
        }

        private class ChildModel
        {
            public string Name { get;set; }
        }
    }
}