﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Wheel.Commands.Tests.Samples
{
    class StaticCommand
    {
        public static void ConfigureCommand(ICommandBuilder cmd)
        {
            cmd.Description = "Apply an operation to operand '3'";
            var multiplyOpt = cmd.Option("-m", "Multiplication (default is addition)", noValue: true);
            var operandArg = cmd.Argument("[operand]", "2nd operand");
            cmd.Func = () => multiplyOpt.IsEnabled ? 3 * operandArg.IntValue : 3 + operandArg.IntValue;
        }
    }
}
