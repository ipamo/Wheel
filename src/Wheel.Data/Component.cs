﻿using System.ComponentModel.DataAnnotations;

namespace Wheel.Data
{
    /// <summary>
    /// A component in the infrastructure: could be a C# class, a SQL stored procedure or routine, etc.
    /// </summary>
    [AutoColumns]
    public class Component
    {
        public int Id { get; set; }

        [Required, MaxLength(250)]
        public string Name { get; set; }
    }
}
