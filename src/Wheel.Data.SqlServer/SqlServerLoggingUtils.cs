﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using Microsoft.Data.SqlClient;
using System.Text;

namespace Wheel.Data
{
    /// <summary>
    /// Helpers for Sql Server logging.
    /// </summary>
    public static class SqlServerLoggingUtils
    {
        /// <summary>
        /// Get the C# log level associated to a (severity, state) couple.
        /// </summary>
        /// <remarks>
        /// Conventions:
		///
        /// 	RAISERROR('INFORMATION', 0, 0) WITH NOWAIT
        /// 
        /// 	RAISERROR('TRACE', 0, 1) WITH NOWAIT
        /// 	RAISERROR('DEBUG', 0, 2) WITH NOWAIT
        ///     RAISERROR('WARNING', 0, 3) WITH NOWAIT
        /// 	RAISERROR('ERROR', 11, 4) WITH NOWAIT
        /// 	RAISERROR('CRITICAL', 11, 5) WITH NOWAIT
        /// 
        /// </remarks>
        public static LogLevel GetLevel(int severity, int state)
        {
            if (severity < 11)
            {
                if (state == 0)
                    return LogLevel.Information;
                else if (state == 1)
                    return LogLevel.Trace;
                else if (state == 2)
                    return LogLevel.Debug;
                else // recommandation: state == 3
                    return LogLevel.Warning;
            }

            if (state >= 5)
				return LogLevel.Critical;
			else
                return LogLevel.Error;
        }

        /// <summary>
        /// Write a C# log from a SQL error.
        /// </summary>
        /// <remarks>
        /// Conventions:
		///
        /// 	RAISERROR('INFORMATION', 0, 0) WITH NOWAIT
        /// 
        /// 	RAISERROR('TRACE', 0, 1) WITH NOWAIT
        /// 	RAISERROR('DEBUG', 0, 2) WITH NOWAIT
        ///     RAISERROR('WARNING', 0, 3) WITH NOWAIT
        /// 	RAISERROR('ERROR', 11, 4) WITH NOWAIT
        /// 	RAISERROR('CRITICAL', 11, 5) WITH NOWAIT
        /// 
        /// </remarks>
        public static void LogSqlServer(this ILogger logger, SqlError e, string prefix = null)
        {
            var level = GetLevel(e.Class, e.State);

            string fullPrefix;
            if (prefix == "")
                fullPrefix = "";
            else if (prefix != null)
                fullPrefix = $"{prefix}";
            //else if (!string.IsNullOrEmpty(e.Procedure) && !e.Procedure.StartsWith("P_Log"))
            // fullPrefix = $"[{e.Procedure}, line {e.LineNumber}] ";
            else
                fullPrefix = "";

            var message = e.Message;
            if (message != null)
            {
                if (level == LogLevel.Trace && message.StartsWith("[TRACE] "))
                    message = message.Substring(8);
                else if (level == LogLevel.Debug && message.StartsWith("[DEBUG] "))
                    message = message.Substring(8);
                else if (level == LogLevel.Information && message.StartsWith("[ INFO] "))
                    message = message.Substring(8);
                else if (level == LogLevel.Warning && message.StartsWith("[ WARN] "))
                    message = message.Substring(8);
                else if (level == LogLevel.Error && message.StartsWith("[ERROR] "))
                    message = message.Substring(8);
                else if (level == LogLevel.Critical && message.StartsWith("[CRITICAL] "))
                    message = message.Substring(8);
            }

            logger.Log(level, default(EventId), $"{fullPrefix}{message}", null, MessageFormatter);
        }

        /// <summary>
        /// Write a C# log from a SQL exception.
        /// </summary>
        /// <remarks>
        /// Conventions:
		///
        /// 	RAISERROR('INFORMATION', 0, 0) WITH NOWAIT
        /// 
        /// 	RAISERROR('TRACE', 0, 1) WITH NOWAIT
        /// 	RAISERROR('DEBUG', 0, 2) WITH NOWAIT
        ///     RAISERROR('WARNING', 0, 3) WITH NOWAIT
        /// 	RAISERROR('ERROR', 11, 4) WITH NOWAIT
        /// 	RAISERROR('CRITICAL', 11, 5) WITH NOWAIT
        /// 
        /// </remarks>
        public static void LogSqlServer(this ILogger logger, SqlException e, string prefix = null)
        {
            var level = SqlServerLoggingUtils.GetLevel(e.Class, e.State);

            string fullPrefix;
            if (prefix == "")
                fullPrefix = "";
            else if (prefix != null)
                fullPrefix = $"{prefix}";
            else if (!string.IsNullOrEmpty(e.Procedure) && !e.Procedure.StartsWith("P_Log"))
                fullPrefix = $"[{e.Procedure}, line {e.LineNumber}] ";
            else
                fullPrefix = "";

            logger.Log(level, default(EventId), $"{fullPrefix}{e.Message}", null, MessageFormatter);
        }
        

        //------------------------------------------Helpers------------------------------------------

        private static string MessageFormatter(object state, Exception error)
        {
            return state.ToString();
        }
    }
}
