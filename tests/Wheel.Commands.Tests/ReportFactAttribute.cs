﻿using Xunit;

namespace Wheel.Commands.Tests
{
    /// <summary>
    /// A xunit fact skipped if settings directive "Tests:SkipReport" is enabled.
    /// </summary>
    public class ReportFactAttribute : FactAttribute
    {
        public ReportFactAttribute()
            : base()
        {
            Skip = TestsInit.SkipReport;
        }
    }
}
