﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Wheel
{
    /// <summary>
    /// Exception raised when the report email cannot be sent.
    /// </summary>
    public class EmailSenderException : MessageException
    {
        public EmailSenderException(string message, string to)
            : base(string.Format("Cannot send email to {0}: {1}", to, message))
        {

        }
    }
}
