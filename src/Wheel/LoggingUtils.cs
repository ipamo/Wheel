﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.IO;
using Microsoft.Extensions.Logging;

namespace Wheel
{
    /// <summary>
    /// Helper methods for the logging system.
    /// </summary>
    public static class LoggingUtils
    {
        public static string BootstrapColor(this LogLevel level)
        {
            switch (level)
            {
                case LogLevel.Critical:
                case LogLevel.Error:
                    return "danger";

                case LogLevel.Warning:
                    return "warning";

                case LogLevel.Information:
                    return "info";

                case LogLevel.Debug:
                case LogLevel.Trace:
                case LogLevel.None:
                default:
                    return "secondary";
            }
        }

        //------------------------------------------ SafeLog ------------------------------------------

        /// <summary>
        /// To be used when there is a chance that the given logger is not available (e.g. during termination).
        /// </summary>
        public static void SafeLog(ILogger logger, LogLevel level, Exception exception, string message, params object[] args)
        {
            if (logger != null)
            {
                logger.Log(level, default(EventId), FormatLogValues(message, args), exception, MessageFormatter);
            }
            else
            {
                // TODO : detect if console logger is disabled?
                lock (Console.Out)
                {
                    var color = Console.ForegroundColor;
                    try
                    {
                        if (level >= LogLevel.Error)
                            Console.ForegroundColor = ConsoleColor.Red;
                        else if (level >= LogLevel.Warning)
                            Console.ForegroundColor = ConsoleColor.Yellow;

                        if (exception != null)
                            Console.Error.WriteLine(exception.ToString());
                        Console.Error.WriteLine("[{0}] {1}", level.ToString().ToUpperInvariant(), FormatLogValues(message, args));
                    }
                    finally
                    {
                        Console.ForegroundColor = color;
                    }
                }
            }
        }

        /// <summary>
        /// To be used when there is a chance that the given logger is not available (e.g. during termination).
        /// </summary>
        public static void SafeLog(ILogger logger, LogLevel level, string message, params object[] args)
            => SafeLog(logger, level, null, message, args);

        //------------------------------------------ Log ------------------------------------------

        /// <summary>
        /// Writes a log message.
        /// </summary>
        /// <param name="logger">The logger to write to.</param>
        /// <param name="level">Severity level.</param>
        /// <param name="eventId">The event id associated with the log.</param>
        /// <param name="exception">The exception to log.</param>
        /// <param name="message">Format string of the log message.</param>
        /// <param name="args">An object array that contains zero or more objects to format.</param>
        /// <remarks>
        /// Voluntarely not an extension method, because versions of Microsoft.Extensions.Logging.Abstractions >= 2.1.0 add it.
        /// </remarks>
        public static void Log(ILogger logger, LogLevel level, EventId eventId, Exception exception, string message, params object[] args)
            => logger.Log(level, eventId, FormatLogValues(message, args), exception, MessageFormatter);

        /// <summary>
        /// Writes a log message.
        /// </summary>
        /// <param name="logger">The logger to write to.</param>
        /// <param name="level">Severity level.</param>
        /// <param name="eventId">The event id associated with the log.</param>
        /// <param name="message">Format string of the log message.</param>
        /// <param name="args">An object array that contains zero or more objects to format.</param>
        /// <remarks>
        /// Voluntarely not an extension method, because versions of Microsoft.Extensions.Logging.Abstractions >= 2.1.0 add it.
        /// </remarks>
        public static void Log(ILogger logger, LogLevel level, EventId eventId, string message, params object[] args)
            => logger.Log(level, eventId, FormatLogValues(message, args), null, MessageFormatter);

        /// <summary>
        /// Writes a log message.
        /// </summary>
        /// <param name="logger">The logger to write to.</param>
        /// <param name="level">Severity level.</param>
        /// <param name="exception">The exception to log.</param>
        /// <param name="message">Format string of the log message.</param>
        /// <param name="args">An object array that contains zero or more objects to format.</param>
        /// <remarks>
        /// Voluntarely not an extension method, because versions of Microsoft.Extensions.Logging.Abstractions >= 2.1.0 add it.
        /// </remarks>
        public static void Log(ILogger logger, LogLevel level, Exception exception, string message, params object[] args)
            => logger.Log(level, 0, FormatLogValues(message, args), exception, MessageFormatter);

        /// <summary>
        /// Writes a log message.
        /// </summary>
        /// <param name="logger">The logger to write to.</param>
        /// <param name="level">Severity level.</param>
        /// <param name="exception">The exception to log.</param>
        /// <remarks>
        /// Voluntarely not an extension method, because versions of Microsoft.Extensions.Logging.Abstractions >= 2.1.0 add it.
        /// </remarks>
        public static void Log(ILogger logger, LogLevel level, Exception exception)
            => logger.Log(level, 0, FormatLogValues(exception.Message), exception, MessageFormatter);

        /// <summary>
        /// Writes a log message.
        /// </summary>
        /// <param name="logger">The logger to write to.</param>
        /// <param name="level">Severity level.</param>
        /// <param name="message">Format string of the log message.</param>
        /// <param name="args">An object array that contains zero or more objects to format.</param>
        /// <remarks>
        /// Voluntarely not an extension method, because versions of Microsoft.Extensions.Logging.Abstractions >= 2.1.0 add it.
        /// </remarks>
        public static void Log(ILogger logger, LogLevel level, string message, params object[] args)
            => logger.Log(level, 0, FormatLogValues(message, args), null, MessageFormatter);


        //------------------------------------------ Helpers ------------------------------------------

        private static string MessageFormatter(object state, Exception error)
        {
            return state.ToString();
        }

        private static string FormatLogValues(string message, params object[] args)
        {
            // Was: return new FormatLogValues(message, args)
            // But made internal in Microsoft.Extensions.Logging 3.0.0 ?
            if (args != null && args.Length > 0)
                return string.Format(message, args);
            else
                return message;
        }
    }
}
