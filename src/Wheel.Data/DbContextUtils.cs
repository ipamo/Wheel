﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Wheel.Data
{
    public static class DbContextUtils
    {
        /// <summary>
        /// Affiche le statut du ChangeTracker de ce DbContext. Pour debugger uniquement.
        /// </summary>
        public static void DebugChangeTracker(this DbContext db, TextWriter writer = null)
        {
            if (writer == null)
                writer = Console.Out;

            foreach (var entity in db.ChangeTracker.Entries())
            {
                writer.WriteLine($"{entity.Metadata.Name} (id:{entity.Property("Id").CurrentValue}, {entity.Entity.ToString()}): {entity.State}");
                foreach (var md in entity.Metadata.GetProperties())
                {
                    var property = entity.Property(md.Name);
                    if (property.IsModified)
                        writer.WriteLine($"- {property.Metadata.Name}: {property.OriginalValue ?? "<null>"} => {property.CurrentValue ?? "<null>"}");
                }
            }
        }

        /// <summary>
        /// Attach an entity and mark it as modified. 
        /// </summary>
        /// <param name="context">The DbContext.</param>
        /// <param name="entity">The entity to attach.</param>
        /// <param name="properties">If given, only the given properties will be marked as modified.</param>
        public static void AttachForModification(this DbContext context, object entity, params string[] properties)
        {
            if (properties == null || properties.Length == 0)
            {
                context.Attach(entity).State = EntityState.Modified;
            }
            else
            {
                context.Attach(entity);
                var entry = context.Entry(entity);
                foreach (var propertyName in properties)
                {
                    entry.Property(propertyName).IsModified = true;
                }
            }
        }

        /// <summary>
        /// Attach an entity and mark it as modified. 
        /// </summary>
        /// <param name="context">The DbContext.</param>
        /// <param name="entity">The entity to attach.</param>
        /// <param name="properties">If given, only the given properties will be marked as modified.</param>
        public static void AttachForModification<TEntity>(this DbContext context, TEntity entity, params Func<TEntity, string>[] properties)
        {
            string[] propertyNames;
            if (properties == null || properties.Length == 0)
            {
                propertyNames = null;
            }
            else
            {
                propertyNames = properties.Select(x => x(entity)).ToArray();
            }

            AttachForModification(context, entity, propertyNames);
        }
    }
}
