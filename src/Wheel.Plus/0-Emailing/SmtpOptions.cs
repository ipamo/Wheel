﻿#if !NET20
namespace Wheel
{
    /// <summary>
    /// SMTP options used by <see cref="EmailSender"/>.
    /// </summary>
    public class SmtpOptions
    {
        public string Host { get; set; }
        public string User { get; set; }
        public string Password { get; set; }
        public int Port { get; set; }
        public bool Ssl { get; set; }
        public string From { get; set; }
        public string ReplyTo { get; set; }
    }
}
#endif
