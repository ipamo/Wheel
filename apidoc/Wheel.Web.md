---
uid: Wheel.Web
---

_Wheel.Web_ is the namespace for ASP.NET Core utility classes.
