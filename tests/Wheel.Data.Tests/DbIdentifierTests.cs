﻿using Wheel.Data;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;
using Xunit.Abstractions;
using Wheel.Testing;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace Wheel.Data.Tests
{
    [Collection(nameof(FixtureCollection))]
    public class DbIdentifierTests
    {
        public DbIdentifierTests(Fixture fx, ITestOutputHelper output)
        {

        }

        [Fact]
        public virtual void SameBeginEnd()
        {
            var o = new DbIdentifier(protectBegin: '"', protectEnd: '"', defaultSchema: "public");

            o.Parse("base.\"sche ma\".\"ta-ble\"");
            Assert.Equal("base", o.Catalog);
            Assert.Equal("sche ma", o.Schema);
            Assert.Equal("ta-ble", o.Name);

            try
            {
                o.Parse("\"essai");
                Assert.False(true);
            }
            catch
            {
                Assert.True(true);
            }
        }

        [Fact]
        public virtual void DistinctBeginEnd()
        {
            var o = new DbIdentifier(protectBegin: '[', protectEnd: ']', defaultSchema: "end");

            o.Parse("base.[sche ma].[ta-ble]");
            Assert.Equal("base", o.Catalog);
            Assert.Equal("sche ma", o.Schema);
            Assert.Equal("ta-ble", o.Name);

            try
            {
                o.Parse("[essai[");
                Assert.False(true);
            }
            catch
            {
                Assert.True(true);
            }
        }
    }
}
