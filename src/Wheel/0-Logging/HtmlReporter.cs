﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace Wheel
{
    /// <summary>
    /// An implementation of <see cref="IReporter"/> primarly used to emit a reporting HTML email
    /// when <c>--report</c> or <c>--report-to</c> options are enabled for a command.
    /// </summary>
    public class HtmlReporter : IReporter
    {
        private readonly StringBuilder _htmlContent = new StringBuilder();
        private readonly StringBuilder _cssContent = new StringBuilder();
        private readonly bool _noCss;

        public string SubjectPrefix { get; set; }

        public HtmlReporter(bool noCss = false)
        {
            Count = new Dictionary<LogLevel, int>();
            foreach (LogLevel l in Enum.GetValues(typeof(LogLevel)))
            {
                Count[l] = 0;
            }

            _noCss = noCss;
        }

        public void AddLine(string line)
        {
            _htmlContent.Append(HtmlEncode(line).Replace("\n", "<br/>"));
            _htmlContent.AppendLine("<br>");
        }

        public void AddHtml(string html)
        {
            _htmlContent.Append(html);
        }

        public void AddSection(string title, int level = 4)
        {
            _htmlContent.AppendLine($@"
<h{level}>{HtmlEncode(title).Replace("\n", "<br/>")}</h{level}>
");
        }

        public void AddCss(string css)
        {
            _cssContent.Append(css);
        }

        private string GetReportedLogs()
        {
            var logsBuilder = new StringBuilder();

            foreach (var item in _logReports)
            {
                if (item == null)
                    continue;

                var logLine = FormatLog(item.Value);

                string cssClass = null;
                if (!_noCss)
                {
                    if (item.Value.Level >= LogLevel.Error)
                    {
                        cssClass = "text-danger";
                    }
                    else if (item.Value.Level == LogLevel.Warning)
                    {
                        cssClass = "text-warning";
                    }
                }

                if (cssClass != null)
                    logsBuilder.Append("<span class=\"" + cssClass + "\">");
                logsBuilder.Append(HtmlEncode(logLine));
                if (cssClass != null)
                    logsBuilder.Append("</span>");
                logsBuilder.AppendLine();
            }

            return logsBuilder.ToString();
        }

        private static string FormatLog(LogReport report)
        {
            var sb = new StringBuilder();

            // Exception
            if (report.Exception != null)
                sb.AppendLine(report.Exception.ToString());

            // Time
            sb.Append(report.DateTime.ToString("HH:mm:ss"));
            sb.Append(" ");

            // Level
            sb.Append(report.Level.ToString().ToUpperInvariant().PadRight(8, ' '));
            sb.Append(" ");

            // Thread
            sb.Append($"[T:{report.ThreadId}] ");

            // Message
            if (!string.IsNullOrEmpty(report.Message))
                sb.Append(report.Message);

            return sb.ToString();
        }

        private string GetTimespan(DateTime from)
        {
            var ts = DateTime.Now - from;
            string timespan = null;
            if (ts.Days > 0)
                timespan = (timespan != null ? timespan + ", " : "") + $"{ts.Days} j";
            if (ts.Hours > 0 || timespan != null)
                timespan = (timespan != null ? timespan + ", " : "") + $"{ts.Hours} h";
            if (ts.Minutes > 0 || timespan != null)
                timespan = (timespan != null ? timespan + ", " : "") + $"{ts.Minutes} min";
            if (ts.Seconds > 0 || timespan != null)
                timespan = (timespan != null ? timespan + ", " : "") + $"{ts.Seconds} s";
            if (timespan == null)
                timespan = "1 s";
            return timespan;
        }

        private DateTime? _initTime;
        private string _environmentName;
        private string _applicationName;
        private string _applicationVersion;
        private string _commandName;
        private string[] _arguments;

        public void SetApplicationDetails(string environmentName = null, string applicationName = null, string applicationVersion = null, string commandName = null, string[] arguments = null, DateTime? initTime = null)
        {
            _environmentName = environmentName;
            _initTime = initTime;
            _applicationName = applicationName;
            _applicationVersion = applicationVersion;
            _commandName = commandName;
            _arguments = arguments;
        }
        
        public string GetSubject()
        {
            // Application and command
            string subject;
            if (SubjectPrefix != null && SubjectPrefix.Trim() != "")
                subject = SubjectPrefix.Trim();
            else if (_applicationName != null && _commandName != null)
                subject = $"{_applicationName} ({_commandName})";
            else if (_applicationName != null)
                subject = $"{_applicationName}";
            else if (_commandName != null)
                subject = $"{_commandName}";
            else
                subject = null;

            // Environment
            if (_environmentName != null && !_environmentName.Equals("Production", StringComparison.InvariantCultureIgnoreCase))
                subject = (subject != null ? subject + " " : "") + $"[{_environmentName}]";

            // Separator
            if (subject != null)
                subject += " : ";

            // Report message
            string reportMessage = null;
            if (CountErrors + CountCriticals > 0)
                reportMessage = (reportMessage != null ? reportMessage + ", " : "") + $"{(CountErrors + CountCriticals)} erreur{(CountErrors + CountCriticals > 1 ? "s" : "")}";
            if (CountWarnings > 0)
                reportMessage = (reportMessage != null ? reportMessage + ", " : "") + $"{CountWarnings} avertissement{(CountWarnings > 1 ? "s" : "")}";
            if (reportMessage == null)
                reportMessage = "OK";
            subject += reportMessage;

            return subject;
        }

        public string GetBodyContent()
        {
            var bodyBuilder = new StringBuilder();

            // -------------------------------------------------------
            // Header

            bodyBuilder.AppendLine(@"<html>
<head>
	<meta http-equiv=""Content-Type"" content=""text/html;charset=utf-8"" />
	<title>" + GetSubject() + @"</title>
	<style>
");
            bodyBuilder.Append(_cssContent);
            bodyBuilder.Append(@"
	</style>
</head>
<body>
");

            bodyBuilder.Append(_htmlContent);
            bodyBuilder.AppendLine();

            // -------------------------------------------------------
            // Bilan

            bodyBuilder.AppendLine($@"
    <h4>Bilan</h4>
    ");

            string problems = null;
            if (CountCriticals > 0 || CountErrors > 0)
                problems = (problems != null ? problems + ", " : "") + $"{(CountCriticals + CountErrors)} erreur{(CountCriticals + CountErrors > 1 ? "s" : "")}";
            if (CountWarnings > 0)
                problems = (problems != null ? problems + ", " : "") + $"{CountWarnings} avertissement{(CountWarnings > 1 ? "s" : "")}";
            if (problems == null)
                problems = "aucune erreur, aucun avertissement";

            string problemsHtml;
            if (CountCriticals > 0 || CountErrors > 0)
                problemsHtml = $@"<strong{(!_noCss ? @" class=""text-danger""" : "")}>{problems}</strong>";
            else if (CountWarnings > 0)
                problemsHtml = $@"<strong{(!_noCss ? @" class=""text-warning""" : "")}>{problems}</strong>";
            else
                problemsHtml = problems;
            
            bodyBuilder.Append($@"
    <ul>");

            if (_applicationName != null)
            {
                bodyBuilder.Append($@"
        <li>Application : <strong>{_applicationName}</strong>");
                if (_applicationVersion != null)
                    bodyBuilder.Append($", v{_applicationVersion}");
                bodyBuilder.AppendLine("</li>");
            }

            if (_commandName != null)
            {
                bodyBuilder.Append($@"
        <li>Commande : <strong>{_commandName}</strong></li>");
            }

            if (_arguments != null && _arguments.Length > 0)
            {
                bodyBuilder.Append($@"
        <li>Arguments : <code>{string.Join(" ", _arguments.Select(a => a.Contains(" ") ? $"\"{a}\"" : a).ToArray())}</code></li>");
            }

            if (_environmentName != null)
            {
                bodyBuilder.Append($@"
        <li>Environnement : <i>{_environmentName}</i></li>");
            }

            if (_initTime != null)
            {
                var timespanHtml = HtmlEncode(GetTimespan(_initTime.Value));

                bodyBuilder.AppendLine($@"
        <li>Durée : <strong>{timespanHtml}</strong> (du {_initTime.Value.ToString("dd/MM/yyyy HH:mm:ss")} au {DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")})</li>");
            }

            bodyBuilder.AppendLine($@"
        <li>Problèmes signalés : {problemsHtml}</li>
    </ul>
    ");

            // -------------------------------------------------------
            // Extrait des logs

            if (_logReports.Count > 0)
            {
                bodyBuilder.AppendLine($@"<h4>Extrait des logs</h4>

    <pre>{GetReportedLogs()}</pre>
    ");
            }

            // -------------------------------------------------------
            // Footer
            bodyBuilder.AppendLine($@"
</body>
</html>
");

            return bodyBuilder.ToString();
        }

        public bool HighImportance => CountProblems > 0;


        #region Log reporter

        public bool IsEnabled(LogLevel logLevel)
        {
            return logLevel >= LogLevel.Information;
        }

        public void Log<TState>(LogLevel logLevel, EventId eventId, TState state, Exception exception, Func<TState, Exception, string> formatter)
        {
            Count[logLevel]++;
            if (logLevel >= LogLevel.Warning)
            {
                var message = formatter(state, exception);
                var report = new LogReport(Thread.CurrentThread.ManagedThreadId, logLevel, eventId, message, exception);
                _logReports.Add(report);
            }
            else
            {
                if (_logReports.LastOrDefault() != null)
                    _logReports.Add(null);
            }
        }

        public IDisposable BeginScope<TState>(TState state)
        {
            return null;
        }

        private readonly List<LogReport?> _logReports = new List<LogReport?>();
        public Dictionary<LogLevel, int> Count { get; }
        public int CountCriticals => Count[LogLevel.Critical];
        public int CountErrors => Count[LogLevel.Error];
        public int CountWarnings => Count[LogLevel.Warning];
        public int CountProblems => CountCriticals + CountErrors + CountWarnings;

        private struct LogReport
        {
            public LogReport(int threadId, LogLevel logLevel, EventId eventId, string message, Exception exception)
            {
                ThreadId = threadId;
                DateTime = DateTime.Now;
                Level = logLevel;
                EventId = eventId;
                Message = message;
                Exception = exception;
            }

            public DateTime DateTime { get; }
            public int ThreadId { get; }
            public LogLevel Level { get; }
            public EventId EventId { get; }
            public string Message { get; }
            public Exception Exception { get; }
        }

        #endregion

#if NET20
        private string HtmlEncode(string html) => System.Web.HttpUtility.HtmlEncode(html);
#else
        private string HtmlEncode(string html) => System.Net.WebUtility.HtmlEncode(html);
#endif
    }
}
