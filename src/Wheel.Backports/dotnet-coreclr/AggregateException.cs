﻿using System;
using System.Collections.Generic;
using System.Text;

namespace System
{
    public class AggregateException : Exception
    {
        public IList<Exception> InnerExceptions { get; }

        public AggregateException(string message, IEnumerable<Exception> innerExceptions)
            : this(message, new List<Exception>(innerExceptions))
        {

        }

        public AggregateException(string message, IList<Exception> innerExceptions)
            : base(message, innerExceptions != null && innerExceptions.Count > 0 ? innerExceptions[0] : null)
        {
            InnerExceptions = innerExceptions;
        }

        /// <summary>Gets a message that describes the exception.</summary>
        public override string Message
        {
            get
            {
                if (InnerExceptions.Count == 0)
                {
                    return base.Message;
                }

                StringBuilder sb = new StringBuilder();
                sb.Append(base.Message);
                sb.Append(' ');
                for (int i = 0; i < InnerExceptions.Count; i++)
                {
                    sb.Append('(');
                    sb.Append(InnerExceptions[i].Message);
                    sb.Append(") ");
                }
                sb.Length -= 1;
                return sb.ToString();
            }
        }
    }
}

