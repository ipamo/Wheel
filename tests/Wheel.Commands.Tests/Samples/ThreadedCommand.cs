﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Wheel.Commands.Tests.Samples
{
    class ThreadedCommand
    {
        public static void ConfigureCommand(ICommandBuilder cmd, IServiceCollection services)
        {
            services.AddTransient<ThreadedCommand>();
            cmd.Action = () =>
            {
                var o = cmd.Services.GetRequiredService<ThreadedCommand>();
                o.Run();
            };
        }

        private static int _num = 0;
        private readonly ILoggerFactory _loggerFactory;
        private readonly ILogger _logger;
        private readonly IReporterManager _reporterManager;
        private readonly IReporter _reporter;

        public ThreadedCommand(
            ILoggerFactory loggerFactory,
            IReporterManager reporterManager)
        {
            _num++;
            _loggerFactory = loggerFactory;
            _logger = loggerFactory.CreateLogger<ThreadedCommand>();
            _reporterManager = reporterManager;
            _reporter = reporterManager.Reporter;
        }

        public void Run()
        {
            var task1 = Task.Factory.StartNew(() => Do(1, 2));
            var task2 = Task.Factory.StartNew(() => Do(3, 2));
            Task.WaitAll(task1, task2);
        }

        private void Do(int start, int length)
        {
            _reporterManager.Register(_reporter);

            var logger = _loggerFactory.CreateLogger("from-thread");
            for (int i = start; i < start + length; i++)
            {
                _logger.LogWarning($"Test threaded with initial logger {_num} {i}");
                _logger.LogWarning($"Test threaded with thread logger {_num} {i}");
            }
        }
    }
}
