Create a web application with commands
======================================

The following program runs an ASP.NET Core web application if no command-line argument is given, or commands if command-line arguments are given.

## Program class

```c#
class Program
{
	static int Main(string[] args)
	{
		if (args.Length == 0)
		{
			WebHost.CreateDefaultBuilder(args)
				.UseStartup<Startup>()
				.ConfigureAppConfiguration(ConfigurationUtils.AddWheelToWebHost)
				.ConfigureLogging(LoggingUtils.AddWheelToWebHost)
				.Build()
				.Run();
			return ReturnCode.OK;
		}
		else
		{
			return CommandApplication.CreateDefaultBuilder()
				.UseStartup<Startup>()
				.Build()
				.Run(args);
		}
	}
}
```

Methods `ConfigurationUtils.AddWheelToWebHost` and `LoggingUtils.AddWheelToWebHost` allow to configure the web host with the same defaults as for Wheel command-line applications:

- Use NLog logging system if file `nlog.config` exists in the application current directory. Files `nlog.{environment}.config`, `nlog.local.config` or `nlog.{environment}.local.config` are also accepted.
- Accept files `appsettings.local.json` and `appsettings.{environment}.local.json` as additional configuration providers. 

## Startup class

Example of a Startup class valid for both ASP.NET Core web application or Wheel command-line application:

```c#
class Startup
{
	private readonly IHostingEnvironment _env;
	private readonly IConfiguration _config;

	public Startup(IHostingEnvironment env, IConfiguration config)
	{
		_env = env;
		_config = config;
	}

	public void ConfigureServices(IServiceCollection services)
	{
		services.AddOptions();
		services.Configure<SmtpOptions>(_config.GetSection("Smtp"));
		services.AddSingleton<IEmailSender, EmailSender>();
		
		var connectionString = _config.GetConnectionString("Default");
		services.AddSingleton<IDbms, PostgreSqlDbms>();
		services.AddDbContext<ApplicationDbContext>(builder => builder.UseNpgsql(connectionString));

		services.AddIdentity<User, Role>()
			.AddEntityFrameworkStores<ApplicationDbContext>()
			.AddDefaultTokenProviders();

		if (_env.WebRootPath != null) // Are we running an ASP.NET Core web application?
		{
			services.AddMvc();
			services.AddAuthentication();
		}
	}

	public void ConfigureCommands(ICommandBuilder root)
	{
		// Configure the command-line application
		
		root.Command<HelloCommand>();
	}

	public void Configure(IApplicationBuilder app)
	{
		// Configure the ASP.NET Core web application
	
		if (_env.IsDevelopment())
		{
			app.UseDeveloperExceptionPage();
			app.UseDatabaseErrorPage();
		}
		else
		{
			app.UseExceptionHandler("/Home/Error");
		}

		app.UseStaticFiles();

		app.UseAuthentication();

		app.UseMvc(routes =>
		{
			routes.MapRoute(
				name: "default",
				template: "{controller=Default}/{action=Index}/{id?}");
		});
	}
}
```
