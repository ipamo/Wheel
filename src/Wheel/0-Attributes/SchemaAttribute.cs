﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;


namespace Wheel
{
    /// <summary>
    /// Assign schema name (keeping default table name).
    /// <para>
    /// <see cref="TableAttribute.Schema"/> or <see cref="ViewAttribute.Schema"/>  has higher priority if given.
    /// </para>
    /// </summary>
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = false)]
    public class SchemaAttribute : Attribute
    {
        public SchemaAttribute(string name)
        {
            Name = name;
        }

        public string Name { get; set; }
    }
}
