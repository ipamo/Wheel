﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Wheel.Ftp
{
    /// <summary>
    /// Specifies a TCP port range defining the lower and upper limits for
    /// data-channels.
    /// </summary>
    /// <remarks>
    /// The default is to let the operating system select
    /// the port number within the range 1024-5000.  If the range is set to
    /// anything other than the default then ports will be selected sequentially,
    /// increasing by one until the higher limit is reached and then wrapping around
    /// to the lower limit.
    /// </remarks>
    public class PortRange
    {
        /// <summary>
        /// Lowest port number permitted.  This is also the default value for 
        /// <see cref="LowPort"/>.
        /// </summary>
        internal const int LOW_PORT = 1024;

        /// <summary>
        /// Default value for <see cref="HighPort"/>.
        /// </summary>
        internal const int DEFAULT_HIGH_PORT = 5000;

        /// <summary>
        /// Highest port number permitted.
        /// </summary>
        private const int HIGH_PORT = 65535;

        /// <summary>
        /// Used to notify of changed properties.
        /// </summary>
        private PropertyChangedEventHandler propertyChangeHandler = null;

        /// <summary>
        /// Default Constructor.
        /// </summary>
        internal PortRange()
        {
            this.low = LOW_PORT;
            this.high = DEFAULT_HIGH_PORT;
        }

        /// <summary>
        /// Constructor setting the lower and higher limits of the range.
        /// </summary>
        /// <param name="low">Lower limit of the port-range.</param>
        /// <param name="high">Higher limit of the port-range.</param>
        internal PortRange(int low, int high)
        {
            if (low < LOW_PORT || high > HIGH_PORT)
                throw new ArgumentException("Ports must be in range [" + LOW_PORT + "," + HIGH_PORT + "]");
            if (low >= high)
                throw new ArgumentException("Low port (" + low + ") must be smaller than high port (" + high + ")");
            this.low = low;
            this.high = high;
        }

        /// <summary>
        /// Lowest port number in range.
        /// </summary>
        /// <remarks>
        /// The default value is 1024.  If it is left at this value and <see cref="HighPort"/>
        /// is left at 5000 then the OS will select the port.  If it is set to
        /// anything other than 1024 then ports will be selected sequentially,
        /// increasing by one until the higher limit is reached and then wrapping around
        /// to the lower limit.
        /// </remarks>
        [Description("Lowest port number in range.")]
        [RefreshProperties(RefreshProperties.All)]
        [DefaultValue(LOW_PORT)]
        public int LowPort
        {
            get
            {
                return low;
            }
            set
            {
                if (value > HIGH_PORT || value < LOW_PORT)
                    throw new ArgumentException("Ports must be in range [" + LOW_PORT + "," + HIGH_PORT + "]");
                if (LowPort != value)
                {
                    low = value;
                    if (propertyChangeHandler != null)
                        propertyChangeHandler(this, new PropertyChangedEventArgs("LowPort"));
                }
            }
        }

        /// <summary>
        /// Highest port number in range.
        /// </summary>
        /// <remarks>
        /// The default value is 5000.  If it is left at this value and <see cref="LowPort"/>
        /// is left at 1024 then the OS will select the port.  If it is set to
        /// anything other than 5000 then ports will be selected sequentially,
        /// increasing by one until the higher limit is reached and then wrapping around
        /// to the lower limit.
        /// </remarks>
        [Description("Highest port number in range.")]
        [RefreshProperties(RefreshProperties.All)]
        [DefaultValue(DEFAULT_HIGH_PORT)]
        public int HighPort
        {
            get
            {
                return high;
            }
            set
            {
                if (value > HIGH_PORT || value < LOW_PORT)
                    throw new ArgumentException("Ports must be in range [" + LOW_PORT + "," + HIGH_PORT + "]");
                if (HighPort != value)
                {
                    high = value;
                    if (propertyChangeHandler != null)
                        propertyChangeHandler(this, new PropertyChangedEventArgs("HighPort"));
                }
            }
        }

        /// <summary>
        /// Determines if the operating system should select the ports within the range 1024-5000.
        /// </summary>
        /// <remarks>
        /// If <c>UseOSAssignment</c> is set to <c>true</c> then the OS will select data-channel
        /// ports within the range 1024-5000.  Otherwise ports will be selected sequentially,
        /// increasing by one until the higher limit is reached and then wrapping around
        /// to the lower limit.  Setting this flag will cause <see cref="LowPort"/> and
        /// <see cref="HighPort"/> to be set to 1024 and 5000, respectively.
        /// </remarks>
        [Description("Determines if the operating system should select the ports within the range 1024-5000.")]
        [RefreshProperties(RefreshProperties.All)]
        [DefaultValue(true)]
        public bool UseOSAssignment
        {
            get
            {
                return low == LOW_PORT && high == DEFAULT_HIGH_PORT;
            }
            set
            {
                LowPort = LOW_PORT;
                HighPort = DEFAULT_HIGH_PORT;
            }
        }

        /// <summary>
        /// Validate the port range, and throw an exception if incorrect.
        /// </summary>
        internal void ValidateRange()
        {
            if (low >= high)
                throw new FTPException("Low port (" + low + ") must be smaller than high port (" + high + ")");
        }

        /// <summary>
        /// Called when a property is changed.
        /// </summary>
        internal PropertyChangedEventHandler PropertyChangeHandler
        {
            get { return propertyChangeHandler; }
            set { propertyChangeHandler = value; }
        }

        /// <summary>
        /// Returns a <see cref="String"/> that represents the current <see cref="Object"/>. 
        /// </summary>
        /// <returns>A String that represents the current Object. </returns>
		public override string ToString()
        {
            return string.Format("{0} -> {1}", low, high);
        }

        /// <summary>
        /// Low port number in range
        /// </summary>
        private int low;

        /// <summary>
        /// High port number in range
        /// </summary>
        private int high;
    }
}
