﻿using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApp.Data;
using Wheel;
using Wheel.Commands;

namespace WebApp
{
    public class DbContextFactory : IDesignTimeDbContextFactory<ApplicationDbContext>
    {
        public ApplicationDbContext CreateDbContext(string[] args)
        {
            var services = CommandApplication.CreateDefaultBuilder()
                .UseStartup<Startup>()
                .Build()
                .Services;
            
            return services.GetRequiredService<ApplicationDbContext>();
        }
    }
}
