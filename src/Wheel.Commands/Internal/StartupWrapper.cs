﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Wheel.Commands.Internal
{
    internal class StartupWrapper : ICommandStartup
    {
        private readonly WheelInfra _infra;
        private readonly IDictionary<Type, object> _infraAvailableParams;
        private readonly MethodInfo _configureServicesMethod;
        private readonly MethodInfo _configureCommandsMethod;
        private readonly object _startupInstance;

        public StartupWrapper(Type startupType, WheelInfra infra)
        {
            _infra = infra;
            _infraAvailableParams = infra.GetInfraServicesDictionary();
            _configureServicesMethod = GetConfigureServicesMethod(startupType);
            _configureCommandsMethod = GetConfigureCommandsMethod(startupType);

            if ((_configureServicesMethod != null && !_configureServicesMethod.IsStatic)
                || (_configureCommandsMethod != null && !_configureCommandsMethod.IsStatic))
                _startupInstance = ReflectionUtils.CreateInstance(startupType, _infraAvailableParams);
        }

        public bool HasStartupMethod => _configureServicesMethod != null || _configureCommandsMethod != null;

        public void ConfigureServices(IServiceCollection services)
        {
            if (_configureServicesMethod == null)
                return;

            var availableParams = new Dictionary<Type, object>(_infraAvailableParams);
            availableParams.Add(typeof(IServiceCollection), services);
            if (_infra.HasInfraServices)
                availableParams.Add(typeof(ILogger<>), new Func<Type, object>(t => _infra.InfraServices.GetRequiredService(typeof(ILogger<>).MakeGenericType(t))));

            var preparedParams = ReflectionUtils.PrepareParameters(_configureServicesMethod, availableParams);
            _configureServicesMethod.Invoke(!_configureServicesMethod.IsStatic ? _startupInstance : null, preparedParams);
        }

        public void ConfigureCommands(ICommandBuilder cmd)
        {
            if (_configureCommandsMethod == null)
                return;

            var availableParams = new Dictionary<Type, object>(_infraAvailableParams);
            availableParams.Add(typeof(IServiceCollection), _infra.ServiceCollection);
            if (_infra.HasInfraServices)
                availableParams.Add(typeof(ILogger<>), new Func<Type, object>(t => _infra.InfraServices.GetRequiredService(typeof(ILogger<>).MakeGenericType(t))));
            availableParams.Add(typeof(ICommandBuilder), cmd);
            
            var preparedParams = ReflectionUtils.PrepareParameters(_configureCommandsMethod, availableParams);
            _configureCommandsMethod.Invoke(!_configureCommandsMethod.IsStatic ? _startupInstance : null, preparedParams);
        }
        
        private static MethodInfo GetConfigureServicesMethod(Type startupType)
        {
            var methods = startupType
#if NET20
                .GetMethods()
#else
                .GetTypeInfo().DeclaredMethods
#endif
                .Where(m => m.Name == "ConfigureServices");

            if (!methods.Any())
                return null;

            if (methods.Count() > 1)
                throw new CommandDefinitionException(string.Format("Several ConfigureServices methods in {0}", startupType));

            var method = methods.First();

            if (!method.IsPublic)
                throw new CommandDefinitionException(string.Format("ConfigureServices method in {0} is not public", startupType));
            
            if (! method.GetParameters().Any(p => p.ParameterType == typeof(IServiceCollection)))
                throw new CommandDefinitionException(string.Format("ConfigureServices method in {0} does not have parameter of type IServiceCollection", startupType));

            if (method.ReturnType != typeof(void))
                throw new CommandDefinitionException(string.Format("ConfigureServices method in {0} must not return any object", startupType));

            return method;
        }

        private static MethodInfo GetConfigureCommandsMethod(Type startupType)
        {
            MethodInfo configureCommandsMethod = null;
            MethodInfo configureCommandMethod = null;

            var methods = startupType
#if NET20
                .GetMethods()
#else
                .GetTypeInfo().DeclaredMethods
#endif
                .Where(m => m.Name == "ConfigureCommands");
            if (methods.Count() > 1)
                throw new CommandDefinitionException(string.Format("Several ConfigureCommands methods in {0}", startupType));
            configureCommandsMethod = methods.FirstOrDefault();
            if (configureCommandsMethod != null)
            {
                var paramsOk = configureCommandsMethod.GetParameters().Any(p => p.ParameterType == typeof(ICommandBuilder));
                if (!paramsOk)
                    throw new CommandDefinitionException(string.Format("ConfigureCommands method in {0} does not have parameter of type ICommandBuilder", startupType));
            }

            methods = methods = startupType
#if NET20
                .GetMethods()
#else
                .GetTypeInfo().DeclaredMethods
#endif
                .Where(m => m.Name == "ConfigureCommand");
            if (methods.Any() && configureCommandsMethod != null)
                throw new CommandDefinitionException(string.Format("ConfigureCommands and ConfigureCommand method are both defined in {0}", startupType));
            if (methods.Count() > 1)
                throw new CommandDefinitionException(string.Format("Several ConfigureCommand methods in {0}", startupType));
            configureCommandMethod = methods.FirstOrDefault();
            if (configureCommandMethod != null)
            {
                var paramsOk = configureCommandMethod.GetParameters().Any(p => p.ParameterType == typeof(ICommandBuilder));
                if (!paramsOk)
                    throw new CommandDefinitionException(string.Format("ConfigureCommand method in {0} does not have parameter of type ICommandBuilder", startupType));
            }

            // Select which method we will use
            MethodInfo method;
            if (configureCommandsMethod != null)
                method = configureCommandsMethod;
            else if (configureCommandMethod != null)
                method = configureCommandMethod;
            else
                return null;

            // Check method
            if (!method.IsPublic)
                throw new CommandDefinitionException(string.Format("{0} method in {1} is not public", method.Name, startupType));
            
            if (!method.GetParameters().Any(p => p.ParameterType == typeof(ICommandBuilder)))
                throw new CommandDefinitionException(string.Format("{0} method in {1} does not have parameter of type ICommandBuilder", method.Name, startupType));

            if (method.ReturnType != typeof(void))
                throw new CommandDefinitionException(string.Format("{0} method in {1} must not return any object", method.Name, startupType));

            return method;
        }
    }
}
