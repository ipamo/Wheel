﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;
using Wheel.Data;

namespace Wheel.Data.Tests.DbContextIntegration
{
    public class Dbms : PostgreSqlDbms
    {
        public Dbms(IConfiguration config, ILoggerFactory loggerFactory)
            : base(config, loggerFactory)
        {

        }

        public override string InsertedAtProperty => "InsertedAt";
        public override string InsertedByIdProperty => "InsertedById";

        public override string DateOnlySqlType => "date";
        public override string DateOnlyNowSql => "(now() at time zone 'utc')";

        public override string DateTimeSqlType => "timestamp(0)";
        public override string DateTimeNowSql => "(now() at time zone 'utc')";
    }
}
