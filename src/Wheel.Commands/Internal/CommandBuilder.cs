// Adapted from .NET Foundation's CommandLineUtils.
// Copyright (c) .NET Foundation. All rights reserved.
// Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.

using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Reflection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.DependencyInjection;
using System.Diagnostics;
#if !NET20
using System.Threading.Tasks;
#endif

namespace Wheel.Commands.Internal
{
    internal sealed class CommandBuilder : ICommandBuilder
    {
        internal const string DEFAULT_COMMAND_NAME = "*";

        private readonly ILogger _logger;

        internal WheelInfra Infra { get; }
        internal CommandApplicationOptions ApplicationOptions { get; }

        internal List<CommandBuilder> Subcommands { get; }
        internal List<CommandOption> Options { get; }
        internal List<CommandArgument> Arguments { get; }
        internal List<string> RemainingArguments { get; }

        internal CommandOption OptionHelp { get; }
        internal CommandOption OptionVersion { get; }
        internal Func<string> VersionGetter { get; }

        internal CommandOption OptionReport { get; private set; }
        internal CommandOption OptionReportTo { get; private set; }
        internal CommandBuilder DefaultSubcommand { get; private set; }

        // Set during parsing
        internal string[] ParsedArguments { get; set; }
        internal bool ParsedReport { get; set; }
        internal string ParsedReportTo { get; set; }

        public CommandBuilder(WheelInfra infra, CommandApplicationOptions options)
            : this(infra,
                options,
                name: infra.ApplicationInformation.Name,
                parent: null,
                version: infra.ApplicationInformation.Version,
                description: infra.ApplicationInformation.Description)
        {

        }

        private CommandBuilder(WheelInfra infra, CommandApplicationOptions options, string name, CommandBuilder parent, string version = null, string description = null)
        {
            Infra = infra ?? throw new ArgumentNullException(nameof(infra));
            ApplicationOptions = options ?? throw new ArgumentNullException(nameof(options));
            _logger = Infra.LoggerFactory.CreateLogger<CommandBuilder>();

            Subcommands = new List<CommandBuilder>();
            Options = new List<CommandOption>();
            Arguments = new List<CommandArgument>();
            RemainingArguments = new List<string>();

            Name = name;
            Parent = parent;
                        
            OptionHelp = Option("-h|--help", "Show help information.", noValue: true);
            
            if (version != null)
            {
                OptionVersion = Option("-v|--version", "Show version information.", noValue: true);
                VersionGetter = () => version;
            }

            Description = description;
        }
        
        public ICommandBuilder Parent { get; private set; }
        public string Name { get; private set; }
        public string Description { get; set; }
        public string HelpText { get; set; }
        public bool ShowInHelp { get; set; } = true;

        private Func<int> _func;

        public Func<int> Func
        {
            internal get
            {
                return _func;
            }
            set
            {
                if (value == null)
                {
                    _func = null;
                }
                else
                {
                    _func = () =>
                    {
                        return value();
                    };
                }
            }
        }

        
        public Action Action
        {
            set
            {
                if (value == null)
                {
                    _func = null;
                }
                else
                {
                    _func = () =>
                    {
                        value();
                        return ReturnCode.OK;
                    };
                }
            }
        }



#if !NET20
        public Func<Task<int>> AsyncFunc
        {
            set
            {
                if (value == null)
                {
                    _func = null;
                }
                else
                {
                    _func = () =>
                    {
                        return value().Result;
                    };
                }
            }
        }
        
        public Func<Task> AsyncAction
        {
            set
            {
                if (value == null)
                {
                    _func = null;
                }
                else
                {
                    _func = () =>
                    {
                        value().Wait();
                        return ReturnCode.OK;
                    };
                }
            }
        }
#endif
        
        public IReporter Reporter => Infra.ReporterManager.Reporter;
        
        public ICommandBuilder Command(string name, Action<ICommandBuilder> action)
        {
            if (string.IsNullOrEmpty(name))
                throw new ArgumentException(nameof(name), "name must be explicitly set for commands defined with an action");

            var cmd = new CommandBuilder(Infra, ApplicationOptions, name, parent: this);
            Subcommands.Add(cmd);
            cmd.Define(action);
            return cmd;
        }

        public ICommandBuilder Command<T>(string name = null)
            where T : class
            => Command(typeof(T), name);

        public ICommandBuilder Command(Type t, string name = null)
        {
            var cmd = new CommandBuilder(Infra, ApplicationOptions, name, parent: this);
            Subcommands.Add(cmd);
            cmd.Define(t);
            return cmd;
        }

        private static readonly MethodInfo _commandGenericMethod = typeof(CommandBuilder).GetMethod("Command", new Type[] { typeof(string) });

        public ICommandBuilder AddCommands(Assembly assembly = null)
        {
            if (assembly == null)
                assembly = Infra.ApplicationInformation.Assembly;
            
            foreach (var commandType in GetAttributeCommandTypes(assembly))
            {
                var method = _commandGenericMethod.MakeGenericMethod(commandType);
                method.Invoke(this, new object[] { null });
            }

            return this;
        }
        
        public ICommandBuilder DefaultCommand(Action<ICommandBuilder> defineAction)
        {
            return DefaultCommand(DEFAULT_COMMAND_NAME, defineAction);
        }

        public ICommandBuilder DefaultCommand(string name, Action<ICommandBuilder> defineAction)
        {
            var cmd = Command(name, defineAction);
            DefaultSubcommand = (CommandBuilder)cmd;
            return cmd;
        }

        public ICommandBuilder DefaultCommand<T>(string name = null) where T : class
        {
            var cmd = Command<T>(name);
            DefaultSubcommand = (CommandBuilder)cmd;
            return cmd;
        }


        public CommandOption Option(string template, string description = null, bool noValue = false, bool multiple = false, bool required = false, bool inherited = false)
            => Option(template, _ => { }, description, noValue, required, inherited);

        public CommandOption Option(string template, Action<CommandOption> configuration, string description = null, bool noValue = false, bool multiple = false, bool required = false, bool inherited = false)
        {
            var option = new CommandOption(template) { Description = description, NoValue = noValue, Multiple = multiple, Required = required, Inherited = inherited };
            Options.Add(option);
            configuration(option);
            return option;
        }


        public CommandArgument Argument(string name, string description = null, bool multiple = false, bool required = false)
        {
            return Argument(name, _ => { }, description, multiple, required);
        }

        public CommandArgument Argument(string name, Action<CommandArgument> configuration, string description = null, bool multiple = false, bool required = false)
        {
            var lastArg = Arguments.LastOrDefault();
            if (lastArg != null && lastArg.Multiple)
            {
                var message = string.Format("The last argument '{0}' accepts multiple values. No more argument can be added.",
                    lastArg.Name);
                throw new InvalidOperationException(message);
            }

            var argument = new CommandArgument(name) { Description = description, Multiple = multiple, Required = required };
            Arguments.Add(argument);
            configuration(argument);
            return argument;
        }

        public bool IsDependencyInjectionEnabled => Infra.IsDependencyInjectionEnabled;
        
        public IServiceProvider Services
        {
            get
            {
                if (! Infra.HasApplicationServices)
                {
                    if (!Infra.IsDependencyInjectionEnabled)
                        throw new InvalidOperationException("Cannot use ICommandBuilder.Services: dependency injection is not enabled for this application");
                    else
                        throw new InvalidOperationException("ICommandBuilder.Services cannot be used during command definition");
                }

                return Infra.ApplicationServices;
            }
        }


        #region Define command from define action or target class

        private void Define(Action<ICommandBuilder> action)
        {
            action(this);

            AfterDefine();
        }
        
        internal void Define(Type t)
        {
            var attr = GetCommandAttribute(t);
            if (attr != null)
                AttributeDefine(t, attr);
            
            var usedStartup = StartupDefine(t);
            if (attr == null && !usedStartup)
                throw new CommandDefinitionException(string.Format("Cannot use type {0} as a command: no CommandAttribute, and no ConfigureServices and ConfigureCommands methods", t));

            if (string.IsNullOrEmpty(Name))
                Name = GetCommandName(t);

            AfterDefine();
        }
        
        private void AfterDefine()
        {
            if (Infra.IsDependencyInjectionEnabled && ApplicationOptions.ReportEmailEnabled != false)
            {
                OptionReport = Option("--report", "Enable report email.", noValue: true);
                OptionReportTo = Option("--report-to", "Send report email to the indicated recipient.");
            }
        }

        private void AttributeDefine(Type t, CommandAttribute attr)
        {
            if (! t
#if !NET20
                .GetTypeInfo()
#endif
                .IsAbstract)
                Infra.ServiceCollection.AddTransient(t);

            if (!string.IsNullOrEmpty(attr.Name))
                Name = attr.Name;
            if (!string.IsNullOrEmpty(attr.Description))
                Description = attr.Description;
            if (!string.IsNullOrEmpty(attr.HelpText))
                HelpText = attr.HelpText;

            var wrapper = new CommandTypeWrapper(t, this, Infra);
            wrapper.ConfigureOptionsArguments();
            Func = wrapper.Run;
        }

        private bool StartupDefine(Type t)
        {
            ICommandStartup startup;
            if (typeof(ICommandStartup).IsAssignableFrom(t))
            {
                startup = (ICommandStartup)ReflectionUtils.CreateInstance(t, Infra.GetInfraServicesDictionary());
            }
            else
            {
                startup = new StartupWrapper(t, Infra);
                if (!((StartupWrapper)startup).HasStartupMethod)
                    return false;
            }

            startup.ConfigureServices(Infra.ServiceCollection);
            startup.ConfigureCommands(this);
            return true;
        }

        #endregion


        #region Helpers
        
        /// <summary>
        /// List types with the <see cref="CommandAttribute"/> in the given assembly.
        /// </summary>
        private static IEnumerable<Type> GetAttributeCommandTypes(Assembly assembly)
        {
            return assembly
#if NET20
                    .GetTypes()
#else
                    .DefinedTypes
#endif
                .Where(t => GetCommandAttribute(t) != null);
        }

        private static CommandAttribute GetCommandAttribute(Type type)
        {
#if NET20
            return (CommandAttribute)type
                    .GetCustomAttributes(inherit: false)
                    .FirstOrDefault(a => a.GetType() == typeof(CommandAttribute));
#else
            return type.GetCustomAttribute<CommandAttribute>();
#endif
        }

        private static string GetCommandName(Type t)
        {
            var name = t.Name;
            if (name.EndsWith("Command"))
                name = name.Substring(0, name.Length - 7);
            else if (name.EndsWith("Commands"))
                name = name.Substring(0, name.Length - 8);
            name = name.ToLowerSnake(separator: '-');
            
            if (name.Length == 0)
                throw new CommandDefinitionException(string.Format("Class name {0} cannot be used as command name", t));

            return name;
        }

        #endregion
    }
}
