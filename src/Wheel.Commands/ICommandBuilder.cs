﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Extensions.DependencyInjection;
using System.Reflection;
#if !NET20
using System.Threading.Tasks;
#endif

namespace Wheel.Commands
{
    /// <summary>
    /// Build a command-line application.
    /// </summary>
    public interface ICommandBuilder
    {
        /// <summary>
        /// Get the parent command.
        /// <para>
        /// Set in one of the <c>Command</c> or <c>DefaultCommand</c> methods.
        /// </para>
        /// </summary>
        ICommandBuilder Parent { get; }

        /// <summary>
        /// Get the name for the command.
        /// <para>
        /// Set in one of the <c>Command</c> or <c>DefaultCommand</c> methods.
        /// </para>
        /// </summary>
        string Name { get; }

        /// <summary>
        /// Short description of the command. Displayed in the list of commands in the parent's help text.
        /// </summary>
        string Description { get; set; }

        /// <summary>
        /// Detailed description of the behaviour of the command. Displayed under <see cref="Description"/> in the command's help text.
        /// <para>
        /// No need to add a leading or trailing new line.
        /// </para>
        /// </summary>
        string HelpText { get; set; }

        /// <summary>
        /// When disabled, the command does not appears in the list of sub-commands of the parent command's help text. Enabled by default.
        /// </summary>
        bool ShowInHelp { get; set; }
        
        /// <summary>
        /// Define the function executed when the command is called.
        /// </summary>
        Func<int> Func { set; }

        /// <summary>
        /// Define the action executed when the command is called.
        /// <para>
        /// This is a shortcut for <see cref="Func"/>, where the return code is always <see cref="ReturnCode.OK"/> (when no exception is raised).
        /// </para>
        /// </summary>
        Action Action { set; }

#if !NET20
        /// <summary>
        /// Define the async function executed when the command is called.
        /// </summary>
        Func<Task<int>> AsyncFunc { set; }

        /// <summary>
        /// Define the async action executed when the command is called.
        /// <para>
        /// This is a shortcut for <see cref="AsyncFunc"/>, where the return code is always <see cref="ReturnCode.OK"/> (when no exception is raised).
        /// </para>
        /// </summary>
        Func<Task> AsyncAction { set; }
#endif

        /// <summary>
        /// Register a command using a target type.
        /// </summary>
        /// <param name="name">Name of the command. If not given, it will be deducted from the name of the <typeparamref name="T"/> class.</param>
        ICommandBuilder Command<T>(string name = null)
            where T : class;

        /// <summary>
        /// Register a command using a target type.
        /// </summary>
        /// <param name="name">Name of the command. If not given, it will be deducted from the name of the <paramref name="t"/> class.</param>
        ICommandBuilder Command(Type t, string name = null);

        /// <summary>
        /// Register a command using a configure action.
        /// </summary>
        /// <param name="name">Name of the command.</param>
        /// <param name="action">Configure action.</param>
        ICommandBuilder Command(string name, Action<ICommandBuilder> action);

        /// <summary>
        /// Register all types with the <see cref="CommandAttribute"/>. 
        /// </summary>
        /// <param name="assembly">Assembly (if null, the application assembly is used).</param>
        ICommandBuilder AddCommands(Assembly assembly = null);

        /// <summary>
        /// Register the default command using a target type.
        /// </summary>
        /// <param name="name">Name of the command. If not given, it will be deducted from the name of the <typeparamref name="T"/> class.</param>
        ICommandBuilder DefaultCommand<T>(string name = null)
            where T : class;

        /// <summary>
        /// Register the default command using a configure action and assign it a name.
        /// </summary>
        /// <param name="name">Name of the command.</param>
        /// <param name="action">Configure action.</param>
        ICommandBuilder DefaultCommand(string name, Action<ICommandBuilder> action);

        /// <summary>
        /// Register the default command using a configure action without assigning it a name.
        /// </summary>
        /// <param name="action">Configure action.</param>
        ICommandBuilder DefaultCommand(Action<ICommandBuilder> action);

        // TODO : set default command?

        /// <summary>
        /// Add an option for the command.
        /// </summary>
        /// <param name="template">Name(s) of the option, for example: <c>-o|--option</c>, or <c>&lt;option&gt;</c>.</param>
        /// <param name="description">Short description of the option displayed in help text. For example: <c>An option.</c>.</param>
        /// <param name="noValue">Indicate that the option does not expect any value. Use <see cref="CommandOption.IsEnabled"/> to know if the option was provided at least once, or <see cref="CommandOption.Count"/> to know how many times the option was provided.</param>
        /// <param name="multiple">Indicate whether the option can have multiple values.</param>
        /// <param name="required">Indicate whether the option is required. If it is required and not given, a <see cref="CommandParserException"/> will be raised when using <see cref="CommandOption.Value"/> property.</param>
        /// <param name="inherited">Indicate that the option is defined for the current command and all sub commands.</param>
        CommandOption Option(string template, string description = null, bool noValue = false, bool multiple = false, bool required = false, bool inherited = false);

        /// <summary>
        /// Add an option for the command.
        /// </summary>
        /// <param name="template">Name(s) of the option, for example: <c>-o|--option</c>, or <c>&lt;option&gt;</c>.</param>
        /// <param name="configuration">Action to further configure the option being defined.</param>
        /// <param name="description">Short description of the option displayed in help text. For example: <c>An option.</c>.</param>
        /// <param name="noValue">Indicate that the option does not expect any value. Use <see cref="CommandOption.IsEnabled"/> to know if the option was provided at least once, or <see cref="CommandOption.Count"/> to know how many times the option was provided.</param>
        /// <param name="multiple">Indicate whether the option can have multiple values.</param>
        /// <param name="required">Indicate whether the option is required. If it is required and not given, a <see cref="CommandParserException"/> will be raised when using <see cref="CommandOption.Value"/> property.</param>
        /// <param name="inherited">Indicate that the option is defined for the current command and all sub commands.</param>
        CommandOption Option(string template, Action<CommandOption> configuration, string description = null, bool noValue = false, bool multiple = false, bool required = false, bool inherited = false);

        /// <summary>
        /// Add an argument or a list of arguments for the command.
        /// </summary>
        /// <param name="name">Name of the argument displayed in help text. For example: <c>[arg]</c>.</param>
        /// <param name="description">Short description of the argument displayed in help text. For example: <c>An argument.</c>.</param>
        /// <param name="multiple">Indicate whether the argument can have multiple values.</param>
        /// <param name="required">Indicate whether the argument is required. If it is required and not given, a <see cref="CommandParserException"/> will be raised when using <see cref="CommandArgument.Value"/> property.</param>
        CommandArgument Argument(string name, string description = null, bool multiple = false, bool required = false);

        /// <summary>
        /// Add an argument or a list of arguments for the command.
        /// </summary>
        /// <param name="name">Name of the argument displayed in help text. For example: <c>[arg]</c>.</param>
        /// <param name="configuration">Action to further configure the argument being defined.</param>
        /// <param name="description">Short description of the argument displayed in help text. For example: <c>An argument.</c>.</param>
        /// <param name="multiple">Indicate whether the argument can have multiple values.</param>
        /// <param name="required">Indicate whether the argument is required. If it is required and not given, a <see cref="CommandParserException"/> will be raised when using <see cref="CommandArgument.Value"/> property.</param>
        CommandArgument Argument(string name, Action<CommandArgument> configuration, string description = null, bool multiple = false, bool required = false);

        /// <summary>
        /// Indicate whether <see cref="Services"/> can be used for this application.
        /// </summary>
        bool IsDependencyInjectionEnabled { get; }
        
        /// <summary>
        /// The service provider, intended to be used only to access the dependency injection container from the assigned <see cref="Action"/> or <see cref="Func"/>.
        /// </summary>
        IServiceProvider Services { get; }
        
        /// <summary>
        /// Get the log reporter associated to this command. Only available during command execution.
        /// </summary>
        IReporter Reporter { get; }
    }
}
