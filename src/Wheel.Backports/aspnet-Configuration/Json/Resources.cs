﻿// Copyright (c) .NET Foundation. All rights reserved.
// Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.

using Newtonsoft.Json;

namespace Microsoft.Extensions.Configuration.Json
{
    internal static class Resources
    {
        public static string Error_InvalidFilePath
            => "File path must be a non-empty string.";

        public static string FormatError_UnsupportedJSONToken(JsonToken tokenType, string path, int lineNumber, int linePosition)
            => "Unsupported JSON token '{0}' was found. Path '{1}', line {2} position {3}.";

        public static string FormatError_KeyIsDuplicated(string key)
            => "A duplicate key '{0}' was found.";

        public static string FormatError_JSONParseError(int lineNumber, string errorLine)
            => "Could not parse the JSON file. Error on line number '{0}': '{1}'.";
    }
}
