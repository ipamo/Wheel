public static class HostExtensions
{
	/// <summary>
	/// Ensure that "Default" connection string contains the given word.
	/// <para>
	/// Usefull for tests (ensure it contains "test").
	/// </para>
	/// </summary>
	public static void EnsureConnectionStringContains(this IHost host, string contains, string connectionStringKey = "Default")
	{
		// Ensure the connection string is configured with word "test"
		var connectionString = host.Services.GetRequiredService<IConfiguration>()
			.GetConnectionString(connectionStringKey);

		if (string.IsNullOrEmpty(connectionString))
			throw new InvalidOperationException($"Connection string \"{connectionStringKey}\" was not provided");

		if (!connectionString.Contains(contains, StringComparison.InvariantCultureIgnoreCase))
			throw new InvalidOperationException($"Connection string \"{connectionStringKey}\" does not contain \"{contains}\"");
	}
}
