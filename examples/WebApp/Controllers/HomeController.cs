﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApp.Models;
using Wheel.Data;

namespace WebApp.Controllers
{
    public class HomeController : Controller
    {
        private readonly UserManager<User> _userManager;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IDbms _dbms;

        public HomeController(UserManager<User> userManager, IHttpContextAccessor httpContextAccessor, IDbms dbms)
        {
            _dbms = dbms;
            _userManager = userManager;
            _httpContextAccessor = httpContextAccessor;
        }

        public string Index()
        {
            return "Welcome on the controller. See also /Home/Dict and /Home/Users.";
        }

        public string Dict()
        {
            _dbms.SetDictValue("test", "success");
            var value = _dbms.GetDictValue("test");
            return $"Dict test value: {value}";
        }

        public IActionResult Users()
        {
            var list = _userManager.Users.ToList();
            return View(list);
        }
    }
}
