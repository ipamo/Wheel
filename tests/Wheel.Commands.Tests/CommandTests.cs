﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Wheel.Testing;
using Xunit;
using Xunit.Abstractions;
using Wheel.Commands.Tests.Samples;

namespace Wheel.Commands.Tests
{
    public class CommandTests : IDisposable
    {
        private readonly ITestOutputHelper _output;
        private readonly TestLogger _logger;

        public CommandTests(ITestOutputHelper output)
        {
            _output = output;
            _logger = new TestLogger(output);
        }

        public void Dispose()
        {
            _logger.Clear();
        }

        [Fact]
        public void HelpApplication()
        {
            string stderr;
            using (var capture = new ConsoleCapture(_output))
            {
                var app = BuildCommandApplication();
                app.Run("-h");
                stderr = capture.GetError()?.Trim();
            }

            Assert.StartsWith("Wheel.Commands.Tests", stderr);
            Assert.EndsWith("Unit tests for Wheel.Commands.", stderr);
            Assert.Contains("Usage: Wheel.Commands.Tests [options] [command] [[--] <arg>...]", stderr);
            Assert.Contains("add", stderr);
            Assert.Contains("Add or multiply two integers", stderr);
            Assert.Contains("direct", stderr);
            Assert.Contains("*static", stderr);
            Assert.Contains("Apply an operation to operand '3'", stderr);
            Assert.Contains("threaded", stderr);
        }

        [Fact]
        public void HelpTypeCommand()
        {
            string stderr;
            using (var capture = new ConsoleCapture(_output))
            {
                var app = BuildCommandApplication();
                app.Run("add", "-h");
                stderr = capture.GetError()?.Trim();
            }

            Assert.StartsWith("Usage: Wheel.Commands.Tests add", stderr);
            Assert.EndsWith("Add or multiply two integers", stderr);
            Assert.Contains("num1", stderr);
            Assert.Contains("num2", stderr);
            Assert.Contains("nums", stderr);
            Assert.Contains("-m|--multiply  If set, multiply instead of add", stderr);
        }

        [Fact]
        public void DirectCommand()
        {
            var app = BuildCommandApplication();
            app.Run("direct", "-n", "1");
            app.Run("direct", "-n", "2");

            var messages = _logger.Messages;
            Assert.Contains("[ERROR] Test direct 1", messages);
            Assert.Contains("[ERROR] Test direct 2", messages);
            Assert.Contains("[ERROR] Command direct emitted 1 error", messages); // x2

            Assert.Equal(4, _logger.CountProblems);
            Assert.Equal(4, _logger.CountErrors);
        }

        [Fact]
        public void ThreadedCommand()
        {
            var app = BuildCommandApplication();
            app.Run("threaded");
            app.Run("threaded");

            var messages = _logger.Messages;
            Assert.Contains("[WARNING] Test threaded with initial logger", messages);
            Assert.Contains("[WARNING] Test threaded with thread logger", messages);
            Assert.Contains("[WARNING] Command threaded emitted 8 warnings", messages); // x2

            Assert.Equal(18, _logger.CountProblems);
            Assert.Equal(18, _logger.CountWarnings);
        }

        [Fact]
        public void DefaultCommand()
        {
            var result = BuildCommandApplication().Run();

            var messages = _logger.Messages;
            Assert.DoesNotContain("[WARNING]", messages);
            Assert.DoesNotContain("[ERROR]", messages);

            Assert.Equal(3, result);
        }

        [Fact]
        public void DefaultCommandWithArgOrOpt()
        {
            // If we want to use the default command with arguments, we have to pass the command name.
            var result = BuildCommandApplication().Run("static", "-m", "3");

            var messages = _logger.Messages;
            Assert.DoesNotContain("[WARNING]", messages);
            Assert.DoesNotContain("[ERROR]", messages);

            Assert.Equal(9, result);
        }

        [Fact]
        public void TypeCommand_Required()
        {
            using (var capture = new ConsoleCapture(_output))
            {
                var app = BuildCommandApplication();
                var result = app.Run("add", "-m", "1");

                var messages = _logger.Messages;
                Assert.Contains("[ERROR] Argument 'num2' is required", messages);
                Assert.Equal(2, _logger.CountProblems);
            }
        }

        [Fact]
        public void TypeCommand_EmptyMultiArg()
        {
            using (var capture = new ConsoleCapture(_output))
            {
                var app = BuildCommandApplication();
                var result = app.Run("add", "-m", "2", "3");

                Assert.Equal(0, _logger.CountProblems);
                Assert.Equal(6, result);
            }
        }

        [Fact]
        public void TypeCommand_WithMultiArg()
        {
            using (var capture = new ConsoleCapture(_output))
            {
                var app = BuildCommandApplication();
                var result = app.Run("add", "2", "3", "4", "5");

                Assert.Equal(0, _logger.CountProblems);
                Assert.Equal(14, result);
            }
        }


        [Fact]
        public void TypeCommand_WithStringArray()
        {
            using (var capture = new ConsoleCapture(_output))
            {
                var app = BuildCommandApplication();
                var result = app.Run("string-array", "a", "b", "c");

                Assert.Contains("3 arguments: a, b, c", _logger.Messages);
                Assert.Equal(0, _logger.CountProblems);
            }
        }


        [Fact]
        public void TypeCommand_EmptyStringArray()
        {
            using (var capture = new ConsoleCapture(_output))
            {
                var app = BuildCommandApplication();
                var result = app.Run("string-array");

                Assert.Contains("0 arguments", _logger.Messages);
                Assert.Equal(0, _logger.CountProblems);
            }
        }

        private ICommandApplication BuildCommandApplication()
        {
            return CommandApplication.CreateDefaultBuilder()
                .UseStartup<Startup>()
                .ConfigureLogging(builder =>
                {
                    builder.SetMinimumLevel(LogLevel.Trace);
                    builder.AddProvider(new SingleLoggerProvider(_logger));
                })
                .Build();
        }
    }
}
