﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Wheel
{
    /// <summary>
    /// Produce (roadmap: and parse) CSV files.
    /// </summary>
    /// <remarks>
    /// Uses FR Excel CSV format.
    /// </remarks>
    public static class CsvUtils
    {
        private const char _valueSeparator = ';';
        // \r\n est interprété par SSIS comme un nouvel enregistrement, même à l'intérieur de l'échappement. Il est donc important de distinguer \n (nouvelle ligne dans une valeur) de \r\n (nouvelle ligne correspondant à un nouvel enregistrement)
        private readonly static string _recordSeparator = "\r\n";
        private const char _valueEscaper = '\"';
        private const char _valueNewline = '\n';

        private readonly static NumberFormatInfo _numberFormatInfo = new NumberFormatInfo
        {
            NumberDecimalSeparator = ".",
            NumberGroupSeparator = "",
        };

        /// <summary>
        /// Format a value so that it can be used as a CSV value.
        /// No escaping.
        /// </summary>
        /// <param name="value">The value to format.</param>
        /// <param name="normalize">If true, decimal numbers are standardized (decimal separator is a dot, no thousands separator), this is necessary for SSIS.
        /// If false (default), decimal numbers are localized, this is necessary for Excel.</param>
        private static string ToString(object value, bool normalize = false)
        {
            if (value == null)
                return null;

            var type = value.GetType();
            if (Nullable.GetUnderlyingType(type) != null)
                type = Nullable.GetUnderlyingType(type);

            if (type == typeof(DateTime))
            {
                var dt = (DateTime)value;
                if (dt.Hour == 0 && dt.Minute == 0 && dt.Second == 0)
                    return dt.ToString("yyyy-MM-dd");
                else
                    return dt.ToString("yyyy-MM-dd HH:mm:ss");
            }

            if (normalize)
            {
                if (type == typeof(decimal))
                    return ((decimal)value).ToString(_numberFormatInfo);

                if (type == typeof(double))
                    return ((double)value).ToString(_numberFormatInfo);

                if (type == typeof(float))
                    return ((float)value).ToString(_numberFormatInfo);
            }

            return value.ToString();
        }

        /// <summary>
        /// Format a value (and escape it if necessary) so that it can be used as a CSV value.
        /// </summary>
        /// <param name="value">The value to format.</param>
        /// <param name="normalize">If true, decimal numbers are standardized (decimal separator is a dot, no thousands separator), this is necessary for SSIS.
        /// If false (default), decimal numbers are localized, this is necessary for Excel.</param>
        public static string ToCsvValue(object value, bool normalize = true)
        {
            if (value == null)
                return null;

            var str = ToString(value, normalize);

            // Standardize newlines and escape
            bool needEscape = false;
            var sb = new StringBuilder(str.Length);
            for (int i = 0; i < str.Length; i++)
            {
                var c = str[i];
                switch(c)
                {
                    case _valueEscaper:
                        sb.Append(_valueEscaper);
                        sb.Append(_valueEscaper); // double (escape)
                        needEscape = true;
                        break;

                    case _valueSeparator:
                    case '\n':
                        sb.Append('\n');
                        needEscape = true;
                        break;

                    case '\r':
                        sb.Append('\n');
                        if (i < str.Length - 1 && str[i+1] == '\n')
                            i++; // this is windows-style newline (\r\n), we ignore the following \n
                        needEscape = true;
                        break;

                    default:
                        sb.Append(c);
                        break;
                }
            }

            if (needEscape)
            {
                sb.Insert(0, _valueEscaper);
                sb.Append(_valueEscaper);
            }

            return sb.ToString();
        }

        /// <summary>
        /// Add a CSV record (format values and add CSV separator ';') to the given text writer.
        /// </summary>
        /// <param name="writer">The writer (should be an ISO-8859-1 file in order to be used easily in Excel and SSIS).</param>
        /// <param name="normalize">If true, decimal numbers are standardized (decimal separator is a dot, no thousands separator), this is necessary for SSIS.
        /// If false (default), decimal numbers are localized, this is necessary for Excel.</param>
		public static void AddCsvRecord(TextWriter writer, IEnumerable<object> values, bool normalize = true, string rawEnd = null)
		    => writer.Write(ToCsvRecord(values, normalize, rawEnd) + _recordSeparator);

        /// <summary>
        /// Add a CSV record (format values and add CSV separator ';') to the given text writer.
        /// </summary>
        /// <param name="writer">The writer (should be an ISO-8859-1 file in order to be used easily in Excel and SSIS).</param>
        public static void AddCsvRecord_Array(TextWriter writer, params object[] values)
			=> AddCsvRecord(writer, values, normalize: true);

        /// <summary>
        /// Add a CSV record (format values and add CSV separator ';') to the given text writer. If <paramref name="values"/> is a dictionary, keys from <paramref name="headerColumns"/> are used.
        /// Otherwise, properties from <paramref name="values"/> whose name is in <paramref name="headerColumns"/> are used.
        /// </summary>
        /// <param name="writer">The writer (should be an ISO-8859-1 file in order to be used easily in Excel and SSIS).</param>
        /// <param name="normalize">If true, decimal numbers are standardized (decimal separator is a dot, no thousands separator), this is necessary for SSIS.
        /// If false (default), decimal numbers are localized, this is necessary for Excel.</param>
		public static void AddCsvRecord(TextWriter writer, object values, IEnumerable<string> headerColumns, bool normalize = true, string rawEnd = null)
            => writer.Write(ToCsvRecord(values, headerColumns, normalize, rawEnd) + _recordSeparator);

        /// <summary>
        /// Create a CSV record (format values and add CSV separator ';').
        /// </summary>
        /// <param name="normalize">If true, decimal numbers are standardized (decimal separator is a dot, no thousands separator), this is necessary for SSIS.
        /// If false (default), decimal numbers are localized, this is necessary for Excel.</param>
        public static string ToCsvRecord(IEnumerable<object> values, bool normalize = true, string rawEnd = null)
        {
            bool first = true;
            var str = "";
            foreach (var col in values)
            {
                if (first)
                    first = false;
                else
                    str += _valueSeparator;
                str += ToCsvValue(col, normalize);
            }
			
			if (rawEnd != null)
			{
				if (!first && (rawEnd.Length == 0 || rawEnd[0] != _valueSeparator))
					str += _valueSeparator;
				str += rawEnd;
			}
            return str;
        }

        /// <summary>
        /// Create a CSV record. If <paramref name="values"/> is a dictionary, keys from <paramref name="headerColumns"/> are used.
        /// Otherwise, properties from <paramref name="values"/> whose name is in <paramref name="headerColumns"/> are used.
        /// </summary>
        /// <param name="normalize">If true, decimal numbers are standardized (decimal separator is a dot, no thousands separator), this is necessary for SSIS.
        /// If false (default), decimal numbers are localized, this is necessary for Excel.</param>
        public static string ToCsvRecord(object values, IEnumerable<string> headerColumns, bool normalize = true, string rawEnd = null)
        {
            bool first = true;
            var str = "";
            foreach (var col in ToRecord(values, headerColumns))
            {
                if (first)
                    first = false;
                else
                    str += _valueSeparator;
                str += ToCsvValue(col, normalize);
            }
			
			if (rawEnd != null)
			{
				if (!first && (rawEnd.Length == 0 || rawEnd[0] != _valueSeparator))
					str += _valueSeparator;
				str += rawEnd;
			}
            return str;
        }

        /// <summary>
        /// Create a record for CSV. If <paramref name="values"/> is a dictionary, keys from <paramref name="headerColumns"/> are used.
        /// Otherwise, properties from <paramref name="values"/> whose name is in <paramref name="headerColumns"/> are used.
        /// </summary>
        public static object[] ToRecord(object values, IEnumerable<string> headerColumns)
        {
            if (values == null)
                return ToRecord_Object(values, headerColumns);

            var type = values.GetType();

            if (typeof(IDictionary<string, object>).IsAssignableFrom(type))
                return ToRecord_Dictionary((IDictionary<string, object>)values, headerColumns);

            if (typeof(IDictionary<string, string>).IsAssignableFrom(type))
            {
                var stringDict = (IDictionary<string, string>)values;
                var objectDict = new Dictionary<string, object>(stringDict.Count);
                foreach (var e in stringDict)
                    objectDict.Add(e.Key, e.Value);
                return ToRecord_Dictionary(objectDict, headerColumns);
            }

            return ToRecord_Object(values, headerColumns);
        }

        private static object[] ToRecord_Dictionary(IDictionary<string, object> data, IEnumerable<string> headerColumns)
        {
            var array = new object[headerColumns.Count()];
            if (data != null)
            {
                int i = 0;
                foreach (var headerName in headerColumns)
                {
                    if (data.ContainsKey(headerName))
                        array[i++] = data[headerName];
                    else
                        i++;
                }
            }
            return array;
        }

        private static object[] ToRecord_Object(object values, IEnumerable<string> headerColumns)
        {
            var array = new object[headerColumns.Count()];
            if (values != null)
            {
                var type = values.GetType();
                int i = 0;
                foreach (var headerName in headerColumns)
                {
                    var propertyName = headerName;
                    var property = type.GetProperty(propertyName);
                    if (property != null)
                        array[i++] = property.GetValue(values, null);
                    else
                        i++;
                }
            }
            return array;
        }

        /// <summary>
        /// Create a CSV header from a type, using instance properties (with public getters and setters)
        /// </summary>
        public static string ToCsvHeader<T>()
        {
            bool first = true;
            var str = "";
            foreach (var col in ToHeader<T>())
            {
                if (first)
                    first = false;
                else
                    str += _valueSeparator;
                str += ToCsvValue(col);
            }
            return str;
        }

        /// <summary>
        /// Create a header from a type, using instance properties (with public getters and setters)
        /// </summary>
        public static string[] ToHeader<T>()
        {
            return typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance)
                .Where(p => p.GetGetMethod() != null && p.GetGetMethod().IsPublic && p.GetSetMethod() != null && p.GetSetMethod().IsPublic)
                .Select(p => p.Name)
                .ToArray();
        }
    }
}
