﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Wheel.Data.Tests.DbContextIntegration.Models
{
    public class User
    {
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }
    }
}
