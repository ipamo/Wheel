Wheel 3.0 changelog
===================

## Change minimal dependency levels to 3.0

- Because of many breaking API changes from Microsoft. Examples: https://github.com/aspnet/EntityFrameworkCore/issues/18256
The workaround is complicated to maintain.

- This also imply changes on Wheel.Data framework requirements (because of minimal framework requirements for EntityFrameworkCore 3):
	- Upgrade from netstandard2.0 to netstandard2.1
	- Drop support for net461 (EntityFrameworkCore 3 is only available on .NET Core).

## Created Wheel.Plus assembly for highly-dependent features

The base Wheel assembly has been stripped down to make it have as low dependencies as possible:
- netstandard2.0
- Microsoft.Extensions.Logging.Abstractions
- System.ComponentModel.Annotations

Features that have additional dependencies have been moved to the new Wheel.Plus assembly:
- Configuration utils
- NLog utils
- Emailing utils
- Infra features

## Moved schema attributes from Wheel.Data to Wheel

These attributes may be used to define behavior on POCOs.

Only the behavior implemntation needs to be in Wheel.Data.

## Improved schema attributes

- IndexAttribute: added filter options, renamed IsUnique to Unique.
- Added DecimalAttribute, AmountAttribute, CoordinateAttribute. 

## Misc

- Added many utils to Wheel project.
- Adapt to newer versions of Microsoft.Extensions.Logging.Abstractions
- Simplify BaseDbms constructor requirements to allow its usage as a host service
- Move OnModelCreating features from DbContextUtils to new, more specialized ModelCreatingHelper.
