﻿using System;
using System.Collections.Generic;
using System.Text;

namespace System.ComponentModel.DataAnnotations
{
    internal class SR
    {

        public static readonly string ArgumentIsNullOrWhitespace
            = "The argument '{0}' cannot be null, empty or contain only whitespace.";

        public static readonly string AttributeStore_Unknown_Property
            = "The type '{0}' does not contain a public property named '{1}'.";

        public static readonly string Common_PropertyNotFound
            = "The property {0}.{1} could not be found.";

        public static readonly string CompareAttribute_MustMatch
            = "'{0}' and '{1}' do not match.";

        public static readonly string CompareAttribute_UnknownProperty
            = "Could not find a property named {0}.";

        public static readonly string CreditCardAttribute_Invalid
            = "The {0} field is not a valid credit card number.";

        public static readonly string CustomValidationAttribute_Method_Must_Return_ValidationResult
            = "The CustomValidationAttribute method '{0}' in type '{1}' must return System.ComponentModel.DataAnnotations.ValidationResult.  Use System.ComponentModel.DataAnnotations.ValidationResult.Success to represent success.";

        public static readonly string CustomValidationAttribute_Method_Not_Found
            = "The CustomValidationAttribute method '{0}' does not exist in type '{1}' or is not public and static.";

        public static readonly string CustomValidationAttribute_Method_Required
            = "The CustomValidationAttribute.Method was not specified.";

        public static readonly string CustomValidationAttribute_Method_Signature
            = "The CustomValidationAttribute method '{0}' in type '{1}' must match the expected signature: public static ValidationResult {0}(object value, ValidationContext context).  The value can be strongly typed.  The ValidationContext parameter is optional.";

        public static readonly string CustomValidationAttribute_Type_Conversion_Failed
            = "Could not convert the value of type '{0}' to '{1}' as expected by method {2}.{3}.";

        public static readonly string CustomValidationAttribute_Type_Must_Be_Public
            = "The custom validation type '{0}' must be public.";

        public static readonly string CustomValidationAttribute_ValidationError
            = "{0} is not valid.";

        public static readonly string CustomValidationAttribute_ValidatorType_Required
            = "The CustomValidationAttribute.ValidatorType was not specified.";

        public static readonly string DataTypeAttribute_EmptyDataTypeString
            = "The custom DataType string cannot be null or empty.";

        public static readonly string DisplayAttribute_PropertyNotSet
            = "The {0} property has not been set.  Use the {1} method to get the value.";

        public static readonly string EmailAddressAttribute_Invalid
            = "The {0} field is not a valid e-mail address.";

        public static readonly string EnumDataTypeAttribute_TypeCannotBeNull
            = "The type provided for EnumDataTypeAttribute cannot be null.";

        public static readonly string EnumDataTypeAttribute_TypeNeedsToBeAnEnum
            = "The type '{0}' needs to represent an enumeration type.";

        public static readonly string FileExtensionsAttribute_Invalid
            = "The {0} field only accepts files with the following extensions: {1}";

        public static readonly string LocalizableString_LocalizationFailed
            = "Cannot retrieve property '{0}' because localization failed.  Type '{1}' is not public or does not contain a public static string property with the name '{2}'.";

        public static readonly string MaxLengthAttribute_InvalidMaxLength
            = "MaxLengthAttribute must have a Length value that is greater than zero. Use MaxLength() without parameters to indicate that the string or array can have the maximum allowable length.";

        public static readonly string MaxLengthAttribute_ValidationError
            = "The field {0} must be a string or array type with a maximum length of '{1}'.";

        public static readonly string MinLengthAttribute_InvalidMinLength
            = "MinLengthAttribute must have a Length value that is zero or greater.";

        public static readonly string MinLengthAttribute_ValidationError
            = "The field {0} must be a string or array type with a minimum length of '{1}'.";

        public static readonly string LengthAttribute_InvalidValueType
            = "The field of type {0} must be a string, array or ICollection type.";

        public static readonly string PhoneAttribute_Invalid
            = "The {0} field is not a valid phone number.";

        public static readonly string RangeAttribute_ArbitraryTypeNotIComparable
            = "The type {0} must implement {1}.";

        public static readonly string RangeAttribute_MinGreaterThanMax
            = "The maximum value '{0}' must be greater than or equal to the minimum value '{1}'.";

        public static readonly string RangeAttribute_Must_Set_Min_And_Max
            = "The minimum and maximum values must be set.";

        public static readonly string RangeAttribute_Must_Set_Operand_Type
            = "The OperandType must be set when strings are used for minimum and maximum values.";

        public static readonly string RangeAttribute_ValidationError
            = "The field {0} must be between {1} and {2}.";

        public static readonly string RegexAttribute_ValidationError
            = "The field {0} must match the regular expression '{1}'.";

        public static readonly string RegularExpressionAttribute_Empty_Pattern
            = "The pattern must be set to a valid regular expression.";

        public static readonly string RequiredAttribute_ValidationError
            = "The {0} field is required.";

        public static readonly string StringLengthAttribute_InvalidMaxLength
            = "The maximum length must be a nonnegative integer.";

        public static readonly string StringLengthAttribute_ValidationError
            = "The field {0} must be a string with a maximum length of {1}.";

        public static readonly string StringLengthAttribute_ValidationErrorIncludingMinimum
            = "The field {0} must be a string with a minimum length of {2} and a maximum length of {1}.";

        public static readonly string UIHintImplementation_ControlParameterKeyIsNotAString
            = "The key parameter at position {0} with value '{1}' is not a string. Every key control parameter must be a string.";

        public static readonly string UIHintImplementation_ControlParameterKeyIsNull
            = "The key parameter at position {0} is null. Every key control parameter must be a string.";

        public static readonly string UIHintImplementation_ControlParameterKeyOccursMoreThanOnce
            = "The key parameter at position {0} with value '{1}' occurs more than once.";

        public static readonly string UIHintImplementation_NeedEvenNumberOfControlParameters
            = "The number of control parameters must be even.";

        public static readonly string UrlAttribute_Invalid
            = "The {0} field is not a valid fully-qualified http, https, or ftp URL.";

        public static readonly string ValidationAttribute_Cannot_Set_ErrorMessage_And_Resource
            = "Either ErrorMessageString or ErrorMessageResourceName must be set, but not both.";

        public static readonly string ValidationAttribute_IsValid_NotImplemented
            = "IsValid(object value) has not been implemented by this class.  The preferred entry point is GetValidationResult() and classes should override IsValid(object value, ValidationContext context).";

        public static readonly string ValidationAttribute_NeedBothResourceTypeAndResourceName
            = "Both ErrorMessageResourceType and ErrorMessageResourceName need to be set on this attribute.";

        public static readonly string ValidationAttribute_ResourcePropertyNotStringType
            = "The property '{0}' on resource type '{1}' is not a string type.";

        public static readonly string ValidationAttribute_ResourceTypeDoesNotHaveProperty
            = "The resource type '{0}' does not have an accessible static property named '{1}'.";

        public static readonly string ValidationAttribute_ValidationError
            = "The field {0} is invalid.";

        public static readonly string Validator_InstanceMustMatchValidationContextInstance
            = "The instance provided must match the ObjectInstance on the ValidationContext supplied.";

        public static readonly string Validator_Property_Value_Wrong_Type
            = "The value for property '{0}' must be of type '{1}'.";

        public static string Format(string format, params object[] args)
            => string.Format(format, args);

        public static bool IsNullOrWhiteSpace(string str)
            => str == null || str == "" || str.Trim() == "";
    }
}
