﻿using System;
using System.Net;
using System.Net.NetworkInformation;

namespace Wheel
{
    public static class NetUtils
    {
        /// <summary>
        /// Check network connectivity.
        /// </summary>
        /// <param name="http">If true, make a DNS and HTTP full test.
        /// THis might take a long time (> 1 min) in case of no-connectivity.</param>
        public static bool CheckConnectivity(bool http = false)
        {
            if (http)
            {
                try
                {
                    using (var client = new WebClient())
                    using (client.OpenRead("http://google.com/generate_204"))
                        return true;
                }
                catch
                {
                    return false;
                }
            }
            else
            {
                return Ping("8.8.8.8"); // Google DNS server
            }
        }

        public static bool Ping(string host, int timeout = 1000)
        {
            try
            {
                var buffer = new byte[32];
                using (var ping = new Ping())
                {
                    var reply = ping.Send(host, timeout, buffer, new PingOptions { });
                    return reply.Status == IPStatus.Success;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
