﻿using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;

namespace Wheel.Commands
{
    /// <summary>
    /// An interface for command application Startup classes. The exact usage of this interface is not required: Startup classes
    /// can use infrastructure services (such as <see cref="IHostEnvironment"/>, <see cref="IConfiguration"/> or <see cref="ILoggerFactory"/>)
    /// as additional arguments to the ConfigureServices and ConfigureCommands methods.
    /// </summary>
    public interface ICommandStartup
    {
        void ConfigureServices(IServiceCollection services);

        void ConfigureCommands(ICommandBuilder cmd);
    }
}
