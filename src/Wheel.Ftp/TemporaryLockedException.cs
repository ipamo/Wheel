﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Wheel.Ftp
{
    internal class TemporaryLockedException : Exception
    {
        public TemporaryLockedException(string message)
            : base(message)
        {

        }
    }
}
