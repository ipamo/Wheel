CREATE OR REPLACE FUNCTION fn_essai_out(
	nom text, 
    OUT status_message text)
AS
$$
BEGIN
    status_message := 'Hello, ' || nom;
	RAISE DEBUG 'test debug: %', status_message;
    RAISE LOG 'test log: %', status_message;
    RAISE INFO 'test info: %', status_message;
    RAISE WARNING 'test warning: %', status_message;
END;
$$
LANGUAGE 'plpgsql' VOLATILE;

CREATE OR REPLACE FUNCTION fn_essai_return(
	nom text)
RETURNS int
AS
$$
BEGIN
	RAISE DEBUG 'test debug: %', nom;
    RAISE LOG 'test log: %', nom;
    RAISE INFO 'test info: %', nom;
    RAISE WARNING 'test warning: %', nom;
	RETURN 3;
END;
$$
LANGUAGE 'plpgsql' VOLATILE;

