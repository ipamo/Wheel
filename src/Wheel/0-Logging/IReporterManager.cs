﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Wheel
{
    /// <summary>
    /// Manage <see cref="IReporter"/> objects in an application.
    /// </summary>
    public interface IReporterManager
    {
        /// <summary>
        /// Get the <see cref="IReporter"/> associated to the current thread,
        /// or <c>null</c> if none was registered.
        /// </summary>
        IReporter Reporter { get; }

        /// <summary>
        /// Register the given reporter to the current thread.
        /// </summary>
        void Register(IReporter reporter);

        /// <summary>
        /// Unregister reporter associated to the current thread, if any.
        /// </summary>
        /// <param name="allThreads">The reporter will also be removed from all other threads it has been registered.</param>
        void Unregister(bool allThreads = false);
    }
}
