Roadmap
=======

## Before all releases

- Review examples

- Review TODO markers and NotImplementedException

- Review list of warnings and messages in Visual Studio

- Review docfx website

## For next versions

- Allow usage of non-public (internal ? private ? nothing ?) ConfigureCommands, ConfigureServices, Run methods.
		=> must be tested on .NET 2.0, .NET 4.6 and .NET Core, as behaviours might vary

- Allow usage of root.AddCommands() for static command classes and/or classes defined using ConfigureCommand (instead of attributes and Run method).

- Make CommandRunWrapper available for async Run methods (+ for static Run() methods and static properties?)

- Check for production version of https://www.nuget.org/packages/Oracle.ManagedDataAccess.Core

- Finalize Wheel.Data.Oracle and PostgreSql (components, dicts, stored procedures). Wait for Oracle.ManagedDataAccess.Core final version.

- Internationalize (HtmlReport, erc).

- AutoColumns: add foreign keys in model configuring?

- Test on Linux

- Assembly signing

- Wheel.Zabbix

- net20 and ICommandApplicationBuilder: configure logging with ILoggingBuilder rather than ILoggerFactory, in order to unify with the rest of logging configurations.
